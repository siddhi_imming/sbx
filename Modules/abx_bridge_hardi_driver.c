/*
                             *******************
******************************* C HEADER FILE *******************************
**                           *******************                           **
**                                                                         **
** project   : General modules                                             **
** filename  : abx_bridge_hardi_unit.c                                      **
**                                                                         **
*****************************************************************************
**       Property of SBG Precision Farming    All rights reserved          **
*****************************************************************************

VERSION HISTORY:
----------------

Version     : 1
Date        : 20130416
Revised by  : Siddhi Imming
Description : * Original version.

Global usage description:
-------------------------

This unit converts SBGuidance CAN message to RS232 Hardi protocol.

*/

/****************************************************************************/
/**                                                                        **/
/**                     MODULES USED                                       **/
/**                                                                        **/
/****************************************************************************/

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "abx_bridge_hardi_driver.h"
#include "uart_service.h"
#include "log.h"
#include "hardi_task.h"
#include "ibs_dl_section_ctrl.h"

/****************************************************************************/
/**                                                                        **/
/**                     DEFINITIONS AND MACROS                             **/
/**                                                                        **/
/****************************************************************************/

#define START_OF_HEADER     0x01
#define START_OF_TEXT       0x02
#define END_OF_TEXT         0x03
#define END_OF_TRANSMISSION 0x04

#define STR_2_DWORD(str) ((UI32_t)(0x00000000L | (str[0] << 16) | (str[1] << 8) | (str[2])))
#define STR_2_WORD(str)  ((UI16_t)(0x0000 | (str[0] << 8) | (str[1])))

/****************************************************************************/
/**                                                                        **/
/**                     TYPEDEFS AND STRUCTURES                            **/
/**                                                                        **/
/****************************************************************************/

typedef struct HardiMeesage_s
{
    UI08_t startOfHeader;
    UI08_t teleType[3];
    UI08_t startOfData;
    Char_t cmdAndData[60]; // after the string there will follow End of data, 
                           // checksum and end of transmission.
}HardiMeesage_t;

typedef struct HardiMeesageEnd_s
{
    UI08_t endOfData;
    UI08_t checksum;
    UI08_t endOfTransmission;
}HardiMeesageEnd_t;

typedef union Sections_u
{
    UI08_t sectionBytes[2];
    struct
    {
        UI08_t sectionBit0  :1; //1
        UI08_t sectionBit1  :1; //2
        UI08_t sectionBit2  :1; //3
        UI08_t sectionBit3  :1; //4
        UI08_t sectionBit4  :1; //5
        UI08_t sectionBit5  :1; //6
        UI08_t sectionBit6  :1; //7
        UI08_t sectionBit7  :1; //8
        UI08_t sectionBit8  :1; //9
        UI08_t sectionBit9  :1; //10
        UI08_t sectionBit10 :1; //11
        UI08_t sectionBit11 :1; //12
        UI08_t sectionBit12 :1; //13
        UI08_t sectionBit13 :1; //14
        UI08_t sectionBit14 :1; //15
        UI08_t sectionBit15 :1; //16
    }bits;
}Sections_t;

/****************************************************************************/
/**                                                                        **/
/**                     PROTOTYPES OF LOCAL FUNCTIONS                      **/
/**                                                                        **/
/****************************************************************************/

static char* checksumHardi(char *data, UI08_t length);

/****************************************************************************/
/**                                                                        **/
/**                     EXPORTED VARIABLES                                 **/
/**                                                                        **/
/****************************************************************************/

/****************************************************************************/
/**                                                                        **/
/**                     GLOBAL VARIABLES                                   **/
/**                                                                        **/
/****************************************************************************/

static ModuleConfiguration_t ModuleCfg;
static HardiMeesage_t HardiCmd;
static Sections_t SectionsCmd;
static Sections_t SectionsHardi;

/****************************************************************************/
/**                                                                        **/
/**                     EXPORTED FUNCTIONS                                 **/
/**                                                                        **/
/****************************************************************************/

/*************************************************************************
* function: ConfigHardi  
* comments: Configures the hardi peripheral driver.  
**************************************************************************/
void ConfigHardi(ModuleConfiguration_t *moduleConfigPtr)
{
    ModuleCfg = *moduleConfigPtr;
    SectionsCmd.sectionBytes[0] = 0x00;
    SectionsCmd.sectionBytes[1] = 0x00;
    SectionsHardi.sectionBytes[0] = 0x00;
    SectionsHardi.sectionBytes[1] = 0x00;

    OS_SignalEvent(HARDI_EVENT_START,&TcbHardiTask);
}


/*************************************************************************
* function:     SetSectionInputHardi 
* parameters:   SectionCollectionId_t sectionCollectionId: indicates which 
*               collection should be reported
* returns:      void   
* comments:     The section command is saved and a event is set to task 
*               that determines if it is possible to send data.
**************************************************************************/
void SetSectionInputHardi(SectionCollectionId_t collectionId,SectionCollectionData_t *sectionCollectionDataPtr)
{
    static Bool_t mainSwitch = FALSE;
    static Bool_t prevMainSwitch = FALSE;
    static Bool_t enableUnit = FALSE;
    static Bool_t prevEnableUnit = FALSE;

    switch (collectionId)
    {
        case SECTION_ID_1_56:
            SectionsCmd.sectionBytes[0] = sectionCollectionDataPtr->sections1_8;
            SectionsCmd.sectionBytes[1] = sectionCollectionDataPtr->sections9_16;
            sectionCollectionDataPtr->sections1_8 = SectionsHardi.sectionBytes[0];
            sectionCollectionDataPtr->sections9_16 = SectionsHardi.sectionBytes[1];

            OS_SignalEvent(HARDI_EVENT_SEND_COMMAND, &TcbHardiTask);
            break;
        case STATUS_INFORMATION:
            
            mainSwitch = (sectionCollectionDataPtr->sectionBytes[0] & 0x04) >> 2;
            enableUnit = (sectionCollectionDataPtr->sectionBytes[0] & 0x01);
            // Check last states to reduce RS232 traffic.
            if ((mainSwitch != prevMainSwitch) ||
                (enableUnit != prevEnableUnit))
            {
                prevMainSwitch = mainSwitch;
                prevEnableUnit = enableUnit;
                if ((mainSwitch == FALSE) || (enableUnit == FALSE))
                {
                    SectionsCmd.sectionBytes[0] = 0;
                    SectionsCmd.sectionBytes[1] = 0;
                }
                OS_SignalEvent(HARDI_EVENT_SEND_COMMAND, &TcbHardiTask);
            }
            break;
        case SEND_LAST_SECTION_STATUS:
            break;
        default:
            break;
    }
}

/*************************************************************************
* function:     SendSectionInput2Hardi 
* parameters:   void
* returns:      void   
* comments:     Send section command status over the RS232 port
**************************************************************************/
void SendSectionInput2Hardi(void)
{
    UI08_t lengthStr;
    char *checksumPtr;
    HardiCmd.startOfHeader = START_OF_HEADER;
    HardiCmd.teleType[0] = 'S';
    HardiCmd.teleType[1] = '0';
    HardiCmd.teleType[2] = 'C';
    HardiCmd.startOfData = START_OF_TEXT;

    sprintf((char*)&HardiCmd.cmdAndData, "6C,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d",
        SectionsCmd.bits.sectionBit0, SectionsCmd.bits.sectionBit1, SectionsCmd.bits.sectionBit2,
        SectionsCmd.bits.sectionBit3, SectionsCmd.bits.sectionBit4, SectionsCmd.bits.sectionBit5,
        SectionsCmd.bits.sectionBit6, SectionsCmd.bits.sectionBit7, SectionsCmd.bits.sectionBit8,
        SectionsCmd.bits.sectionBit9, SectionsCmd.bits.sectionBit10,SectionsCmd.bits.sectionBit11,
        SectionsCmd.bits.sectionBit12);
    lengthStr = strlen((char*)&HardiCmd.cmdAndData);
    HardiCmd.cmdAndData[lengthStr] = END_OF_TEXT;
    checksumPtr = checksumHardi((char*)&HardiCmd, lengthStr+6);
    HardiCmd.cmdAndData[lengthStr + 1] = checksumPtr[0];
    HardiCmd.cmdAndData[lengthStr + 2] = checksumPtr[1];
    HardiCmd.cmdAndData[lengthStr + 3] = END_OF_TRANSMISSION;
    HardiCmd.cmdAndData[lengthStr + 4] = '\r';
    UartWriteBytes(UART_PORT2, (UI08_t*)&HardiCmd, lengthStr+10);
}

/*************************************************************************
* function:     SendSectionInput2SectionCtrl 
* parameters:   void
* returns:      void   
* comments:     Send section status status over CAN to section control ON/OFF
**************************************************************************/
void SendSectionInput2SectionCtrl(void)
{
    static SectionCollectionData_t tmp;
    tmp.sectionBytes[0] = SectionsHardi.sectionBytes[0];
    tmp.sectionBytes[1] = SectionsHardi.sectionBytes[1];
    tmp.sectionBytes[2] = 0x00;
    tmp.sectionBytes[3] = 0x00;
    tmp.sectionBytes[4] = 0x00;
    tmp.sectionBytes[5] = 0x00;
    tmp.sectionBytes[6] = 0x00;
    IbsSendCurrentSectionStatus(SECTION_ID_1_56, &tmp);
}

/*************************************************************************
* function:     SendSectionStatusRequest2Hardi
* parameters:   void
* returns:      void   
* comments:     Send section request to get the section status.
**************************************************************************/
void SendSectionStatusRequest2Hardi(void)
{
    UI08_t lengthStr;
    char *checksumPtr;
    HardiCmd.startOfHeader = START_OF_HEADER;
    HardiCmd.teleType[0] = 'R';
    HardiCmd.teleType[1] = '0';
    HardiCmd.teleType[2] = 'D';
    HardiCmd.startOfData = START_OF_TEXT;
    sprintf((char*)&HardiCmd.cmdAndData, "6B");
    lengthStr = strlen((char*)&HardiCmd.cmdAndData);
    HardiCmd.cmdAndData[lengthStr] = END_OF_TEXT;
    checksumPtr = checksumHardi((char*)&HardiCmd, lengthStr+6);
    HardiCmd.cmdAndData[lengthStr + 1] = checksumPtr[0];
    HardiCmd.cmdAndData[lengthStr + 2] = checksumPtr[1];
    HardiCmd.cmdAndData[lengthStr + 3] = END_OF_TRANSMISSION;
    HardiCmd.cmdAndData[lengthStr + 4] = '\r';
    UartWriteBytes(UART_PORT2, (UI08_t*)&HardiCmd, lengthStr+10);

}

/*************************************************************************
* function:     ParseByte
* parameters:   void
* returns:      void   
* comments:     Parse incoming data one byte at the time. If a complete 
*               message is received data will be parsed in a separate task.
**************************************************************************/
void ParseByteHardi(UI08_t incomingByte)
{
    static UI08_t receivingState = 1;
    static UI08_t buffer[HARDI_BUFFER_SIZE];
    static UI08_t index = 0;

    switch (receivingState)
    {
        case 1: // find Start of header
            if (incomingByte == START_OF_HEADER)
            {
                buffer[index++] = incomingByte;
                receivingState = 2;
            }
            break;
        case 2: // find end of transmission
            if (incomingByte == END_OF_TRANSMISSION)
            {
                // if CR is received then complete string
                buffer[index++] = incomingByte;
                buffer[index++] = '\0';

                // Put the data in the mailbox on succes signal a event.
                if(OS_PutMailCond(&MailboxHardiRs232, buffer) == 0)
                {
                    OS_SignalEvent(HARDI_EVENT_DATA_RECEIVED, &TcbHardiTask);
                }
                else
                {
                    //PROBLEEM
                }

                // Clear buffer and index because start of header must be on the first byte.
                index = 0;
                receivingState = 1;
                memset(&buffer, 0, HARDI_BUFFER_SIZE);
            }
            // check is message is not to long
            else if (index > HARDI_BUFFER_SIZE)
            {
                index = 0;
                receivingState = 1;
                memset(&buffer, 0, HARDI_BUFFER_SIZE);
            }
            // add byte until end of transmission is received.
            else
            {
                buffer[index++] = incomingByte;
            }
            break;
    }
}

/*************************************************************************
* function:     HardiHandleData 
* parameters:   char *data
* returns:      TRUE is section information is received.  
* comments:     This function parse the received data of the HC5500 or HC6500 (Hardi)
**************************************************************************/
Bool_t HardiHandleData(char *data)
{
    char    tmpStr[4];
    char    tokenToFind[] = ",";
    char    *resultPtr;
    char    *checksumPtr;
    UI08_t  index = 0;
    UI08_t  strLength = 0;
    Sections_t tmpStatus;

    tmpStatus.sectionBytes[0] = 0x00; 
    tmpStatus.sectionBytes[1] = 0x00;

    strLength = strlen(&data[0]);

    // check checksum and then handle incoming message.
    checksumPtr = checksumHardi(&data[0], strLength-3);
    if ((checksumPtr[0] != data[strLength-3]) || 
        (checksumPtr[1] != data[strLength-2]))
    {
        return FALSE;
    }

    // only pars "A0D" answer of a request and 
    // "V0C" acknowledge of a command
    strncpy(&tmpStr[0],&data[1],3);
    switch (STR_2_DWORD(tmpStr))
    {
        case STR_2_DWORD("A0D"):    // This this is the current section status. 
        //case STR_2_DWORD("V0C"):  // This is not send by the HC6500
        //case STR_2_DWORD("N0C"):  // This is only a replay on the cmd hence this will not send the current section status.
            break;
        default:
            return FALSE;
            break;
    }

    strncpy(&tmpStr[0],&data[5],2);
    switch (STR_2_WORD(tmpStr))
    {
      //case STR_2_WORD("6C"):
      case STR_2_WORD("6B"):
            break;
        default:
            return FALSE;
            break;
    }

    // find first ',' token
    resultPtr = strtok(&data[0], tokenToFind );
    if (resultPtr == NULL)
    {
        return FALSE;
    }


    while( resultPtr != NULL ) 
    {
        resultPtr = strtok( NULL, tokenToFind );
        //if (strncmp(resultPtr, " 6C", 3) != 0)
        //{  
          if(index < 8)
          {
              tmpStatus.sectionBytes[0] = tmpStatus.sectionBytes[0] | ((atoi(resultPtr) & 0x01) << index);
              index++;
          }
          else
          {
              tmpStatus.sectionBytes[1] = tmpStatus.sectionBytes[1] | ((atoi(resultPtr) & 0x01)  << (index-8));
              index++;
              // if all 13 section are set the hardi status.
              if (index == 13)
              {
                SectionsHardi.sectionBytes[0] =  tmpStatus.sectionBytes[0];
                SectionsHardi.sectionBytes[1] =  tmpStatus.sectionBytes[1];
                return TRUE;
              }
          }
        //}
    }
    return FALSE;
}

/****************************************************************************/
/**                                                                        **/
/**                     LOCAL FUNCTIONS                                    **/
/**                                                                        **/
/****************************************************************************/

/*************************************************************************
* function:     checksumHardi 
* parameters:   UI08_t *data
* returns:      UI08_t checkSum of the data   
* comments:     This function creates a checksum out of array of data bytes
**************************************************************************/
static char* checksumHardi(char *data, UI08_t length)
{
    static char hexChecksum[3];
    UI08_t checksum = data[0];
    for(UI08_t index = 1; index < length; index++)
    {
        checksum ^= data[index];
    }
    sprintf((char*)hexChecksum, "%X", checksum);

    return &hexChecksum[0];
}

/****************************************************************************/
/**                                                                        **/
/**                               EOF                                      **/
/**                                                                        **/
/****************************************************************************/
