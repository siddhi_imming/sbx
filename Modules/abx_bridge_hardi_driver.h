/*
                             *******************
******************************* C HEADER FILE *******************************
**                           *******************                           **
**                                                                         **
** project   : General modules                                             **
** filename  : abx_bridge_hardi_driver.h                                   **        
**                                                                         **
*****************************************************************************
**                                                                         **
** Copyright (c) 2012, SBG Precision Farming/Navtronics Bvba               **
** All rights reserved.                                                    **
**                                                                         **
*****************************************************************************


VERSION HISTORY:
----------------

Version     : 1
Date        : 20130308
Revised by  : Siddhi Imming
Description : * Original version.

FILE INFORMATION:
-----------------

This unit converts SBGuidance CAN messege to auxillary ISO-Bus message (version 2006).

*/

#ifndef _ABX_BRIDGE_HARDI_DRIVER_INCLUDED
#define _ABX_BRIDGE_HARDI_DRIVER_INCLUDED

/****************************************************************************/
/**                                                                        **/
/**                     MODULES USED                                       **/
/**                                                                        **/
/****************************************************************************/

#include "sysconf.h"
#include "module_defs.h"
#include "section_ctrl_defs.h"

/****************************************************************************/
/**                                                                        **/
/**                     DEFINITIONS AND MACROS                             **/
/**                                                                        **/
/****************************************************************************/

#define HARDI_BUFFER_SIZE         64

/****************************************************************************/
/**                                                                        **/
/**                     TYPEDEFS AND STRUCTURES                            **/
/**                                                                        **/
/****************************************************************************/

/****************************************************************************/
/**                                                                        **/
/**                     EXPORTED VARIABLES                                 **/
/**                                                                        **/
/****************************************************************************/


/****************************************************************************/
/**                                                                        **/
/**                     EXPORTED FUNCTIONS                                 **/
/**                                                                        **/
/****************************************************************************/


/*************************************************************************
* function: ConfigHardi  
* comments: Configures the hardi peripheral driver.  
**************************************************************************/
void ConfigHardi(ModuleConfiguration_t *moduleConfigPtr);

/*************************************************************************
* function:     SetSectionInputHardi 
* parameters:   SectionCollectionId_t sectionCollectionId: indicates which 
*               collection should be reported
* returns:      void   
* comments:     
**************************************************************************/
void SetSectionInputHardi(SectionCollectionId_t collectionId,SectionCollectionData_t *sectionCollectionDataPtr);

/*************************************************************************
* function:     SendSectionInput2Hardi 
* parameters:   void
* returns:      void   
* comments:     Send section command status over the RS232 port
**************************************************************************/
void SendSectionInput2Hardi(void);

/*************************************************************************
* function:     SendSectionStatusRequest2Hardi
* parameters:   void
* returns:      void   
* comments:     Parse incoming data.
**************************************************************************/
void SendSectionStatusRequest2Hardi(void);


/*************************************************************************
* function:     ParseByte
* parameters:   void
* returns:      void   
* comments:     Parse incoming data.
**************************************************************************/
void ParseByteHardi(UI08_t);

/*************************************************************************
* function:     SendSectionInput2SectionCtrl 
* parameters:   void
* returns:      void   
* comments:     Send section status status over CAN to section control ON/OFF
**************************************************************************/
void SendSectionInput2SectionCtrl(void);

/*************************************************************************
* function:     HardiHandleData 
* parameters:   char *data
* returns:      TRUE is section information is received.  
* comments:     This function parse the received data of the HC5500 or HC6500 (Hardi)
**************************************************************************/
Bool_t HardiHandleData(char *data);

#endif

/****************************************************************************/
/**                                                                        **/
/**                               EOF                                      **/
/**                                                                        **/
/****************************************************************************/
