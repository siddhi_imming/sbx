/*
                             *******************
******************************* C HEADER FILE *******************************
**                           *******************                           **
**                                                                         **
** project   : General modules                                             **
** filename  : abx_bridge_auxiliary_unit.c                                  **
**                                                                         **
*****************************************************************************
**       Property of SBG Precision Farming    All rights reserved          **
*****************************************************************************

VERSION HISTORY:
----------------

Version     : 1
Date        : 20130319
Revised by  : Siddhi Imming
Description : * Original version.

Global usage description:
-------------------------

This unit converts SBGuidance CAN messege to auxillary ISO-Bus message (version 2006).

*/

/****************************************************************************/
/**                                                                        **/
/**                     MODULES USED                                       **/
/**                                                                        **/
/****************************************************************************/

#include "abx_bridge_auxiliary_driver.h"
#include "ibs_dl_aux_unit.h"

/****************************************************************************/
/**                                                                        **/
/**                     DEFINITIONS AND MACROS                             **/
/**                                                                        **/
/****************************************************************************/

/****************************************************************************/
/**                                                                        **/
/**                     TYPEDEFS AND STRUCTURES                            **/
/**                                                                        **/
/****************************************************************************/

/****************************************************************************/
/**                                                                        **/
/**                     PROTOTYPES OF LOCAL FUNCTIONS                      **/
/**                                                                        **/
/****************************************************************************/

/****************************************************************************/
/**                                                                        **/
/**                     EXPORTED VARIABLES                                 **/
/**                                                                        **/
/****************************************************************************/

/****************************************************************************/
/**                                                                        **/
/**                     GLOBAL VARIABLES                                   **/
/**                                                                        **/
/****************************************************************************/

static ModuleConfiguration_t ModuleCfg;
static AuxMessage_t AuxMessage;

/****************************************************************************/
/**                                                                        **/
/**                     EXPORTED FUNCTIONS                                 **/
/**                                                                        **/
/****************************************************************************/

/*************************************************************************
* function: ConfigDriverMotor  
* comments: Configures the motor driver.  
**************************************************************************/
void ConfigAuxUnit(ModuleConfiguration_t *moduleConfigPtr)
{
    ModuleCfg = *moduleConfigPtr;
    AuxMessage.enableUnit = FALSE;
    
    if (moduleConfigPtr->numSections < 16)
    {
        AuxMessage.reservedByte = moduleConfigPtr->numSections;
    }
    else
    {
        AuxMessage.reservedByte = 0xFF;
    }
}


/*************************************************************************
* function:     SetSectionAuxInput 
* parameters:   SectionCollectionId_t sectionCollectionId: indicates which 
*               collection should be reported
* returns:      void   
* comments:     
**************************************************************************/
void SetSectionAuxInput(SectionCollectionId_t collectionId,SectionCollectionData_t *sectionCollectionDataPtr)
{
    static Bool_t mainSwitch = FALSE;
    static Bool_t prevMainSwitch = FALSE;
    static Bool_t prevEnableUnit = FALSE;

    switch (collectionId)
    {
        case SECTION_ID_1_56:
            AuxMessage.input1 = ((UI08_t*)sectionCollectionDataPtr)[0];
            if ((( ((UI08_t*)sectionCollectionDataPtr)[0] != 0) ||
               (( ((UI08_t*)sectionCollectionDataPtr)[1] & 0x7F) != 0)) &&
                mainSwitch)
            {
                AuxMessage.input2 = ((UI08_t*)sectionCollectionDataPtr)[1] | 0x80;
            }
            else
            {
                AuxMessage.input2 = 0x00 | (((UI08_t*)sectionCollectionDataPtr)[1] & 0x7F) ;
            }
            SendSectionAuxInput();
            break;
        case STATUS_INFORMATION:
            mainSwitch = (((UI08_t*)sectionCollectionDataPtr)[0] & 0x04) >> 2;
            AuxMessage.enableUnit = (((UI08_t*)sectionCollectionDataPtr)[0] & 0x01);
            // Check last states to reduce CAN traffic.
            if ((mainSwitch != prevMainSwitch) ||
                 (AuxMessage.enableUnit != prevEnableUnit))
            {
                prevMainSwitch = mainSwitch;
                prevEnableUnit = AuxMessage.enableUnit;
                SendSectionAuxInput();
            }
            break;
        case SEND_LAST_SECTION_STATUS:
            SendSectionAuxInput();
            break;
        default:
            break;
    }
}

/*************************************************************************
* function:     SendSectionAuxInput 
* parameters:   void
* returns:      void   
* comments:     Send only a CAN message if the module is set to Amaclick or 
*               S-Box.
**************************************************************************/
void SendSectionAuxInput(void)
{
    if (ModuleCfg.moduleType == MODULE_BRIDGE)
    {
        switch(ModuleCfg.protocolType)
        {
            case AMAZONE_AMACLICK:
            case MULLER_S_BOX:
                IbsSendAuxiliaryInputStatus(&AuxMessage);
                AuxMessage.runningCnt++;
                break;
            default:
                break;
        }
    }
}

/****************************************************************************/
/**                                                                        **/
/**                     LOCAL FUNCTIONS                                    **/
/**                                                                        **/
/****************************************************************************/


/****************************************************************************/
/**                                                                        **/
/**                               EOF                                      **/
/**                                                                        **/
/****************************************************************************/
