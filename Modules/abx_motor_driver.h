/*
                             *******************
******************************* C HEADER FILE *******************************
**                           *******************                           **
**                                                                         **
** project   : General modules                                             **
** filename  : abx_motor_driver.h                                       **        
**                                                                         **
*****************************************************************************
**                                                                         **
** Copyright (c) 2012, SBG Precision Farming/Navtronics Bvba               **
** All rights reserved.                                                    **
**                                                                         **
*****************************************************************************


VERSION HISTORY:
----------------

Version     : 1
Date        : 20130308
Revised by  : Koen Smeuninx
Description : * Original version.

FILE INFORMATION:
-----------------
This unit is the driver for an actuator box equipped with a motor driver
daughter board.  It accepts section commands and translates it into motor 
driver commands.
*/

#ifndef _ABX_MOTOR_DRIVER_INCLUDED
#define _ABX_MOTOR_DRIVER_INCLUDED

/****************************************************************************/
/**                                                                        **/
/**                     MODULES USED                                       **/
/**                                                                        **/
/****************************************************************************/

#include "sysconf.h"
#include "module_defs.h"
#include "section_ctrl_defs.h"

/****************************************************************************/
/**                                                                        **/
/**                     DEFINITIONS AND MACROS                             **/
/**                                                                        **/
/****************************************************************************/


/****************************************************************************/
/**                                                                        **/
/**                     TYPEDEFS AND STRUCTURES                            **/
/**                                                                        **/
/****************************************************************************/

/****************************************************************************/
/**                                                                        **/
/**                     EXPORTED VARIABLES                                 **/
/**                                                                        **/
/****************************************************************************/


/****************************************************************************/
/**                                                                        **/
/**                     EXPORTED FUNCTIONS                                 **/
/**                                                                        **/
/****************************************************************************/


/*************************************************************************
* function:     ConfigDriverMotor   
* parameters:   ModuleConfiguration_t *moduleConfigPtr: pointer to structure 
*               containing all the settings needed by the driver to translate 
*               section commands into steering signals for the ioexpander
* returns:      void
* comments:     configures the driver
**************************************************************************/
void ConfigDriverMotor(ModuleConfiguration_t *moduleConfigPtr);


/*************************************************************************
* function:     SetSectionsMotor   
* parameters:   SectionCollectionId_t collectionId: indicates which collection
*               data is passed, 1-56 or 57_112
*               SectionCollectionData_t * sectionCollectionData:
*               SectionData, information of the sections.
* returns:      void
* comments:     This function translates section data into IO Expander data
*               It also takes into account if an Aux valve is available.  
*               The Aux valve is always connected to the 16th motor driver.
**************************************************************************/
void SetSectionsMotor(SectionCollectionId_t collectionId, SectionCollectionData_t *collectionDataPtr);
#endif

/****************************************************************************/
/**                                                                        **/
/**                               EOF                                      **/
/**                                                                        **/
/****************************************************************************/
