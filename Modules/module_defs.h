/*
                             *******************
******************************* C HEADER FILE *******************************
**                           *******************                           **
**                                                                         **
** project   : General modules                                             **
** filename  : module_defs.h                                               **        
**                                                                         **
*****************************************************************************
**                                                                         **
** Copyright (c) 2012, SBG Precision Farming/Navtronics Bvba               **
** All rights reserved.                                                    **
**                                                                         **
*****************************************************************************


VERSION HISTORY:
----------------

Version     : 1
Date        : 20130308
Revised by  : Koen Smeuninx
Description : * Original version.

FILE INFORMATION:
-----------------
This unit is the driver for an actuator box equipped with a solenoid driver
daughter board.  It accepts section commands and translates it into solenoid 
driver commands.
*/

#ifndef _MODULE_DEFS_INCLUDED
#define _MODULE_DEFS_INCLUDED

/****************************************************************************/
/**                                                                        **/
/**                     MODULES USED                                       **/
/**                                                                        **/
/****************************************************************************/
#include "sysconf.h"
#include "section_ctrl_defs.h"
/****************************************************************************/
/**                                                                        **/
/**                     DEFINITIONS AND MACROS                             **/
/**                                                                        **/
/****************************************************************************/

/****************************************************************************/
/**                                                                        **/
/**                     TYPEDEFS AND STRUCTURES                            **/
/**                                                                        **/
/****************************************************************************/

typedef enum ModuleType_e
{
    MODULE_OUTPUTS,
    MODULE_BRIDGE
}ModuleType_t;

typedef enum DriverType_e
{
    DRIVER_SOLENOID,
    DRIVER_MOTOR
}DriverType_t;

typedef enum AuxActuatorType_e
{
    AUX_ACT_NOT_AVAILABLE,
    FREE_WHEEL_VALVE,
    PUMP_CONTROL
}AuxActuatorType_t;

typedef enum ActuatorLogicLevel_e
{
    ACT_LOGIC_HIGH,
    ACT_LOGIC_LOW
}ActuatorLogicLevel_t;

typedef struct AuxValveConfig_s
{   
    SectionIdentifier_t     sectionId;
    AuxActuatorType_t       type;
    ActuatorLogicLevel_t    logicLevel;
}AuxValveConfig_t;

typedef struct ModuleConfiguration_s
{
    ModuleType_t         moduleType;
    ActBoxLProtocol_t    protocolType;
    DriverType_t         driverType;
    UI08_t               numSections;
    ActuatorLogicLevel_t sectionLogicLevel;
    AuxActuatorType_t    auxActType;
    ActuatorLogicLevel_t auxActLogicLevel;
}ModuleConfiguration_t;

/****************************************************************************/
/**                                                                        **/
/**                     EXPORTED VARIABLES                                 **/
/**                                                                        **/
/****************************************************************************/


/****************************************************************************/
/**                                                                        **/
/**                     EXPORTED FUNCTIONS                                 **/
/**                                                                        **/
/****************************************************************************/



#endif

/****************************************************************************/
/**                                                                        **/
/**                               EOF                                      **/
/**                                                                        **/
/****************************************************************************/
