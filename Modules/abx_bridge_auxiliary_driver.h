/*
                             *******************
******************************* C HEADER FILE *******************************
**                           *******************                           **
**                                                                         **
** project   : General modules                                             **
** filename  : abx_bridge_auxiliary_driver.h                               **        
**                                                                         **
*****************************************************************************
**                                                                         **
** Copyright (c) 2012, SBG Precision Farming/Navtronics Bvba               **
** All rights reserved.                                                    **
**                                                                         **
*****************************************************************************


VERSION HISTORY:
----------------

Version     : 1
Date        : 20130308
Revised by  : Siddhi Imming
Description : * Original version.

FILE INFORMATION:
-----------------

This unit converts SBGuidance CAN messege to auxillary ISO-Bus message (version 2006).

*/

#ifndef _ABX_BRIDGE_AUXILIARY_DRIVER_INCLUDED
#define _ABX_BRIDGE_AUXILIARY_DRIVER_INCLUDED

/****************************************************************************/
/**                                                                        **/
/**                     MODULES USED                                       **/
/**                                                                        **/
/****************************************************************************/

#include "sysconf.h"
#include "module_defs.h"
#include "section_ctrl_defs.h"

/****************************************************************************/
/**                                                                        **/
/**                     DEFINITIONS AND MACROS                             **/
/**                                                                        **/
/****************************************************************************/


/****************************************************************************/
/**                                                                        **/
/**                     TYPEDEFS AND STRUCTURES                            **/
/**                                                                        **/
/****************************************************************************/

/****************************************************************************/
/**                                                                        **/
/**                     EXPORTED VARIABLES                                 **/
/**                                                                        **/
/****************************************************************************/


/****************************************************************************/
/**                                                                        **/
/**                     EXPORTED FUNCTIONS                                 **/
/**                                                                        **/
/****************************************************************************/


/*************************************************************************
* function:     ConfigAuxUnit   
* parameters:   ModuleConfiguration_t *moduleConfigPtr: pointer to structure 
*               containing all the settings needed for send Amaclick or S-Box message
* returns:      void
* comments:     configures the the send ISO-Bus message
**************************************************************************/
void ConfigAuxUnit(ModuleConfiguration_t *moduleConfigPtr);


/*************************************************************************
* function:     SetSectionsMotor   
* parameters:   SectionCollectionId_t collectionId: indicates which collection
*               data is passed, 1-56 or 57_112 or if there is system information available.
*               SectionCollectionData_t * sectionCollectionData:
*               SectionData, information of the sections.
* returns:      void
* comments:     
**************************************************************************/
void SetSectionAuxInput(SectionCollectionId_t collectionId, SectionCollectionData_t *sectionCollectionDataPtr);


/*************************************************************************
* function:     SendSectionAuxInput 
* parameters:   void
* returns:      void   
* comments:     Send only a CAN message if the module is set to Amaclick or 
*               S-Box.
**************************************************************************/
void SendSectionAuxInput(void);

#endif

/****************************************************************************/
/**                                                                        **/
/**                               EOF                                      **/
/**                                                                        **/
/****************************************************************************/
