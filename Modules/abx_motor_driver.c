/*
                             *******************
******************************* C HEADER FILE *******************************
**                           *******************                           **
**                                                                         **
** project   : General modules                                             **
** filename  : abx_motor_driver.c                                                 
**                                                                         **
*****************************************************************************
**       Property of SBG Precision Farming    All rights reserved          **
*****************************************************************************

VERSION HISTORY:
----------------

Version     : 1
Date        : 20130308
Revised by  : Koen Smeuninx
Description : * Original version.

Global usage description:
-------------------------

This unit is the driver for an actuator box equipped with a motor driver
daughter board.  It accepts section commands and translates it into motor 
driver commands.

*/

/****************************************************************************/
/**                                                                        **/
/**                     MODULES USED                                       **/
/**                                                                        **/
/****************************************************************************/

#include "abx_motor_driver.h"
#include "lpc17xx_spi.h"
#include "io_expander_spi.h"

/****************************************************************************/
/**                                                                        **/
/**                     DEFINITIONS AND MACROS                             **/
/**                                                                        **/
/****************************************************************************/

/****************************************************************************/
/**                                                                        **/
/**                     TYPEDEFS AND STRUCTURES                            **/
/**                                                                        **/
/****************************************************************************/

/****************************************************************************/
/**                                                                        **/
/**                     PROTOTYPES OF LOCAL FUNCTIONS                      **/
/**                                                                        **/
/****************************************************************************/

static void writeIoExpanderData(SectionCollectionId_t collectionId,UI08_t totalBytes);
static void readIoExpanderData(SectionCollectionId_t collectionId, UI08_t *readData, UI08_t totalBytes);

/****************************************************************************/
/**                                                                        **/
/**                     EXPORTED VARIABLES                                 **/
/**                                                                        **/
/****************************************************************************/


/****************************************************************************/
/**                                                                        **/
/**                     GLOBAL VARIABLES                                   **/
/**                                                                        **/
/****************************************************************************/

static ModuleConfiguration_t ModuleCfg;
static UI08_t IoExpanderBytes[16];

/****************************************************************************/
/**                                                                        **/
/**                     EXPORTED FUNCTIONS                                 **/
/**                                                                        **/
/****************************************************************************/

/*************************************************************************
* function: ConfigDriverMotor  
* comments: Configures the motor driver.  
**************************************************************************/
void ConfigDriverMotor(ModuleConfiguration_t *moduleConfigPtr)
{
    SectionCollectionData_t sectionCollectionData;
    ModuleCfg = *moduleConfigPtr;
    
    if (IoExpanderInit(FALSE))
    {
        for (UI08_t index = 0; index < IoExpanderGetNrOfIoBytes(); index++)
        {
            if (IoExpanderAlloc(IO_EXPANDER_SET_AS_OUTPUT, IO_EXPANDER_SET_AS_PULL_UP) == 0xFF)
            {
                // No IO expander free to init.
                break;
            }
        }
    }
    // Set all outputs to off.
    if (ModuleCfg.auxActLogicLevel == ACT_LOGIC_LOW)
    {
        memset(&sectionCollectionData,1,sizeof(sectionCollectionData));
    }
    else
    {
        memset(&sectionCollectionData,0,sizeof(sectionCollectionData));
    }
    SetSectionsMotor(SECTION_ID_1_56,     &sectionCollectionData);
    SetSectionsMotor(SECTION_ID_57_112,   &sectionCollectionData);
}

/*************************************************************************
* function:     SetSectionsMotor   
* parameters:   SectionCollectionId_t collectionId: indicates which collection
*               data is passed, 1-56 or 57_112
*               SectionCollectionData_t * sectionCollectionData:
*               SectionData, information of the sections.
* returns:      void
* comments:     This function translates section data into IO Expander data
*               It also takes into account if an Aux valve is available.  
*               The Aux valve is always connected to 
*               the 16th motor driver.
**************************************************************************/
void SetSectionsMotor(SectionCollectionId_t collectionId,  SectionCollectionData_t *collectionDataPtr)
{
    I08_t index;
    Bool_t allSectionsOff = TRUE;
    //bytes needed to send all the section information. (aux valve not included)  1 section -> 1byte.
    //9 sections 2 bytes... 
    UI08_t nrOfExpBytes = 
        ((ModuleCfg.numSections * 2) / 8) + 
        (((ModuleCfg.numSections * 2) % 8) == 0?0:2);
    UI08_t ioDataStartIndex;
    UI08_t bytesPerCollection = sizeof(*collectionDataPtr);
    UI08_t readIoExpanderBytes[7];
    
    memset(&readIoExpanderBytes[0], 0, 7);
    
    SectionCollectionData_t collectionData = *collectionDataPtr;

    switch (collectionId)
    {
        case SECTION_ID_1_56:
            ioDataStartIndex = 0;
            break;
        case SECTION_ID_57_112:
            ioDataStartIndex = bytesPerCollection*2;
            break;
        default:
            return;
    }
    
    for (index = 0; index < sizeof(collectionData); index++)
    {
        if (collectionData.sectionBytes[index] != 0)
        {
            allSectionsOff = FALSE;
        }
        //if section data active low, invert signals
        if (ModuleCfg.sectionLogicLevel == ACT_LOGIC_LOW)
        {
            collectionData.sectionBytes[index] = ~collectionData.sectionBytes[index];
        }
    }
    

    //copy the section data directly into the IOExpander data, shifting, depending on the aux state is done later

    for (index = 0; index < bytesPerCollection; index++)
    {
        IoExpanderBytes[(index * 2) + ioDataStartIndex]     =  (collectionData.sectionBytes[index]);
        IoExpanderBytes[(index * 2) + 1 + ioDataStartIndex] = ~(collectionData.sectionBytes[index]);
    }

    if (ModuleCfg.auxActType != AUX_ACT_NOT_AVAILABLE)
    {
        //when AUX valve available the 16th driver is connected to the aux function.  
        //so section 16 is connnected to IOcontroller output 17.
      
       
        Bool_t auxValveExpState;          //state of the ioexpander aux valve connection
        UI08_t sectionByte;           
        
        if (((ModuleCfg.numSections * 2) % 8) == 0 || 
            (nrOfExpBytes < 4) )  
        {
            //no more bits left for shift operation, 2 extra bytes needed to send all io expander.
            nrOfExpBytes += 2; 
        }

        if (collectionId == SECTION_ID_1_56)
        {
           
           for (index = (bytesPerCollection-1); index > 1 ; index --)
           {
               sectionByte = (collectionData.sectionBytes[index] << 1) | 
                   ((collectionData.sectionBytes[index-1] >> 7) & (0xFF << 1 ));
               //shift all the bytes which come after the byte containing the aux valve Expander state
               IoExpanderBytes[(index*2)]       =   sectionByte;
               IoExpanderBytes[(index*2) + 1 ]  =  ~sectionByte;   
           }         
        }
        else
        {
            sectionByte = (collectionData.sectionBytes[bytesPerCollection - 1] << 1) | 
                ((IoExpanderBytes[12] >> 7) & (0xFF << 1 ));
            IoExpanderBytes[14] = sectionByte;
            IoExpanderBytes[15] = ~sectionByte;


            //shift all the bytes which come after the byte containing the aux valve Expander state
        }
            
        //check the type of aux act type, and determine the resulting status
        switch (ModuleCfg.auxActType)
        {
            case AUX_ACT_NOT_AVAILABLE:
                break;
            case FREE_WHEEL_VALVE:  //should be on when all sections are off.
                if (ModuleCfg.auxActLogicLevel == ACT_LOGIC_LOW)
                {
                    auxValveExpState = !allSectionsOff;
                }
                else
                {
                    auxValveExpState = allSectionsOff;
                }
                break;
            case PUMP_CONTROL:      //should be on when one of the sections is enabled.
                if (ModuleCfg.auxActLogicLevel == ACT_LOGIC_LOW)
                {
                    auxValveExpState = allSectionsOff;
                }
                else
                {
                    auxValveExpState = !allSectionsOff;
                }
                break;
            default:
                break;
        }

        //insert the status of the aux valve.
        
        IoExpanderBytes[2] = (IoExpanderBytes[2] & 0x7F) |
                                ((auxValveExpState?0x01:0x00) << 7);
        IoExpanderBytes[3] = ~(IoExpanderBytes[2]);

        ////Work around to use with switch-box need the status the 16-section.
        //switch (ModuleCfg.auxActType)
        //{
        //    case FREE_WHEEL_VALVE:
        //        if (ModuleCfg.auxActLogicLevel == ACT_LOGIC_LOW)
        //        {
        //            ((UI08_t*)collectionDataPtr)[1] = ~IoExpanderBytes[2];
        //        }
        //        else
        //        {
        //            ((UI08_t*)collectionDataPtr)[1] = IoExpanderBytes[2];
        //        }
        //        break;
        //    default:
        //        break;
        //}
    }

    writeIoExpanderData(collectionId,nrOfExpBytes);
    
    readIoExpanderData(collectionId, &readIoExpanderBytes[0],nrOfExpBytes);
    

    if (ModuleCfg.auxActLogicLevel == ACT_LOGIC_LOW)
    {
        for (index = 0; (index < nrOfExpBytes) && (index < 8); index ++)
        {
            collectionDataPtr->sectionBytes[index] = ~readIoExpanderBytes[index];
        }  
    }
    else
    {  
        for (index = 0; (index < nrOfExpBytes) && (index < 8); index ++)
        {
            collectionDataPtr->sectionBytes[index] = readIoExpanderBytes[index];
        }   
    }

}

/****************************************************************************/
/**                                                                        **/
/**                     LOCAL FUNCTIONS                                    **/
/**                                                                        **/
/****************************************************************************/

/*************************************************************************
* function:     writeIOExpanderData   
* parameters:   
*
*
* returns:      void
* comments:     
*
**************************************************************************/
static void writeIoExpanderData(SectionCollectionId_t collectionId, UI08_t totalBytes)
{
    UI08_t index;
    if (collectionId == SECTION_ID_1_56)  
    {
        for (index = 0 ; (index < totalBytes) && (index < 14) && (index < (IoExpanderGetNrOfIoBytes())) ; index ++)
        {
            IoExpanderWriteRegister(index, IoExpanderBytes[index]);
        }    
    }
    else
    {
        for (index = 14 ; (index < (totalBytes - 14)) && (index < (IoExpanderGetNrOfIoBytes())); index ++)
        {
            IoExpanderWriteRegister(index, IoExpanderBytes[index-14]);
        }    
    }
}

/*************************************************************************
* function:     readIoExpanderData   
* parameters:   
*
*
* returns:      void
* comments:     
*
**************************************************************************/
static void readIoExpanderData(SectionCollectionId_t collectionId, UI08_t *readData, UI08_t totalBytes)
{
    UI08_t index, index2;
    UI08_t tmpData[16];
    memset(&tmpData[0] , 0, 16);
    
    //extra byte to configure if aux valve, section 56 will be in 8th byte
    if (collectionId == SECTION_ID_1_56)  
    {
        for (index = 0 ; (index < totalBytes) && (index < 14) && (index < (IoExpanderGetNrOfIoBytes())) ; index ++)
        {
            tmpData[index] = IoExpanderReadRegister(index);
        }
    }
    else
    {  
        for (index = 14 ; (index < (totalBytes - 14)) && (index < (IoExpanderGetNrOfIoBytes())) ; index ++)
        {
            tmpData[index-14] = IoExpanderReadRegister(index);
        }   
    }
    // No support for section 56 to 64
    index2 = 0;
    for (UI08_t index1 = 0; ((index1 < 4) && (index2 < 8)); index1++)
    {
        tmpData[index2+1] = ~tmpData[index2+1];
        if(tmpData[index2] == tmpData[index2+1])
        {
            readData[index1] = tmpData[index2+1];
        }      
        index2 = index2 + 2;
    }
}

/****************************************************************************/
/**                                                                        **/
/**                               EOF                                      **/
/**                                                                        **/
/****************************************************************************/
