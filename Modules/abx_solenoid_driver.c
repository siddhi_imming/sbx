/*
                             *******************
******************************* C HEADER FILE *******************************
**                           *******************                           **
**                                                                         **
** project   : General modules                                             **
** filename  : abx_solenoid_driver.c                                                 
**                                                                         **
*****************************************************************************
**       Property of SBG Precision Farming    All rights reserved          **
*****************************************************************************

VERSION HISTORY:
----------------

Version     : 1
Date        : 20130308
Revised by  : Koen Smeuninx
Description : * Original version.

Global usage description:
-------------------------

This unit is the driver for an actuator box equipped with a solenoid driver
daughter board.  It accepts section commands and translates it into solenoid 
driver commands.

*/

/****************************************************************************/
/**                                                                        **/
/**                     MODULES USED                                       **/
/**                                                                        **/
/****************************************************************************/

#include "abx_solenoid_driver.h"
#include "lpc17xx_spi.h"
#include "io_expander_spi.h"

/****************************************************************************/
/**                                                                        **/
/**                     DEFINITIONS AND MACROS                             **/
/**                                                                        **/
/****************************************************************************/

/****************************************************************************/
/**                                                                        **/
/**                     TYPEDEFS AND STRUCTURES                            **/
/**                                                                        **/
/****************************************************************************/

/****************************************************************************/
/**                                                                        **/
/**                     PROTOTYPES OF LOCAL FUNCTIONS                      **/
/**                                                                        **/
/****************************************************************************/

static void writeIoExpanderData(SectionCollectionId_t collectionId, UI08_t totalBytes);
static void readIoExpanderData(SectionCollectionId_t collectionId, UI08_t *readData, UI08_t totalBytes);

/****************************************************************************/
/**                                                                        **/
/**                     EXPORTED VARIABLES                                 **/
/**                                                                        **/
/****************************************************************************/


/****************************************************************************/
/**                                                                        **/
/**                     GLOBAL VARIABLES                                   **/
/**                                                                        **/
/****************************************************************************/

static ModuleConfiguration_t ModuleCfg;
static UI08_t IoExpanderBytes[16]; 

/****************************************************************************/
/**                                                                        **/
/**                     EXPORTED FUNCTIONS                                 **/
/**                                                                        **/
/****************************************************************************/

/*************************************************************************
* function: ConfigDriverSolenoid  
* comments: Configures the solenoid driver.  
**************************************************************************/
void ConfigDriverSolenoid(ModuleConfiguration_t *moduleConfigPtr)
{
    SectionCollectionData_t sectionCollectionData;
    ModuleCfg = *moduleConfigPtr;

    if (IoExpanderInit(FALSE))
    {
        for (UI08_t index = 0; index < IoExpanderGetNrOfIoBytes(); index++)
        {
            if (IoExpanderAlloc(IO_EXPANDER_SET_AS_OUTPUT, IO_EXPANDER_SET_AS_PULL_UP) == 0xFF)
            {
                // No IO expander free to init.
                break;
            }
        }
    }
    // Set all outputs to off.
    if (ModuleCfg.auxActLogicLevel == ACT_LOGIC_LOW)
    {
        memset(&sectionCollectionData,1,sizeof(sectionCollectionData));
    }
    else
    {
        memset(&sectionCollectionData,0,sizeof(sectionCollectionData));
    }
    SetSectionsSolenoid(SECTION_ID_1_56,     &sectionCollectionData);
    SetSectionsSolenoid(SECTION_ID_57_112,   &sectionCollectionData);
}

/*************************************************************************
* function:     SetSectionsSolenoid   
* parameters:   SectionCollectionId_t collectionId: indicates which collection
*               data is passed, 1-56 or 57_112
*               SectionCollectionData_t * sectionCollectionData:
*               SectionData, information of the sections.
* returns:      void
* comments:     This function translates section data into IO Expander data
*               It also takes into account if an Aux valve is available.  
*               This driver is written to be able to handle aux valves at 
*               different positions but the Aux valve is always connected to 
*               the 16th solenoid driver.
**************************************************************************/
void SetSectionsSolenoid(SectionCollectionId_t collectionId,SectionCollectionData_t *collectionDataPtr)
{
    I08_t index;
    Bool_t allSectionsOff = TRUE;
    //bytes needed to send all the section information. (aux valve not included)  1 section -> 1byte.
    //9 sections 2 bytes... 
    UI08_t nrOfExpBytes = 
        (ModuleCfg.numSections / 8) + 
        ((ModuleCfg.numSections % 8) == 0?0:1);
    UI08_t ioDataStartIndex;
    UI08_t bytesPerCollection = sizeof(*collectionDataPtr);
    UI08_t readIoExpanderBytes[7];
    SectionCollectionData_t collectionData = *collectionDataPtr;  //
   
    memset(&readIoExpanderBytes, 0, 7);

    switch(collectionId)
    {
        case SECTION_ID_1_56:
            ioDataStartIndex = 0;
            break;
        case SECTION_ID_57_112:
            ioDataStartIndex = bytesPerCollection;
            break;
        default:
            return;
    }
  
    
    for (index = 0; index < sizeof(collectionData); index++)
    {
        if (collectionData.sectionBytes[index] != 0)
        {
            allSectionsOff = FALSE;
        }
        //if section data active low, invert signals
        if (ModuleCfg.sectionLogicLevel == ACT_LOGIC_LOW)
        {
            collectionData.sectionBytes[index] = ~collectionData.sectionBytes[index];
        }
    }
    

    //when no AUX valve, copy the section data directly into the IOExpander data
    if (ModuleCfg.auxActType == AUX_ACT_NOT_AVAILABLE)
    {
       for (index = 0; (index < bytesPerCollection) && (index < nrOfExpBytes); index++)
       {
            IoExpanderBytes[index + ioDataStartIndex] = (collectionData.sectionBytes[index]);
       }
    }
    else
    {
        //when AUX valve available the 16th driver is connected to the aux function.  
        //so section 16 is connnected to IOcontroller output 17.
        
        UI08_t auxValveExpArrayIndex = 1; //change these variables when other positions needed.
        UI08_t auxValveExpBitIndex = 7;
        Bool_t auxValveExpState;          //state of the ioexpander aux valve connection
        UI08_t loopStartIndex;            
        
        if ((ModuleCfg.numSections % 8) == 0 || 
            (auxValveExpArrayIndex >= nrOfExpBytes) )  
        {
            //no more bits left for shift operation, extra byte needed to send all io expander.
            nrOfExpBytes ++; 
        }

           
        if ((nrOfExpBytes - ioDataStartIndex) < bytesPerCollection)
        {
            loopStartIndex = nrOfExpBytes- ioDataStartIndex - 1;
        }
        else
        {
            loopStartIndex = bytesPerCollection-1;
        }


        for (index = loopStartIndex; index >= 0; index--)
        {       
             //shift all the bytes which come after the byte containing the aux valve Expander state
            IoExpanderBytes[index] =    
                      (collectionData.sectionBytes[index-1] >> 7) | 
                      (IoExpanderBytes[index] & (0xFF << 1 ));
            
            if ((index - 1 + ioDataStartIndex) <= auxValveExpArrayIndex)
            {
                break;  //index reached or index zero.  
            }
            IoExpanderBytes[index-1 + ioDataStartIndex] =  (collectionData.sectionBytes[index-1] << 1);
        }
        
        //if loop exited, loop trough remaining part and copy the data
        for (;index > 0; index--)
        {
            IoExpanderBytes[index- 1 + ioDataStartIndex] =    collectionData.sectionBytes[index-1];
        }

        //if startindex > 0 the data for the previous bit is not available in the 
        //Sectionbytes, get the old value from the status of the ioexpander bytes.  
        if (ioDataStartIndex > 0)
        {
            //shift fist byte and add buffered bit from previous expander byte
            IoExpanderBytes[ioDataStartIndex] = (collectionData.sectionBytes[0] << 1) | 
                (IoExpanderBytes[ioDataStartIndex] & 0x01);
        }
    
             
        //check the type of aux act type, and determine the resulting status
        switch (ModuleCfg.auxActType)
        {
            case AUX_ACT_NOT_AVAILABLE:
                break;
            case FREE_WHEEL_VALVE:  //should be on when all sections are off.
                if (ModuleCfg.auxActLogicLevel == ACT_LOGIC_LOW)
                {
                    auxValveExpState = !allSectionsOff;
                }
                else
                {
                    auxValveExpState = allSectionsOff;
                }
                break;
            case PUMP_CONTROL:      //should be on when one of the sections is enabled.
                if (ModuleCfg.auxActLogicLevel == ACT_LOGIC_LOW)
                {
                    auxValveExpState = allSectionsOff;
                }
                else
                {
                    auxValveExpState = !allSectionsOff;
                }
                break;
            default:
                break;
        }

        //insert the status of the aux valve.
        IoExpanderBytes[auxValveExpArrayIndex] =  
          (   
            ( (collectionData.sectionBytes[auxValveExpArrayIndex] << 1) & (0xFF << (auxValveExpBitIndex + 1) ) ) |
            (  collectionData.sectionBytes[auxValveExpArrayIndex] & (0xFF >> (8 - auxValveExpBitIndex ))    ) |
            ( (auxValveExpState?0x01:0x00) << auxValveExpBitIndex  )
          );
    }

    ////Work around to use with switch-box need the status the 16-section.
    //switch (ModuleCfg.auxActType)
    //{
    //    case FREE_WHEEL_VALVE:
    //        if (ModuleCfg.auxActLogicLevel == ACT_LOGIC_LOW)
    //        {
    //            ((UI08_t*)collectionDataPtr)[1] = ~IoExpanderBytes[1];
    //        }
    //        else
    //        {
    //            ((UI08_t*)collectionDataPtr)[1] = IoExpanderBytes[1];
    //        }
    //        break;
    //    default:
    //        break;
    //}
    writeIoExpanderData(collectionId,nrOfExpBytes);
    

    readIoExpanderData(collectionId, &readIoExpanderBytes[0],nrOfExpBytes);


    if (ModuleCfg.auxActLogicLevel == ACT_LOGIC_LOW)
    {
        for (index = 0; (index < nrOfExpBytes) && (index < 8); index ++)
        {
            collectionDataPtr->sectionBytes[index] = ~readIoExpanderBytes[index];
        }  
    }
    else
    {  
        for (index = 0; (index < nrOfExpBytes) && (index < 8); index ++)
        {
            collectionDataPtr->sectionBytes[index] = readIoExpanderBytes[index];
        }   
    }
}

/****************************************************************************/
/**                                                                        **/
/**                     LOCAL FUNCTIONS                                    **/
/**                                                                        **/
/****************************************************************************/

/*************************************************************************
* function:     writeIOExpanderData   
* parameters:   
*
*
* returns:      void
* comments:     
*
**************************************************************************/
static void writeIoExpanderData(SectionCollectionId_t collectionId, UI08_t totalBytes)
{
    UI08_t index;
    //extra byte to configure if aux valve, section 56 will be in 8th byte
    if (collectionId == SECTION_ID_1_56)  
    {
        for (index = 0 ; (index < totalBytes) && (index < 8) && (index < (IoExpanderGetNrOfIoBytes())) ; index ++)
        {
            IoExpanderWriteRegister(index, IoExpanderBytes[index]);
        }    
    }
    else
    {
        for (index = 7 ; (index < (totalBytes - 7)) && (index < (IoExpanderGetNrOfIoBytes())) ; index ++)
        {
            IoExpanderWriteRegister(index, IoExpanderBytes[index-7]);
        }    
    }
}


/*************************************************************************
* function:     readIoExpanderData   
* parameters:   
*
*
* returns:      void
* comments:     
*
**************************************************************************/
static void readIoExpanderData(SectionCollectionId_t collectionId, UI08_t *readData, UI08_t totalBytes)
{
    UI08_t index;
    if (collectionId == SECTION_ID_1_56)  
    {
        for (index = 0; (index < totalBytes) && (index < 8) && (index < (IoExpanderGetNrOfIoBytes())); index ++)
        {
            readData[index] = IoExpanderReadRegister(index);
        }  
    }
    else
    {  
        for (index = 7; (index < (totalBytes - 7)) && (index < (IoExpanderGetNrOfIoBytes())); index ++)
        {
            readData[index] = IoExpanderReadRegister(index);;
        }   
    }
}
/****************************************************************************/
/**                                                                        **/
/**                               EOF                                      **/
/**                                                                        **/
/****************************************************************************/
