/*
                             *******************
******************************* C HEADER FILE *******************************
**                           *******************                           **
**                                                                         **
** project   : sectionCTRLing controller LPC1768                           **
** filename  : ibs_dl_sectionCTRL.h                                        **                                                
**                                                                         **
*****************************************************************************
**                                                                         **
** Copyright (c) 2012, SBG Precision Farming/Navtronics Bvba               **
** All rights reserved.                                                    **
**                                                                         **
*****************************************************************************

VERSION HISTORY:
----------------
Version     : 2
Date        : 20121015
Revised by  : Koen Smeuninx
Description : * c style changes

Version     : 1
Date        : 20120614
Revised by  : Koen Smeuninx
Description : * Original version.

FILE INFORMATION:
-----------------
This module provides support for the ISO 11783 CAN protocol.
It implements the data layer for the sectionCTRLing controller unit. 
Only PROPRIETARY A commands are send to this unit. 
General Data layer functionality can be found in lbs_dl.h and lbs_dl.c
*/


#ifndef IBS_DL_AUX_UNIT_INCLUDED
#define IBS_DL_AUX_UNIT_INCLUDED

/****************************************************************************/
/**                                                                        **/
/**                     MODULES USED                                       **/
/**                                                                        **/
/****************************************************************************/
#include "can.h"
#include "ibs_dl.h"
#include "section_ctrl_defs.h"

/****************************************************************************/
/**                                                                        **/
/**                     DEFINITIONS AND MACROS                             **/
/**                                                                        **/
/****************************************************************************/

/****************************************************************************/
/**                                                                        **/
/**                     TYPEDEFS AND STRUCTURES                            **/
/**                                                                        **/
/****************************************************************************/

typedef struct AuxMessage_s
{
    Bool_t  enableUnit;
    UI08_t  input1;
    UI08_t  input2;
    UI16_t  runningCnt;
    UI08_t  reservedByte;       // 0x0B = amaclick, 0x09 = S-Box and 0xFF for ISO-Bus
}AuxMessage_t;

typedef enum AuxPartnerNames_e
{
    AUX_PARTNER_DIAG_SERV,
    AUX_PARTNER_MAX_PARTNERS
}AuxPartnerNames_t;

/****************************************************************************/
/**                                                                        **/
/**                     EXPORTED VARIABLES                                 **/
/**                                                                        **/
/****************************************************************************/

/****************************************************************************/
/**                                                                        **/
/**                     EXPORTED FUNCTIONS                                 **/
/**                                                                        **/
/****************************************************************************/
/*************************************************************************
* function:     IbsDlAuxUnitInit  
* parameters:   UI08_t Cid: Controller Id
* returns:      void    
* comments:     Init function of the isobus message handler       
**************************************************************************/
void IbsDlAuxUnitInit(UI08_t Cid);

/*****************************************************************************
*   function :      IbsSendAuxiliaryInputStatus
*   parameters:     Bool_t enableUnit: If this is true the spr5ayer will lissen to
*                   this message.
*                   UI08_t sectionBytesPtr: 2 bytes with 15 section and 1 pump
*   returns:        void
*   comments:       Sends the Auxiliary input status message of the ISO-Bus
*                   for setting the section ON & OFF like the S-Box and Amaclick 
*                   units.
****************************************************************************/
void IbsSendAuxiliaryInputStatus(AuxMessage_t *auxMessagePtr);


#endif

/****************************************************************************/
/**                                                                        **/
/**                              EOF                                       **/
/**                                                                        **/
/****************************************************************************/