/*
                             *******************
******************************* C HEADER FILE *******************************
**                           *******************                           **
**                                                                         **
** project   : sectionCTRLing controller LPC1768                           **
** filename  : ibs_dl_sectionCTRL.h                                        **                                                
**                                                                         **
*****************************************************************************
**                                                                         **
** Copyright (c) 2012, SBG Precision Farming/Navtronics Bvba               **
** All rights reserved.                                                    **
**                                                                         **
*****************************************************************************

VERSION HISTORY:
----------------
Version     : 2
Date        : 20121015
Revised by  : Koen Smeuninx
Description : * c style changes

Version     : 1
Date        : 20120614
Revised by  : Koen Smeuninx
Description : * Original version.

FILE INFORMATION:
-----------------
This module provides support for the ISO 11783 CAN protocol.
It implements the data layer for the sectionCTRLing controller unit. 
Only PROPRIETARY A commands are send to this unit. 
General Data layer functionality can be found in lbs_dl.h and lbs_dl.c
*/


#ifndef IBS_DL_SECTION_CTRL_INCLUDED
#define IBS_DL_SECTION_CTRL_INCLUDED

/****************************************************************************/
/**                                                                        **/
/**                     MODULES USED                                       **/
/**                                                                        **/
/****************************************************************************/
#include "can.h"
#include "ibs_dl.h"
#include "section_ctrl_defs.h"

/****************************************************************************/
/**                                                                        **/
/**                     DEFINITIONS AND MACROS                             **/
/**                                                                        **/
/****************************************************************************/
#define UNKNOWN_CONFIGURATION   0x00
#define RESERVED_CONFIGURATION  0xFF

// Proprietary A message definitions

/****************************************************************************/
/**                                                                        **/
/**                     TYPEDEFS AND STRUCTURES                            **/
/**                                                                        **/
/****************************************************************************/

typedef enum SpayerPartnerNames_e
{
    SPRAYER_PARTNER_SPAYER_MACHINE_CTRL,
    SPRAYER_PARTNER_DIAG_SERV,
    SPRAYER_PARTNER_MAX_PARTNERS
}SpayerPartnerNames_t;

typedef struct SpayerSystemInformation_s
{
    UI08_t controlByte;
    UI08_t machineDirection         :2;
    UI08_t autosteerSwitchCommand   :2;
    UI08_t wheelsteeringMode        :2;
    UI08_t manualSteeringDetection  :2;
    UI16_t wheelbasedSpeed;
    UI08_t DisplayDayNightMode      :2;
    UI08_t mainSwitchCommand        :2;
    UI08_t externalMainSwitchToggle :2;
    UI08_t ReservedBits             :2;
    UI08_t Reserved[3];
}SpayerSystemInformation_t;

enum MachineDirection_e
{
    MACHINE_DIRECTION_STOP_NEUTRAL,
    MACHINE_DIRECTION_REVERSE,
    MACHINE_DIRECTION_FORWARD,
    MACHINE_DIRECTION_NOT_AVAILABLE,
};

enum AutosteerSwitchCommand_e
{
    AUTOSTEER_SWITCH_COMMAND_NOT_PRESSED,
    AUTOSTEER_SWITCH_COMMAND_PRESSED,
};

enum DisplayStatus_e
{
    DISPLAY_STATUS_DAY_MODE,
    DISPLAY_STATUS_NIGHT_MODE,
    DISPLAY_STATUS_RESERVED,
    DISPLAY_STATUS_NOT_AVAILABLE,
};

enum WheelsteeringMode_e
{
    WHEELSTEERING_MODE_2WHEEL_STEERING,
    WHEELSTEERING_MODE_4WHEEL_STEERING,
    WHEELSTEERING_MODE_RESERVED,
    WHEELSTEERING_MODE_NOT_AVAILABLE,
};

enum ManualSteeringDetection_e
{
    MANUAL_STEERING_NOT_DETECTED,
    MANUAL_STEERING_DETECTED,
};

enum SwitchStatus_e
{
    SWITCH_OFF,
    SWITCH_ON,
    SWITCH_ERROR,
    SWITCH_NOT_AVAILABLE,
};

/****************************************************************************/
/**                                                                        **/
/**                     EXPORTED VARIABLES                                 **/
/**                                                                        **/
/****************************************************************************/


/****************************************************************************/
/**                                                                        **/
/**                     EXPORTED FUNCTIONS                                 **/
/**                                                                        **/
/****************************************************************************/
/*************************************************************************
* function:     IbsDlSectionCtrltInit  
* parameters:   UI08_t Cid: Controller Id
* returns:      void    
* comments:     Init function of the isobus message handler       
**************************************************************************/
void IbsDlSectionCtrltInit(UI08_t Cid);

/*****************************************************************************
* function:     IbsDlPartnerHandler
* parameters:   CanMsg_t *msgPtr: pointer to can message.
* returns:      void
* comments:     Handler function for the incoming can terminal messages
****************************************************************************/
void IbsDlPartnerHandler(CanMsg_t *msgPtr); //this function entry is needed to pass to the init function

/*************************************************************************
* function:     BootCmdHandler  
* parameters:   CanMsg_t *pMsg
* returns:      void
* comments:     Handler function for the incoming Diagnostic tool service
**************************************************************************/
void BootCmdHandler(CanMsg_t *pMsg);

/*************************************************************************
* function:     IbsSendCurrentSectionStatus  
* parameters:   SectionCollectionId_t collectionId: identifier of the section
*               collection
*               SectionCollectionData_t *sectionDataPtr: array of uints containing section
*               status bits.
* returns:      void
* comments:     send current section information over the isobus
**************************************************************************/
void IbsSendCurrentSectionStatus(SectionCollectionId_t collectionId, SectionCollectionData_t *sectionDataPtr);


void IbsSendScuCommand(UI08_t *statusByte);

/*************************************************************************
* function:     IbsDlSendBoomConfiguration 
* parameters:   Bool_t fullConfig: 
* returns:      void
* comments:     this function sends the boom configuration, depending on
*               fullconfig only the BOOM config I is send or the complete
*               configuration.
**************************************************************************/
void IbsDlSendBoomConfiguration(Bool_t fullConfig);


/*************************************************************************
* function:     IbsDlSendSprayerSystemInformation 
* parameters:   SpayerSystemInformation_t SpayerSystemInformation: 
*               message to send
* returns:      void
* comments:     this function sends the Sprayer System Information to 
*               sprayer machine control.
**************************************************************************/
void IbsDlSendSprayerSystemInformation(SpayerSystemInformation_t SpayerSystemInformation);



#endif

/****************************************************************************/
/**                                                                        **/
/**                              EOF                                       **/
/**                                                                        **/
/****************************************************************************/