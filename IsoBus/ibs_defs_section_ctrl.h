/*
                             *******************
******************************* C HEADER FILE *******************************
**                           *******************                           **
**                                                                         **
** project   : General modules                                             **
** filename  : ibs_defs_section_ctrl.h                                                  
**                                                                         **
*****************************************************************************
**                                                                         **
** Copyright (c) 2012, SBG Precision Farming/Navtronics Bvba               **
** All rights reserved.                                                    **
**                                                                         **
*****************************************************************************

VERSION HISTORY:
----------------

Version     : 1
Date        : 20130122
Revised by  : Koen Smeuninx
Description : * Original version


FILE INFORMATION:
-----------------
Contains type definitions and defines for the isobus
*/

#ifndef _IBS_DEFS_SECTION_CTRL_INCLUDED
#define _IBS_DEFS_SECTION_CTRL_INCLUDED

/****************************************************************************/
/**                                                                        **/
/**                     MODULES USED                                       **/
/**                                                                        **/
/****************************************************************************/
#include "section_ctrl_defs.h"

/****************************************************************************/
/**                                                                        **/
/**                     DEFINITIONS AND MACROS                             **/
/**                                                                        **/
/****************************************************************************/

/****************************************************************************/
/**                                                                        **/
/**                     TYPEDEFS AND STRUCTURES                            **/
/**                                                                        **/
/****************************************************************************/

#pragma pack(1)


//definition of control bytes

//prefix CBTX to avoid name collision
typedef enum ControlByteTx_e
{   
    CBTX_SECTION_OPERATING_STATUS_0_56       = 0x01,
    CBTX_SECTION_OPERATING_STATUS_57_112     = 0x02,
    CBTX_SECTION_STATUS_0_56                 = 0x11,
    CBTX_SECTION_STATUS_57_112               = 0x12,
    CBTX_BOOM_CONFIGURATION_I                = 0x20,
    CBTX_SECTION_CONFIGURATION               = 0x21,
    CBTX_MEASURED_APPLICATION_RATE           = 0x22,
    CBTX_BOOM_CONFIGURATION_II               = 0x23,
    CBTX_BOOM_CONFIGURATION_III              = 0x24
}ControlByteTx_t;

//prefix CBRX to avoid name collision
typedef enum ControlByteRx_e
{
    CBRX_SECTION_COMMAND_0_56                = 0x01,
    CBRX_SECTION_COMMAND_57_112              = 0x02,
    CBRX_REQUEST_BOOM_CONFIGURATION          = 0x20,
    CBRX_REQUEST_SECTION_CONFIGURATION       = 0x21,
    CBRX_DESIRED_APPLICATION_RATE            = 0x22,
    CBRX_STATUS_INFORMATION                  = 0x23,
    CBRX_BOOM_CONFIGURATION_I                = 0x30,
    CBRX_BOOM_CONFIGURATION_II               = 0x31,
    CBRX_BOOM_CONFIGURATION_III              = 0x32,
    CBRX_SECTION_CONFIGURATION               = 0x35
}ControlByteRx_t;



typedef struct UnsignedFloating_s
{
    UI16_t  value;
    I08_t   resolutionExponent;  //resulting value = (10)^resolutionExponent
}UnsignedFloating_t;

typedef struct SectionOperationStatusMsg_s
{
    UI08_t operatingStatus[7];     //first byte bit 1 = Operating status for section 1, ..., bit 8 = section 8
    //second byte bit 1 = Operating status for section 9, ..., bit 8 = section 16
    //seventh byte bit 1 = operating status for section 49, ...,  bit 8 = section 56
}SectionOperationMsg_t;

typedef struct SectionStatusMsg_s
{
    UI08_t  sectionStatus[7];       //position to section id analog to operation status message.
}SectionStatusMsg_t;

typedef struct SectionCommandMsg_s
{
    UI08_t  sectionCommand[7];      //position to section id analog to operation status message.
}SectionCommandMsg_t;

typedef struct RequestBoomConfigMsg_s
{
    UI08_t  configurationExchangeKey;
}RequestBoomConfigMsg_t;

typedef struct RequestSectionConfigMsg_s
{
    SectionIdentifier_t sectionIdentifier;   
}RequestSectionConfigMsg_t;

typedef union RequestMessagesData_u
{
    SectionOperationMsg_t       sectionOperationMsg;
    SectionStatusMsg_t          sectionStatusMsg; 
    SectionCommandMsg_t         sectionCommandMsg;
    RequestBoomConfigMsg_t      requestBoomConfigMsg;
    RequestSectionConfigMsg_t   requestSectionConfigMsg;
}RequestMessageData_t;

typedef struct IbsRxMessage_s
{
    ControlByteRx_t     messageId;
    RequestMessageData_t messageData;
}IbsRxMessage_t;

#pragma pack()


/****************************************************************************/
/**                                                                        **/
/**                     EXPORTED VARIABLES                                 **/
/**                                                                        **/
/****************************************************************************/


/****************************************************************************/
/**                                                                        **/
/**                     EXPORTED FUNCTIONS                                 **/
/**                                                                        **/
/****************************************************************************/


#endif

/****************************************************************************/
/**                                                                        **/
/**                              EOF                                       **/
/**                                                                        **/
/****************************************************************************/
