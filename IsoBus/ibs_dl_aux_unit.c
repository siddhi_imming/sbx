/*
                             *******************
******************************* C SOURCE FILE *******************************
**                           *******************                           **
**                                                                         **
** project   : Amaclick S-Box data layer                                   **
** filename  : ibs_dl_aux_unit.c                                        **
**                                                                         **
*****************************************************************************
**                                                                         **
** Copyright (c) 2012, SBG Precision Farming/Navtronics Bvba               **
** All rights reserved.                                                    **
**                                                                         **
*****************************************************************************

VERSION HISTORY:
----------------

Version     : 1
Date        : 20130311
Revised by  : Siddhi Imming
Description : * Original version.

FILE INFORMATION:
-----------------
This module provides support for the ISO 11783 CAN protocol.
It implements the data layer for the Aux unit for Amaclick and S-Box. 
Only PROPRIETARY A commands are send to this unit. 
General Data layer functionality can be found in lbs_dl.h and lbs_dl.c
*/

/****************************************************************************/
/**                                                                        **/
/**                     MODULES USED                                       **/
/**                                                                        **/
/****************************************************************************/

#include "ibs_dl_aux_unit.h"
#include "ibs_defs_section_ctrl.h"
#include "sysconf.h"
#include "config.h"
#include "data_check.h"
#include "reset.h"
#include "section_ctrl_task.h"

/****************************************************************************/
/**                                                                        **/
/**                     DEFINITIONS AND MACROS                             **/
/**                                                                        **/
/****************************************************************************/

/****************************************************************************/
/**                                                                        **/
/**                     TYPEDEFS AND STRUCTURES                            **/
/**                                                                        **/
/****************************************************************************/

/****************************************************************************/
/**                                                                        **/
/**                     PROTOTYPES OF LOCAL FUNCTIONS                      **/
/**                                                                        **/
/****************************************************************************/

static void partnerHandler(CanMsg_t *msgPtr);
static void bootCmdHandler(CanMsg_t *msgPtr);

static void handleConfiguration (UI08_t cId, UI08_t pId, CanMsg_t *pMsg);
static void handlePropAMessage(CanMsg_t *msgPtr);
static void handlePropBMessage(CanMsg_t *msgPtr);

static void ibsDlAuxUnitFillPartnerData(IbsPartnerData_t * partnerDataPtr);

/****************************************************************************/
/**                                                                        **/
/**                     GLOBAL VARIABLES                                   **/
/**                                                                        **/
/****************************************************************************/

static UI08_t AuxUnitCid;
static UI08_t PartnerIds[AUX_PARTNER_MAX_PARTNERS];

/****************************************************************************/
/**                                                                        **/
/**                     EXPORTED FUNCTIONS                                 **/
/**                                                                        **/
/****************************************************************************/
void IbsDlAuxUnitInit(UI08_t cId)
{
    int index;
    IbsPartnerData_t PartnerData[AUX_PARTNER_MAX_PARTNERS];

    ibsDlAuxUnitFillPartnerData(PartnerData);
    AuxUnitCid = cId;
    for (index = 0; index < AUX_PARTNER_MAX_PARTNERS; index++)
    {
        PartnerIds[index] = IbsNmSetPartnerHandler(    
            AuxUnitCid,
            &PartnerData[index]);
        if (PartnerIds[index] == AUX_PARTNER_MAX_PARTNERS)
        {
            //error
            return;
        }
    }  
}

/*****************************************************************************
*   function :      IbsSendAuxiliaryInputStatus
*   parameters:     Bool_t enableUnit: If this is true the spr5ayer will lissen to
*                   this message.
*                   UI08_t sectionBytesPtr: 2 bytes with 15 section and 1 pump
*   returns:        void
*   comments:       Sends the Auxiliary input status message of the ISO-Bus
*                   for setting the section ON & OFF like the S-Box and Amaclick 
*                   units.
****************************************************************************/
void IbsSendAuxiliaryInputStatus(AuxMessage_t *auxMessagePtr)
{
    CanFrame_t frm;
    frm.id = 0x18E6FF00;
    frm.dlc = 8;

    frm.bytes[0] = 0x21;
    frm.bytes[1] = 0xFA;
    frm.bytes[2] = auxMessagePtr->input1;
    frm.bytes[3] = auxMessagePtr->input2;
    memcpy(frm.bytes+4, &auxMessagePtr->runningCnt,2);
    frm.bytes[6] = 0x00 | auxMessagePtr->enableUnit;
    frm.bytes[7] = auxMessagePtr->reservedByte;

    IbsNmSendFrame(AuxUnitCid, &frm);
}

/****************************************************************************/
/**                                                                        **/
/**                     LOCAL FUNCTIONS                                    **/
/**                                                                        **/
/****************************************************************************/

/*****************************************************************************
*   function :      partnerHandler
*   parameters:     void
*   returns:        void
*   comments:       Handler function for the incoming can terminal messages
****************************************************************************/
static void partnerHandler(CanMsg_t *msgPtr)
{
    if (PGN_IS_PDU2(msgPtr->pgn))
    {
        handlePropBMessage(msgPtr);
    }
    else
    {
        switch (msgPtr->pgn) 
        { 
        case PGN_PROPRIETARY_A:
            handlePropAMessage(msgPtr);
            break;
        default:
            break;
        } 
    }
}

/*****************************************************************************
*   function :      handlePropAMessage
*   parameters:     CanMsg_t *msgPtr
*   returns:        void
*   comments:       
****************************************************************************/
static void handlePropAMessage(CanMsg_t *msgPtr)
{
    switch(msgPtr->bytesPtr[0])
    {
        case PA_CONFIG:
            handleConfiguration(AuxUnitCid, PartnerIds[AUX_PARTNER_DIAG_SERV], msgPtr);
            break;
        case RESET_SIGNAL:
            if (msgPtr->bytesPtr[1] == 0x00)
            {
                OS_Delay(100);
                Reset();
            }
            break;
        case 0xFF:
            break;
        case CBRX_SECTION_COMMAND_0_56:
        case CBRX_SECTION_COMMAND_57_112:
        case CBRX_REQUEST_BOOM_CONFIGURATION:
        case CBRX_REQUEST_SECTION_CONFIGURATION:
        case CBRX_DESIRED_APPLICATION_RATE:
 //       case CBRX_STATUS_INFORMATION:
        case CBRX_BOOM_CONFIGURATION_I:
        case CBRX_BOOM_CONFIGURATION_II:
        case CBRX_BOOM_CONFIGURATION_III:
        case CBRX_SECTION_CONFIGURATION:
            break;  
    }
}

/*****************************************************************************
*   function :      handlePropBMessage
*   parameters:     CanMsg_t *msgPtr
*   returns:        void
*   comments:       
****************************************************************************/
static void handlePropBMessage(CanMsg_t *msgPtr)
{
    if (msgPtr->pgn == PGN_PRO_B_SECTION_CTRL ) //TODO check source address
    {
        switch(msgPtr->bytesPtr[0])
        {
            case CBRX_SECTION_COMMAND_0_56:
            case CBRX_SECTION_COMMAND_57_112:
            case CBRX_STATUS_INFORMATION:
            break;  

        }
    }
}


/*****************************************************************************
*   function :      BootCmdHandler
*   parameters:     void
*   returns:        void
*   comments:       Handler function for the incoming Diagnostic tool service
****************************************************************************/
static void bootCmdHandler(CanMsg_t *msgPtr)
{
    switch(msgPtr->bytesPtr[0])
    {
        case PA_CONFIG:
            handleConfiguration(AuxUnitCid, PartnerIds[AUX_PARTNER_DIAG_SERV], msgPtr);
            break;
        case RESET_SIGNAL:
            if (msgPtr->bytesPtr[1] == 0x00)
            {
                OS_Delay(100);
                Reset();
            }
            break;
            break;
    }
}

/*****************************************************************************
*   function :      handleConfiguration
*   parameters:     UI08_t cId: Controller identifier
*                   UI08_t pId: terminal identifier
                    CanMsg_t *pMsg: Data bytes of the message
*   returns:        void
*   comments:       Handler function for the incoming can terminal messages
****************************************************************************/
static void handleConfiguration (UI08_t cId, UI08_t pId, CanMsg_t *msgPtr)
{
    UI16_t paramId;
    UI08_t paramType;
    UI08_t opMode;

    opMode = msgPtr->bytesPtr[1]&0x07;

    if (DataCheck3Bit(opMode) == DATA_VALID) 
    {
        memcpy(&paramId, msgPtr->bytesPtr+2, 2);

        switch (opMode) 
        {
            case CFG_RQ_NR_PARAM:
                if (cId == AuxUnitCid) 
                {
                   // CfgReportNumParameters(cId, pId, CFG_NR_PARAMS);
                }
                break;
            case CFG_SET_DEFAULTS:
                CfgSetGroupToDefault((ParameterGroup_t)(msgPtr->bytesPtr[2]));
                // Report
                if (cId == AuxUnitCid) 
                {
                     ReportAllParameters(cId, pId);                   
                }
                break;
            case CFG_RQ_PARAM:
                if (DataCheckWord(paramId) == DATA_VALID) 
                {
                    if (paramId == DATA_MAX_VALID_WORD) 
                    {
                        if (cId == AuxUnitCid) 
                        {
                            ReportAllParameters(cId, pId);
                        }
                    }
                    else 
                    {
                        CfgReportParameter(cId, pId, paramId);
                    }
                }
                break;
            case CFG_MOD_PARAM:
                if (DataCheckWord(paramId) == DATA_VALID) 
                {
                    paramType = (msgPtr->bytesPtr[1]&0x38) >> 3;
                    if (DataCheck3Bit(paramType) == DATA_VALID) 
                    {
                        CfgModifyParameter(cId, pId, paramId, paramType, msgPtr->bytesPtr+4);
                    }
                }
                break;  
            case CFG_CLEAR_EEPROM:
                if (cId == AuxUnitCid) 
                {
                    //CfgClearEeprom();
                    ReportAllParameters(cId, pId);                   
                }
                break;
        }
    }
}


/*************************************************************************
* function: ibsDlSteerFillPartnerData    
* comments: fill the partner data array;
**************************************************************************/
static void ibsDlAuxUnitFillPartnerData(IbsPartnerData_t * partnerDataPtr)
{  
    partnerDataPtr[AUX_PARTNER_DIAG_SERV].name.identityNo              = 0;     
    partnerDataPtr[AUX_PARTNER_DIAG_SERV].name.manfCode                = NAVTRONICS;
    partnerDataPtr[AUX_PARTNER_DIAG_SERV].name.ecuInst                 = 0; 
    partnerDataPtr[AUX_PARTNER_DIAG_SERV].name.functionInst            = 0;
    partnerDataPtr[AUX_PARTNER_DIAG_SERV].name.function                = ON_BOARD_DIAGNOSTIC_SERVICE_TOOL;                      
    partnerDataPtr[AUX_PARTNER_DIAG_SERV].name.reserved                = 0;
    partnerDataPtr[AUX_PARTNER_DIAG_SERV].name.deviceClass             = NON_SPECIFIC;
    partnerDataPtr[AUX_PARTNER_DIAG_SERV].name.deviceClassInst         = 0;
    partnerDataPtr[AUX_PARTNER_DIAG_SERV].name.industryGroup           = GLOBAL;  
    partnerDataPtr[AUX_PARTNER_DIAG_SERV].name.selfConfigSA            = TRUE;

    partnerDataPtr[AUX_PARTNER_DIAG_SERV].mask.identityNo              = MASK_IDENTITY_NO_DISABLED;
    partnerDataPtr[AUX_PARTNER_DIAG_SERV].mask.manfCode                = MASK_MANF_CODE_DISABLED;
    partnerDataPtr[AUX_PARTNER_DIAG_SERV].mask.ecuInst                 = MASK_ECU_INST_DISABLED;
    partnerDataPtr[AUX_PARTNER_DIAG_SERV].mask.functionInst            = MASK_FUNCTION_INST_ENABLED;
    partnerDataPtr[AUX_PARTNER_DIAG_SERV].mask.function                = MASK_FUNCTION_ENABLED;
    partnerDataPtr[AUX_PARTNER_DIAG_SERV].mask.reserved                = MASK_RESERVED_DISABLED;
    partnerDataPtr[AUX_PARTNER_DIAG_SERV].mask.deviceClass             = MASK_DEVICE_CLASS_ENABLED;
    partnerDataPtr[AUX_PARTNER_DIAG_SERV].mask.deviceClassInst         = MASK_DEVICE_CLASS_INST_DISABLED;
    partnerDataPtr[AUX_PARTNER_DIAG_SERV].mask.industryGroup           = MASK_INDUSTRY_GROUP_ENABLED;
    partnerDataPtr[AUX_PARTNER_DIAG_SERV].mask.selfConfigSA            = MASK_SELF_CONFIG_SA_ENABLED;
    partnerDataPtr[AUX_PARTNER_DIAG_SERV].msgHandlerPtr                = bootCmdHandler; 

    partnerDataPtr[AUX_PARTNER_DIAG_SERV].partnerTimeOut               = PARTNER_TIMEOUT_INFINITE;  //disabled

    //stop of partnerdata fill

}


/****************************************************************************/
/**                                                                        **/
/**                              EOF                                       **/
/**                                                                        **/
/****************************************************************************/



