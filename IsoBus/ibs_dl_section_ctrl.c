/*
                             *******************
******************************* C SOURCE FILE *******************************
**                           *******************                           **
**                                                                         **
** project   : sectionCTRLing Controller LPC1768                           **
** filename  : ibs_dl_section_ctrl.c                                       **
**                                                                         **
*****************************************************************************
**                                                                         **
** Copyright (c) 2012, SBG Precision Farming/Navtronics Bvba               **
** All rights reserved.                                                    **
**                                                                         **
*****************************************************************************

VERSION HISTORY:
----------------

Version     : 2
Date        : 20121016
Revised by  : Koen Smeuninx 
Description : c style applied

Version     : 1
Date        : 20120104
Revised by  : Koen Smeuninx
Description : * Original version.

FILE INFORMATION:
-----------------
This module provides support for the ISO 11783 CAN protocol.
It implements the data layer for the sectionCTRLing controller unit. 
Only PROPRIETARY A commands are send to this unit. 
General Data layer functionality can be found in lbs_dl.h and lbs_dl.c
*/

/****************************************************************************/
/**                                                                        **/
/**                     MODULES USED                                       **/
/**                                                                        **/
/****************************************************************************/
#include "ibs_dl_section_ctrl.h"
#include "ibs_defs_section_ctrl.h"
#include "sysconf.h"
#include "config.h"
#include "data_check.h"
#include "reset.h"
#include "section_ctrl_task.h"

/****************************************************************************/
/**                                                                        **/
/**                     DEFINITIONS AND MACROS                             **/
/**                                                                        **/
/****************************************************************************/

/****************************************************************************/
/**                                                                        **/
/**                     TYPEDEFS AND STRUCTURES                            **/
/**                                                                        **/
/****************************************************************************/

/****************************************************************************/
/**                                                                        **/
/**                     PROTOTYPES OF LOCAL FUNCTIONS                      **/
/**                                                                        **/
/****************************************************************************/
static void handleConfiguration (UI08_t cId, UI08_t pId, CanMsg_t *pMsg);
static void handlePropAMessage(CanMsg_t *msgPtr);
static void handlePropBMessage(CanMsg_t *msgPtr);
static void handleScuCommand(CanMsg_t *msgPtr);
static void handleScuStatus(CanMsg_t *canMsgPtr);

static void ibsDlSectionCtrlFillPartnerData(IbsPartnerData_t * partnerDataPtr);
/****************************************************************************/
/**                                                                        **/
/**                     GLOBAL VARIABLES                                   **/
/**                                                                        **/
/****************************************************************************/
static UI08_t SectionCtrlCid;
static UI08_t PartnerIds[SPRAYER_PARTNER_MAX_PARTNERS];

/****************************************************************************/
/**                                                                        **/
/**                     EXPORTED FUNCTIONS                                 **/
/**                                                                        **/
/****************************************************************************/
void IbsDlSectionCtrltInit(UI08_t cId)
{
  
    int index;
    
    IbsPartnerData_t PartnerData[SPRAYER_PARTNER_MAX_PARTNERS];

    ibsDlSectionCtrlFillPartnerData(PartnerData);
    SectionCtrlCid = cId;
    for (index = 0; index < SPRAYER_PARTNER_MAX_PARTNERS; index++)
    {
        PartnerIds[index] = IbsNmSetPartnerHandler(    
            SectionCtrlCid,
            &PartnerData[index]);
        if (PartnerIds[index] == SPRAYER_PARTNER_MAX_PARTNERS)
        {
            //error
            return;
        }
    } 
}

/*****************************************************************************
*   function :      IbsSendScuCommand
*   parameters:     UI08_t statusByte: command for section controller
*   returns:        void
*   comments:       Sends an isobus frame to the partner with the 
*                   current status of the switches.
****************************************************************************/
void IbsSendScuCommand(UI08_t *statusByte)
{
    CanFrame_t frm;
    frm.id = 0x18000000 | (PGN_PRO_B_SECTION_CTRL << 8) ;
    frm.dlc = 8;
    frm.bytes[0] = CBRX_SECTION_COMMAND_0_56;
    frm.bytes[1] = statusByte[0];
    frm.bytes[2] = statusByte[1];
    frm.bytes[3] = 0x00;
    frm.bytes[4] = 0x00;
    frm.bytes[5] = 0x00;
    frm.bytes[6] = 0x00;
    frm.bytes[7] = 0x00;

    IbsNmSendFrame(SectionCtrlCid, &frm);
}

/*****************************************************************************
*   function :      sectionCTRLTerminalHandler
*   parameters:     void
*   returns:        void
*   comments:       Handler function for the incoming can terminal messages
****************************************************************************/
void IbsDlPartnerHandler(CanMsg_t *msgPtr)
{
    if (PGN_IS_PDU2(msgPtr->pgn))
    {
        handlePropBMessage(msgPtr);
    }
    else
    {
        switch (msgPtr->pgn) 
        { 
            case PGN_PROPRIETARY_A:
                handlePropAMessage(msgPtr);
                break;
            default:
                break;
        } 
    }
}



/*************************************************************************
* function:  IbsDlSendBoomConfiguration 
* comments:   
**************************************************************************/
void IbsDlSendBoomConfiguration(Bool_t fullConfig)
{
    int index;
    
    if ( Cfg.cfgFlash.sectionCtrlCfg.boomConfigurationI.numberOfSections > NUMBER_OF_SECTIONS_MAX  )
    {
        Cfg.cfgFlash.sectionCtrlCfg.boomConfigurationI.numberOfSections = 0;
    }

    //CfgReportBoomConfigurationI(SectionCtrlCid, PartnerIds[SPRAYER_PARTNER_SECTION_ON_OFF]);
    if (fullConfig)
    {
        //CfgReportBoomConfigurationII(SectionCtrlCid, PartnerIds[SPRAYER_PARTNER_SECTION_ON_OFF]);
        //CfgReportBoomConfigurationIII(SectionCtrlCid, PartnerIds[SPRAYER_PARTNER_SECTION_ON_OFF]);
        for (index = 1 ; index <= Cfg.cfgFlash.sectionCtrlCfg.boomConfigurationI.numberOfSections; index++)
        {
            //CfgReportSectionConfiguration(SectionCtrlCid, PartnerIds[SPRAYER_PARTNER_SECTION_ON_OFF],index);
        }
    }
  
    //OS_SignalEvent(SECTIONCTRL_START,&TcbSectionCtrlTask);
}

/*************************************************************************
* function:  IbsDlSendSprayerSystemInformation 
* comments:   
**************************************************************************/
void IbsDlSendSprayerSystemInformation(SpayerSystemInformation_t SpayerSystemInformation)
{
    CanFrame_t frame;
    frame.dlc = 8;
    frame.id = PGN_PROPRIETARY_A_DEST;
    SpayerSystemInformation.controlByte = 0x25;
    memcpy(frame.bytes, &SpayerSystemInformation, sizeof(frame.bytes));

    IbsNmSendPartnerFrame(SectionCtrlCid, PartnerIds[SPRAYER_PARTNER_SPAYER_MACHINE_CTRL], &frame);
}

/****************************************************************************/
/**                                                                        **/
/**                     LOCAL FUNCTIONS                                    **/
/**                                                                        **/
/****************************************************************************/

/*****************************************************************************
*   function :      handlePropAMessage
*   parameters:     CanMsg_t *msgPtr
*   returns:        void
*   comments:       
****************************************************************************/
static void handlePropAMessage(CanMsg_t *msgPtr)
{
    switch(msgPtr->bytesPtr[0])
    {
        case PA_CONFIG:
            //handleConfiguration(SectionCtrlCid, PartnerIds[SPRAYER_PARTNER_SECTION_ON_OFF], msgPtr);
            break;
        case RESET_SIGNAL:
            if (msgPtr->bytesPtr[1] == 0x00)
            {
                OS_Delay(100);
                Reset();
            }
            break;
        case 0xFF:
            break;
        case CBRX_SECTION_COMMAND_0_56:
        case CBRX_SECTION_COMMAND_57_112:
        case CBRX_REQUEST_BOOM_CONFIGURATION:
        case CBRX_REQUEST_SECTION_CONFIGURATION:
        case CBRX_DESIRED_APPLICATION_RATE:
 //       case CBRX_STATUS_INFORMATION:
        case CBRX_BOOM_CONFIGURATION_I:
        case CBRX_BOOM_CONFIGURATION_II:
        case CBRX_BOOM_CONFIGURATION_III:
        case CBRX_SECTION_CONFIGURATION:     
            handleScuCommand(msgPtr);
            break;  
    }
}

/*****************************************************************************
*   function :      handlePropBMessage
*   parameters:     CanMsg_t *msgPtr
*   returns:        void
*   comments:       
****************************************************************************/
static void handlePropBMessage(CanMsg_t *msgPtr)
{
    if (msgPtr->pgn == PGN_PRO_B_SECTION_CTRL ) //TODO check source address
    {
        switch(msgPtr->bytesPtr[0])
        {
        case CBTX_SECTION_STATUS_0_56:
            handleScuStatus(msgPtr);
            break;
        default:
            break;  

        }
    }
}

/*****************************************************************************
*   function :      BootCmdHandler
*   parameters:     void
*   returns:        void
*   comments:       Handler function for the incoming Diagnostic tool service
****************************************************************************/
void BootCmdHandler(CanMsg_t *msgPtr)
{
    switch(msgPtr->bytesPtr[0])
    {
        case PA_CONFIG:
            handleConfiguration(SectionCtrlCid, PartnerIds[SPRAYER_PARTNER_DIAG_SERV], msgPtr);
            break;
        case RESET_SIGNAL:
            if (msgPtr->bytesPtr[1] == 0x00)
            {
                OS_Delay(100);
                Reset();
            }
            break;
            break;
    }
}

/*****************************************************************************
*   function :      handleConfiguration
*   parameters:     UI08_t cId: Controller identifier
*                   UI08_t pId: terminal identifier
                    CanMsg_t *pMsg: Data bytes of the message
*   returns:        void
*   comments:       Handler function for the incoming can terminal messages
****************************************************************************/
static void handleConfiguration (UI08_t cId, UI08_t pId, CanMsg_t *msgPtr)
{
    UI16_t paramId;
    UI08_t paramType;
    UI08_t opMode;

    opMode = msgPtr->bytesPtr[1]&0x07;

    if (DataCheck3Bit(opMode) == DATA_VALID) 
    {
        memcpy(&paramId, msgPtr->bytesPtr+2, 2);

        switch (opMode) 
        {
            case CFG_RQ_NR_PARAM:
                if (cId == SectionCtrlCid) 
                {
                   // CfgReportNumParameters(cId, pId, CFG_NR_PARAMS);
                }
                break;
            case CFG_SET_DEFAULTS:
                CfgSetGroupToDefault((ParameterGroup_t)(msgPtr->bytesPtr[2]));
                // Report
                if (cId == SectionCtrlCid) 
                {
                     ReportAllParameters(cId, pId);                   
                }
                break;
            case CFG_RQ_PARAM:
                if (DataCheckWord(paramId) == DATA_VALID) 
                {
                    if (paramId == DATA_MAX_VALID_WORD) 
                    {
                        if (cId == SectionCtrlCid) 
                        {
                            ReportAllParameters(cId, pId);
                        }
                    }
                    else 
                    {
                        CfgReportParameter(cId, pId, paramId);
                    }
                }
                break;
            case CFG_MOD_PARAM:
                if (DataCheckWord(paramId) == DATA_VALID) 
                {
                    paramType = (msgPtr->bytesPtr[1]&0x38) >> 3;
                    if (DataCheck3Bit(paramType) == DATA_VALID) 
                    {
                        CfgModifyParameter(cId, pId, paramId, paramType, msgPtr->bytesPtr+4);
                    }
                }
                break;  
            case CFG_CLEAR_EEPROM:
                if (cId == SectionCtrlCid) 
                {
                    //CfgClearEeprom();
                    ReportAllParameters(cId, pId);                   
                }
                break;
        }
    }
}
/*****************************************************************************
*   function :      IbsSendCurrentSectionStatus
*   comments:       Sends an isobus frame to the partner with the 
*                   current estimatedCurvature and the status.
****************************************************************************/
void IbsSendCurrentSectionStatus(SectionCollectionId_t collectionId, SectionCollectionData_t *sectionDataPtr)
{
    CanFrame_t frm;
    frm.id = 0x18000000 | (PGN_PRO_B_SECTION_CTRL << 8) ;
    frm.dlc = 8;
    switch (collectionId)
    {
        case SECTION_ID_1_56:
            frm.bytes[0] = CBTX_SECTION_STATUS_0_56;
            break;
        case SECTION_ID_57_112:
            frm.bytes[0] = CBTX_SECTION_STATUS_57_112;
            break;
    }
    frm.bytes[1] = sectionDataPtr->sectionBytes[0];
    frm.bytes[2] = sectionDataPtr->sectionBytes[1];
    frm.bytes[3] = sectionDataPtr->sectionBytes[2];
    frm.bytes[4] = sectionDataPtr->sectionBytes[3];
    frm.bytes[5] = sectionDataPtr->sectionBytes[4];
    frm.bytes[6] = sectionDataPtr->sectionBytes[5];
    frm.bytes[7] = sectionDataPtr->sectionBytes[6];
    IbsNmSendFrame(SectionCtrlCid, &frm);
}



/*************************************************************************
* function: HandleScuCommand   
* comments: handler function for SCU specific prop A commands  
**************************************************************************/
static void handleScuCommand(CanMsg_t *canMsgPtr)
{

    IbsRxMessage_t *rxMsgPtr;
    rxMsgPtr = (IbsRxMessage_t*)canMsgPtr->bytesPtr;

    switch (rxMsgPtr->messageId)
    {
        case CBRX_SECTION_COMMAND_0_56:
        case CBRX_SECTION_COMMAND_57_112:
            // Check if Section ON/OFF is online (SBGuindce)
            //if (SectionOnOffEnabled == TRUE && IbsNmGetPartnerOnline(0, PartnerIds[SPRAYER_PARTNER_SECTION_ON_OFF]) == TRUE)
            //{
            //    // Check if it is send by Section ctrl ON/OFF
            //    if (IbsNmIsMyPartner(SectionCtrlCid, PartnerIds[SPRAYER_PARTNER_SECTION_ON_OFF], canMsgPtr->sourceAddress))// TODO make index device depended.
            //    {
            //        if(OS_PutMailCond(&MailboxSectionCtrlSetup, (void*)rxMsgPtr) == 0)
            //        {
            //            OS_SignalEvent(EVENT_SECTION_CTRL_MSG, &TcbSectionCtrlTask);
            //        }
            //        else
            //        {
            //            //PROBLEEM
            //        }
            //    }
            //}
            //// SBGuidance not online
            //else
            //{
            //    static Bool_t firstRun = TRUE;;
            //    // Check if it is send by machine ctrl function instance 1
            //    if (IbsNmIsMyPartner(SectionCtrlCid, PartnerIds[SPRAYER_PARTNER_SPAYER_MACHINE_CTRL], canMsgPtr->sourceAddress))// TODO make index device depended changes in shared.
            //    {
            //        if (firstRun)
            //        {
            //            OS_SignalEvent(SECTIONCTRL_START, &TcbSectionCtrlTask);
            //            firstRun = FALSE;
            //        }
            //        if(OS_PutMailCond(&MailboxSectionCtrlSetup, (void*)rxMsgPtr) == 0)
            //        {
            //            OS_SignalEvent(EVENT_SECTION_CTRL_MSG, &TcbSectionCtrlTask);
            //        }
            //        else
            //        {
            //            //PROBLEEM
            //        }
            //    }
            //}
            break;
        case CBRX_REQUEST_BOOM_CONFIGURATION: 
            if (Cfg.cfgFlash.sectionCtrlCfg.boomConfigurationI.configurationExchangeKey == rxMsgPtr->messageData.requestBoomConfigMsg.configurationExchangeKey)
            {
                IbsDlSendBoomConfiguration(FALSE);
            }
            else
            {
                IbsDlSendBoomConfiguration(TRUE);
            }
            break;
        case CBRX_REQUEST_SECTION_CONFIGURATION:  
            //CfgReportSectionConfiguration(
            //    SectionCtrlCid, 
            //    PartnerIds[SPRAYER_PARTNER_SECTION_ON_OFF],
            //    rxMsgPtr->messageData.requestSectionConfigMsg.sectionIdentifier);
            break;
        case CBRX_DESIRED_APPLICATION_RATE:            
            break;
        case CBRX_STATUS_INFORMATION:
            break;
        case CBRX_BOOM_CONFIGURATION_I:   
            //OS_SignalEvent(SECTIONCTRL_STOP,&TcbSectionCtrlTask);
            CfgHandleBoomConfiguration(BOOM_CONFIG_1,(BoomConfigMessageData_t *)(&(rxMsgPtr->messageData)));
            break;
        case CBRX_BOOM_CONFIGURATION_II:               
            CfgHandleBoomConfiguration(BOOM_CONFIG_2,(BoomConfigMessageData_t *)(&(rxMsgPtr->messageData)));
            break;
        case CBRX_BOOM_CONFIGURATION_III:    
            CfgHandleBoomConfiguration(BOOM_CONFIG_3,(BoomConfigMessageData_t *)(&(rxMsgPtr->messageData)));
            break;
        case CBRX_SECTION_CONFIGURATION:         
            CfgHandleBoomConfiguration(SECTION_CONFIG,(BoomConfigMessageData_t *)(&(rxMsgPtr->messageData)));
            break; 
    }

}


/*************************************************************************
* function: HandleScuCommand   
* comments: handler function for SCU specific prop A commands  
**************************************************************************/
static void handleScuStatus(CanMsg_t *canMsgPtr)
{

    IbsRxMessage_t *rxMsgPtr;
    rxMsgPtr = (IbsRxMessage_t*)canMsgPtr->bytesPtr;

    switch (rxMsgPtr->messageId)
    {
        case CBTX_SECTION_STATUS_0_56:
        case CBTX_SECTION_STATUS_57_112: 
            if(OS_PutMailCond(&MailboxSectionCtrlSetup, (void*)rxMsgPtr) == 0)
            {
                OS_SignalEvent(SWITCH_BOX_HANDLE_STATUS_MSG_ACTBOX, &TcbSectionCtrlTask);
            }
            else
            {
                //PROBLEEM
            }
            break;
        default:
            break;
    }
}


/*************************************************************************
* function: ibsDlSteerFillPartnerData    
* comments: fill the partner data array;
**************************************************************************/
static void ibsDlSectionCtrlFillPartnerData(IbsPartnerData_t * partnerDataPtr)
{
    //fill the partnerdataPtr
    partnerDataPtr[SPRAYER_PARTNER_SPAYER_MACHINE_CTRL].name.identityNo         = 0;     
    partnerDataPtr[SPRAYER_PARTNER_SPAYER_MACHINE_CTRL].name.manfCode           = NAVTRONICS;
    partnerDataPtr[SPRAYER_PARTNER_SPAYER_MACHINE_CTRL].name.ecuInst            = 0;
    partnerDataPtr[SPRAYER_PARTNER_SPAYER_MACHINE_CTRL].name.functionInst       = 0;         
    partnerDataPtr[SPRAYER_PARTNER_SPAYER_MACHINE_CTRL].name.function           = SPRAYERS_MACHINE_CONTROL;

    partnerDataPtr[SPRAYER_PARTNER_SPAYER_MACHINE_CTRL].name.reserved           = 0;
    partnerDataPtr[SPRAYER_PARTNER_SPAYER_MACHINE_CTRL].name.deviceClass        = SPRAYER;
    partnerDataPtr[SPRAYER_PARTNER_SPAYER_MACHINE_CTRL].name.deviceClassInst    = 0;
    partnerDataPtr[SPRAYER_PARTNER_SPAYER_MACHINE_CTRL].name.industryGroup      = AGRICULTURE;  
    partnerDataPtr[SPRAYER_PARTNER_SPAYER_MACHINE_CTRL].name.selfConfigSA       = TRUE;

    partnerDataPtr[SPRAYER_PARTNER_SPAYER_MACHINE_CTRL].mask.identityNo         = MASK_IDENTITY_NO_DISABLED;
    partnerDataPtr[SPRAYER_PARTNER_SPAYER_MACHINE_CTRL].mask.manfCode           = MASK_MANF_CODE_DISABLED;
    partnerDataPtr[SPRAYER_PARTNER_SPAYER_MACHINE_CTRL].mask.ecuInst            = MASK_ECU_INST_DISABLED;
    partnerDataPtr[SPRAYER_PARTNER_SPAYER_MACHINE_CTRL].mask.functionInst       = MASK_FUNCTION_INST_ENABLED;
    partnerDataPtr[SPRAYER_PARTNER_SPAYER_MACHINE_CTRL].mask.function           = MASK_FUNCTION_ENABLED;
    partnerDataPtr[SPRAYER_PARTNER_SPAYER_MACHINE_CTRL].mask.reserved           = MASK_RESERVED_DISABLED;
    partnerDataPtr[SPRAYER_PARTNER_SPAYER_MACHINE_CTRL].mask.deviceClass        = MASK_DEVICE_CLASS_ENABLED;
    partnerDataPtr[SPRAYER_PARTNER_SPAYER_MACHINE_CTRL].mask.deviceClassInst    = MASK_DEVICE_CLASS_INST_DISABLED;
    partnerDataPtr[SPRAYER_PARTNER_SPAYER_MACHINE_CTRL].mask.industryGroup      = MASK_INDUSTRY_GROUP_ENABLED;
    partnerDataPtr[SPRAYER_PARTNER_SPAYER_MACHINE_CTRL].mask.selfConfigSA       = MASK_SELF_CONFIG_SA_ENABLED;

    partnerDataPtr[SPRAYER_PARTNER_SPAYER_MACHINE_CTRL].msgHandlerPtr                = IbsDlPartnerHandler;     
    partnerDataPtr[SPRAYER_PARTNER_SPAYER_MACHINE_CTRL].partnerTimeOut               = PARTNER_TIMEOUT_DEFAULT;

    partnerDataPtr[SPRAYER_PARTNER_DIAG_SERV].name.identityNo              = 0;     
    partnerDataPtr[SPRAYER_PARTNER_DIAG_SERV].name.manfCode                = NAVTRONICS;
    partnerDataPtr[SPRAYER_PARTNER_DIAG_SERV].name.ecuInst                 = 0; 
    partnerDataPtr[SPRAYER_PARTNER_DIAG_SERV].name.functionInst            = 0;
    partnerDataPtr[SPRAYER_PARTNER_DIAG_SERV].name.function                = ON_BOARD_DIAGNOSTIC_SERVICE_TOOL;                      
    partnerDataPtr[SPRAYER_PARTNER_DIAG_SERV].name.reserved                = 0;
    partnerDataPtr[SPRAYER_PARTNER_DIAG_SERV].name.deviceClass             = NON_SPECIFIC;
    partnerDataPtr[SPRAYER_PARTNER_DIAG_SERV].name.deviceClassInst         = 0;
    partnerDataPtr[SPRAYER_PARTNER_DIAG_SERV].name.industryGroup           = GLOBAL;  
    partnerDataPtr[SPRAYER_PARTNER_DIAG_SERV].name.selfConfigSA            = TRUE;

    partnerDataPtr[SPRAYER_PARTNER_DIAG_SERV].mask.identityNo              = MASK_IDENTITY_NO_DISABLED;
    partnerDataPtr[SPRAYER_PARTNER_DIAG_SERV].mask.manfCode                = MASK_MANF_CODE_DISABLED;
    partnerDataPtr[SPRAYER_PARTNER_DIAG_SERV].mask.ecuInst                 = MASK_ECU_INST_DISABLED;
    partnerDataPtr[SPRAYER_PARTNER_DIAG_SERV].mask.functionInst            = MASK_FUNCTION_INST_ENABLED;
    partnerDataPtr[SPRAYER_PARTNER_DIAG_SERV].mask.function                = MASK_FUNCTION_ENABLED;
    partnerDataPtr[SPRAYER_PARTNER_DIAG_SERV].mask.reserved                = MASK_RESERVED_DISABLED;
    partnerDataPtr[SPRAYER_PARTNER_DIAG_SERV].mask.deviceClass             = MASK_DEVICE_CLASS_ENABLED;
    partnerDataPtr[SPRAYER_PARTNER_DIAG_SERV].mask.deviceClassInst         = MASK_DEVICE_CLASS_INST_DISABLED;
    partnerDataPtr[SPRAYER_PARTNER_DIAG_SERV].mask.industryGroup           = MASK_INDUSTRY_GROUP_ENABLED;
    partnerDataPtr[SPRAYER_PARTNER_DIAG_SERV].mask.selfConfigSA            = MASK_SELF_CONFIG_SA_ENABLED;
    partnerDataPtr[SPRAYER_PARTNER_DIAG_SERV].msgHandlerPtr                = BootCmdHandler; 

    partnerDataPtr[SPRAYER_PARTNER_DIAG_SERV].partnerTimeOut               = PARTNER_TIMEOUT_INFINITE;  //disabled

    //stop of partner data fill

}

/****************************************************************************/
/**                                                                        **/
/**                              EOF                                       **/
/**                                                                        **/
/****************************************************************************/



