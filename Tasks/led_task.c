/*
                             *******************
******************************* C SOURCE FILE *******************************
**                           *******************                           **
**                                                                         **
** project   : Steering controller LPC1768                                 **
** filename  : ledtask.c                                                   **
**                                                                         **
*****************************************************************************
**                                                                         **
** Copyright (c) 2012, SBG Precision Farming/Navtronics Bvba               **
** All rights reserved.                                                    **
**                                                                         **
*****************************************************************************

VERSION HISTORY:
----------------

Version     : 2
Date        : 20121016
Revised by  : Koen Smeuninx
Description : * c style changes

Version     : 1
Date        : 20120919
Revised by  : Koen Smeuninx 
Description : * Original version.

FILE INFORMATION:
-----------------
Control of the led behaviour

*/

/****************************************************************************/
/**                                                                        **/
/**                     MODULES USED                                       **/
/**                                                                        **/
/****************************************************************************/
#include "lpc_types.h"
#include "led_task.h"
#include "Tasks.h"
#include "rtos.h"
#include "led_service.h"

/****************************************************************************/
/**                                                                        **/
/**                     DEFINITIONS AND MACROS                             **/
/**                                                                        **/
/****************************************************************************/


/****************************************************************************/
/**                                                                        **/
/**                     TYPEDEFS AND STRUCTURES                            **/
/**                                                                        **/
/****************************************************************************/


/****************************************************************************/
/**                                                                        **/
/**                     PROTOTYPES OF LOCAL FUNCTIONS                      **/
/**                                                                        **/
/****************************************************************************/
static void ledTask (void);
/****************************************************************************/
/**                                                                        **/
/**                     EXPORTED VARIABLES                                 **/
/**                                                                        **/
/****************************************************************************/
OS_TASK TcbLedTask;
/****************************************************************************/
/**                                                                        **/
/**                     GLOBAL VARIABLES                                   **/
/**                                                                        **/
/****************************************************************************/
static OS_STACKPTR int StackLedTask[64];

/****************************************************************************/
/**                                                                        **/
/**                     EXPORTED FUNCTIONS                                 **/
/**                                                                        **/
/****************************************************************************/

/*****************************************************************************
*   function :      InitLedTask
*   parameters:     void
*   returns:        void
*   comments:       Initialization of the led task.
****************************************************************************/
void InitLedTask(void)
{
    // Create Led task, priority 0
    OS_CREATETASK(&TcbLedTask, "Led Task", ledTask,PRIORITY_LED_TASK , StackLedTask);
    extern void InitLedService(void);  //only the ledtask should have access
    InitLedService();
}


/****************************************************************************/
/**                                                                        **/
/**                     LOCAL FUNCTIONS                                    **/
/**                                                                        **/
/****************************************************************************/


/*****************************************************************************
*   function :      ledTask
*   parameters:     void
*   returns:        void
*   comments:       task to control the led.
****************************************************************************/
static void ledTask(void)
{
    LedCommand_t *ledCommandPtr;
    UI08_t msgSize;
    while(TRUE)
    {
        msgSize = OS_Q_GetPtr(&LedServiceQueue,(void**) &ledCommandPtr);
        if (msgSize != sizeof(LedCommand_t) )
        {
            continue;
        }
        HandleLedCommand(ledCommandPtr); 
        OS_Q_Purge(&LedServiceQueue);
    };
}


/****************************************************************************/
/**                                                                        **/
/**                               EOF                                      **/
/**                                                                        **/
/****************************************************************************/
