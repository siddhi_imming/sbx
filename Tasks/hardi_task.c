/*
                             *******************
******************************* C SOURCE FILE *******************************
**                           *******************                           **
**                                                                         **
** project   : Section control                                             **
** filename  : hardi_task.c                                                **
**                                                                         **
*****************************************************************************
**                                                                         **
** Copyright (c) 2012, SBG Precision Farming/Navtronics Bvba               **
** All rights reserved.                                                    **
**                                                                         **
*****************************************************************************

VERSION HISTORY:
----------------

Version     : 1
Date        : 20130419
Revised by  : Siddhi Imming
Description : * Original version.

FILE INFORMATION:
-----------------
Task to control the hardi protocol. 
*/


/****************************************************************************/
/**                                                                        **/
/**                     MODULES USED                                       **/
/**                                                                        **/
/****************************************************************************/

#include <rtos.h>

#include "Tasks.h"
#include "hardi_task.h"
#include "hardi_service.h"
#include "abx_bridge_hardi_driver.h"

/****************************************************************************/
/**                                                                        **/
/**                     DEFINITIONS AND MACROS                             **/
/**                                                                        **/
/****************************************************************************/

#define UPDATE_TIMEOUT           400
#define MAX_NR_OF_MESSAGES         1

/****************************************************************************/
/**                                                                        **/
/**                     TYPEDEFS AND STRUCTURES                            **/
/**                                                                        **/
/****************************************************************************/


/****************************************************************************/
/**                                                                        **/
/**                     PROTOTYPES OF LOCAL FUNCTIONS                      **/
/**                                                                        **/
/****************************************************************************/

static void hardiTask (void);
static void cmdUpdateTimeout(void);

/****************************************************************************/
/**                                                                        **/
/**                     EXPORTED VARIABLES                                 **/
/**                                                                        **/
/****************************************************************************/

OS_TASK TcbHardiTask;

/****************************************************************************/
/**                                                                        **/
/**                     GLOBAL VARIABLES                                   **/
/**                                                                        **/
/****************************************************************************/

static OS_STACKPTR int StackHardiTask[128];
static OS_TIMER StatusUpdateTimer;

OS_MAILBOX      MailboxHardiRs232;
static char     MailboxHardiRs232Buffer[HARDI_BUFFER_SIZE];

/****************************************************************************/
/**                                                                        **/
/**                     EXPORTED FUNCTIONS                                 **/
/**                                                                        **/
/****************************************************************************/

/*****************************************************************************
*   function :      InitUartTask
*   parameters:     void
*   returns:        void
*   comments:       Initialization of the hardi task.
****************************************************************************/
void InitHardiTask(void)
{
    //Create a mailbox which can contain one message
    OS_CREATEMB(&MailboxHardiRs232, HARDI_BUFFER_SIZE, MAX_NR_OF_MESSAGES, &MailboxHardiRs232Buffer);

    //Create hardi task, priority 4
    OS_CREATETASK(&TcbHardiTask, "Hardi Task", hardiTask, PRIORITY_HARDI_TASK , StackHardiTask);
    OS_CREATETIMER(&StatusUpdateTimer,cmdUpdateTimeout, UPDATE_TIMEOUT);
}


/****************************************************************************/
/**                                                                        **/
/**                     LOCAL FUNCTIONS                                    **/
/**                                                                        **/
/****************************************************************************/


/*****************************************************************************
*   function :      statusUpdate
*   parameters:     void
*   returns:        void
*   comments:       Sends the status of the sections
****************************************************************************/
static void cmdUpdateTimeout(void)
{
    OS_RetriggerTimer(&StatusUpdateTimer);
    OS_SignalEvent(HARDI_EVENT_TIME_PASTED,&TcbHardiTask);
}

/*****************************************************************************
*   function :      hardiTask
*   parameters:     void
*   returns:        void
*   comments:       task to control the hardi protocol.
****************************************************************************/
static void hardiTask(void)
{
    UI08_t  receivedEvents;
    char    buffer[HARDI_BUFFER_SIZE];
    Bool_t  newCmdAvailable = FALSE;
    Bool_t  intervalBetweenCmdsOke = FALSE;
    Bool_t  startTask = FALSE;
    Bool_t  requestedSectionStatus = FALSE;

    while(TRUE)
    {
        receivedEvents = OS_WaitEvent(  
            HARDI_EVENT_DATA_RECEIVED |
            HARDI_EVENT_SEND_COMMAND  |
            HARDI_EVENT_TIME_PASTED   |
            HARDI_EVENT_START);

        if (receivedEvents & HARDI_EVENT_START)
        {
            startTask = TRUE;
        }

        if (startTask)
        {
            if(receivedEvents & HARDI_EVENT_DATA_RECEIVED)
            {
                // received feedback of the status of the sections.
                if(OS_GetMailCond(&MailboxHardiRs232, &buffer) == 0)
                {
                    // parse the data the section data 
                    if (HardiHandleData(&buffer[0]) == TRUE)
                    {
                        // Send the section status to the Section Control ON/OFF
                        SendSectionInput2SectionCtrl();
                    }
                }

            }
            if(receivedEvents & HARDI_EVENT_SEND_COMMAND)
            {
                // Send section command to Hardi sprayer
                newCmdAvailable = TRUE;

            }
            if(receivedEvents & HARDI_EVENT_TIME_PASTED)
            {
                intervalBetweenCmdsOke = TRUE;
            }
            if ((newCmdAvailable == TRUE) && (intervalBetweenCmdsOke == TRUE) && (requestedSectionStatus == TRUE))
            {
                newCmdAvailable = FALSE;
                requestedSectionStatus = FALSE;
                intervalBetweenCmdsOke = FALSE;
                SendSectionInput2Hardi();
            }
            else if (intervalBetweenCmdsOke == TRUE)
            {
                requestedSectionStatus = TRUE;
                intervalBetweenCmdsOke = FALSE;
                SendSectionStatusRequest2Hardi();
            }
        }
    }
}


/****************************************************************************/
/**                                                                        **/
/**                               EOF                                      **/
/**                                                                        **/
/****************************************************************************/
