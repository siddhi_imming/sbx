/*
                             *******************
******************************* C SOURCE FILE *******************************
**                           *******************                           **
**                                                                         **
** project   : Shared files                                                **
** filename  : ibs_task.c                                                  **
**                                                                         **
*****************************************************************************
**                                                                         **
** Copyright (c) 2012, SBG Precision Farming/Navtronics Bvba               **
** All rights reserved.                                                    **
**                                                                         **
*****************************************************************************

VERSION HISTORY:
----------------

Version     : 2
Date        : 20121016
Revised by  : Koen Smeuninx 
Description : * c style applied


Version     : 1
Date        : 20120705
Revised by  : Koen Smeuninx 
Description : * Original version.

FILE INFORMATION:
-----------------
performs all the isobus tasks. 
*/

/****************************************************************************/
/**                                                                        **/
/**                     MODULES USED                                       **/
/**                                                                        **/
/****************************************************************************/

#include "tasks.h"
#include "ibs_task.h"
#include "ibs_dl_section_ctrl.h"
#include "ibs_dl_aux_unit.h"
#include "config.h"
#include "ibs_dl.h"

/****************************************************************************/
/**                                                                        **/
/**                     DEFINITIONS AND MACROS                             **/
/**                                                                        **/
/****************************************************************************/
#define ISOBUS_EVENT_TIMER_ELAPSED          0x01
#define ISOBUS_EVENT_MESSAGE_RECEIVED       0x02

/****************************************************************************/
/**                                                                        **/
/**                     TYPEDEFS AND STRUCTURES                            **/
/**                                                                        **/
/****************************************************************************/

/****************************************************************************/
/**                                                                        **/
/**                     PROTOTYPES OF LOCAL FUNCTIONS                      **/
/**                                                                        **/
/****************************************************************************/

static void isoBusTask (void);
static void timerCallBack(void);

/****************************************************************************/
/**                                                                        **/
/**                     EXPORTED VARIABLES                                 **/
/**                                                                        **/
/****************************************************************************/

/****************************************************************************/
/**                                                                        **/
/**                     GLOBAL VARIABLES                                   **/
/**                                                                        **/
/****************************************************************************/

// Second task
static OS_TASK TcbIsoBusTask;
static OS_STACKPTR int StackIsoBusTask[256];
static OS_TIMER Timer;

/****************************************************************************/
/**                                                                        **/
/**                     EXPORTED FUNCTIONS                                 **/
/**                                                                        **/
/****************************************************************************/

/*****************************************************************************
*   function :      InitIsoBusTask
*   parameters:     void
*   returns:        void
*   comments:       Initialization of the IsoBus task.
****************************************************************************/
void InitIsoBusTask(void)
{
    NameStruct_t canUniqueId;
    UI08_t tempCid;
    DataLayerConfiguration_t configuration;

    IbsDlInit();
    IbsNmInit();

    // Initialize isobus
    //partner descriptor of this device
    canUniqueId.identityNo      = Cfg.cfgFlash.staticCfgFlash.serialNr; // serial nr.
    canUniqueId.selfConfigSA    = TRUE;						// 1 (Bool_t)Cfg.staticCfgFlash.selfConfigurableAddress;
    canUniqueId.deviceClass     = SPRAYER;					// this still needs to be changed to none NON_SPECIFIC
    canUniqueId.deviceClassInst = 0;						// first device class instance
    canUniqueId.function        = SPRAYERS_MACHINE_CONTROL;	// 132 Cfg.staticCfgFlash.function;
    canUniqueId.functionInst    = 1;						// first function instance
    canUniqueId.ecuInst         = 0;						// first ECU instance
	canUniqueId.industryGroup   = AGRICULTURE;
    canUniqueId.manfCode        = Cfg.cfgFlash.staticCfgFlash.manufacturer;
    configuration.ChildInitFunction = IbsDlSectionCtrltInit;
    configuration.InitNamePtr = &canUniqueId;
    configuration.SourceAddress = 0xB1; //Cfg.cfgFlash.staticCfgFlash.sourceAddressClaimStart;
    configuration.nrOfPartners = SPRAYER_PARTNER_MAX_PARTNERS;
    configuration.canNumber = 0;

    tempCid = IbsDlAlloc(&configuration); 
    if (tempCid == ALLOC_ERROR)
    {
        //return;    //
    }
    IbsDlStartProtocol(tempCid);

    //Create ans start IsoBus task
    OS_CREATETASK(&TcbIsoBusTask, "IsoBus Task", isoBusTask,PRIORITY_ISOBUS_TASK, StackIsoBusTask);
    OS_CREATETIMER(&Timer,timerCallBack,10);
}


/****************************************************************************/
/**                                                                        **/
/**                     LOCAL FUNCTIONS                                    **/
/**                                                                        **/
/****************************************************************************/

/*****************************************************************************
*   function :      timerCallBack
*   parameters:     void
*   returns:        void
*   comments:       this callback is called when the software timer expires
*                   It sends an event to the isobus task 
****************************************************************************/
static void timerCallBack(void)
{
    OS_SignalEvent(ISOBUS_EVENT_TIMER_ELAPSED,&TcbIsoBusTask);
}

/*****************************************************************************
*   function :      IsoBusTask
*   parameters:     void
*   returns:        void
*   comments:       task to control the isobus.
****************************************************************************/
static void isoBusTask(void)
{
    char receivedEvents;
    int index;
    while(TRUE)
    {
        receivedEvents = OS_WaitEvent(ISOBUS_EVENT_TIMER_ELAPSED | ISOBUS_EVENT_MESSAGE_RECEIVED);
        if (receivedEvents & ISOBUS_EVENT_TIMER_ELAPSED)
        {
            for (index = 0; index < NR_OF_IBS_DEVICES ; index++ )
            {
                IbsDlMonitorNetwork(index);
            }
        }

        if (receivedEvents & ISOBUS_EVENT_MESSAGE_RECEIVED)
        {
         
        }
        OS_RetriggerTimer(&Timer);
    };
}


/****************************************************************************/
/**                                                                        **/
/**                               EOF                                      **/
/**                                                                        **/
/****************************************************************************/
