/*
                             *******************
******************************* C HEADER FILE *******************************
**                           *******************                           **
**                                                                         **
** project   : Switch Box LPC17xx                                          **
** filename  : switch_box_task.h                                           **
**                                                                         **
*****************************************************************************
**                                                                         **
** Copyright (c) 2012, SBG Precision Farming/Navtronics Bvba               **
** All rights reserved.                                                    **
**                                                                         **
*****************************************************************************

VERSION HISTORY:
----------------

Version     : 1
Date        : 20130610
Revised by  : Siddhi Imming
Description : * Original version.


FILE INFORMATION:
-----------------

Task controlling all the switch box actions needed.  

*/

#ifndef _SWITCH_BOX_TASK_INCLUDED
#define _SWITCH_BOX_TASK_INCLUDED

/****************************************************************************/
/**                                                                        **/
/**                     MODULES USED                                       **/
/**                                                                        **/
/****************************************************************************/

#include "rtos.h"
#include "stddefs.h"

/****************************************************************************/
/**                                                                        **/
/**                     DEFINITIONS AND MACROS                             **/
/**                                                                        **/
/****************************************************************************/

#define SWITCH_BOX_READ_INPUTS_TIME_ELAPSED          0x01
#define SWITCH_BOX_HANDLE_STATUS_MSG_ACTBOX          0x02
#define SWITCH_BOX_SEND_SPRAYER_INFO_MSG             0x04
#define SWITCH_BOX_SEND_SWITCH_INFO_MSG              0x08
#define SWITCH_BOX_EVENT_ERROR                       0x80

#define MAX_NR_OF_MESSAGES                  1

/****************************************************************************/
/**                                                                        **/
/**                     TYPEDEFS AND STRUCTURES                            **/
/**                                                                        **/
/****************************************************************************/

/****************************************************************************/
/**                                                                        **/
/**                     EXPORTED VARIABLES                                 **/
/**                                                                        **/
/****************************************************************************/

extern OS_TASK     TcbSectionCtrlTask;
extern OS_MAILBOX  MailboxSectionCtrlSetup;
extern OS_TIMER    MainSwitchSendMsgTimer;
extern OS_TIMER    SectionSwitchesSendMsgTimer;

/****************************************************************************/
/**                                                                        **/
/**                     EXPORTED FUNCTIONS                                 **/
/**                                                                        **/
/****************************************************************************/

/*************************************************************************
* function:     InitsectionCTRLTask 
* parameters:   void
* returns:      void
* comments:     initializes and starts the sectionCTRL task
**************************************************************************/
void InitSectionCtrlTask(void);


#endif

/****************************************************************************/
/**                                                                        **/
/**                               EOF                                      **/
/**                                                                        **/
/****************************************************************************/
