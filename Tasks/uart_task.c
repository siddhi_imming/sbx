/*
                             *******************
******************************* C SOURCE FILE *******************************
**                           *******************                           **
**                                                                         **
** project   : Steering controller based on the NXP LPC17xx                **
** filename  : uarttask.c                                                  **
**                                                                         **
*****************************************************************************
**                                                                         **
** Copyright (c) 2012, SBG Precision Farming/Navtronics Bvba               **
** All rights reserved.                                                    **
**                                                                         **
*****************************************************************************

VERSION HISTORY:
----------------
Version     : 2
Date        : 20121016
Revised by  : Koen Smeuninx
Description : * c style changes

Version     : 1
Date        : 20120919
Revised by  : Koen Smeuninx 
Description : * Original version.

FILE INFORMATION:
-----------------
Task to control the uart behaviour. 
*/


/****************************************************************************/
/**                                                                        **/
/**                     MODULES USED                                       **/
/**                                                                        **/
/****************************************************************************/
#include "lpc_types.h"
#include "uart_task.h"
#include "Tasks.h"
#include "rtos.h"
#include "uart_service.h"

/****************************************************************************/
/**                                                                        **/
/**                     DEFINITIONS AND MACROS                             **/
/**                                                                        **/
/****************************************************************************/


/****************************************************************************/
/**                                                                        **/
/**                     TYPEDEFS AND STRUCTURES                            **/
/**                                                                        **/
/****************************************************************************/


/****************************************************************************/
/**                                                                        **/
/**                     PROTOTYPES OF LOCAL FUNCTIONS                      **/
/**                                                                        **/
/****************************************************************************/
static void uartTask (void);
/****************************************************************************/
/**                                                                        **/
/**                     EXPORTED VARIABLES                                 **/
/**                                                                        **/
/****************************************************************************/
OS_TASK TcbUartTask;
/****************************************************************************/
/**                                                                        **/
/**                     GLOBAL VARIABLES                                   **/
/**                                                                        **/
/****************************************************************************/
static OS_STACKPTR int StackUartTask[128];

/****************************************************************************/
/**                                                                        **/
/**                     EXPORTED FUNCTIONS                                 **/
/**                                                                        **/
/****************************************************************************/

/*****************************************************************************
*   function :      InitUartTask
*   parameters:     void
*   returns:        void
*   comments:       Initialization of the uart task.
****************************************************************************/
void InitUartTask(void)
{
    //Create Uart task, priority 0
    OS_CREATETASK(&TcbUartTask, "Uart Task", uartTask,PRIORITY_UART_TASK , StackUartTask);
    extern void InitUartService(void);  //only the uart task should have access
    InitUartService();
}


/****************************************************************************/
/**                                                                        **/
/**                     LOCAL FUNCTIONS                                    **/
/**                                                                        **/
/****************************************************************************/


/*****************************************************************************
*   function :      uartTask
*   parameters:     void
*   returns:        void
*   comments:       task to control the uart.
****************************************************************************/
static void uartTask(void)
{
    UartCommand_t *uartCmdPtr;
    while(TRUE)
    {
        OS_Q_GetPtr(&UartServiceQueue,(void**)&uartCmdPtr);
        extern void UartSendBytes(UartCommand_t *cmdPtr);  //only the task gets access
        UartSendBytes(uartCmdPtr); 
        OS_Q_Purge(&UartServiceQueue);
    };
}


/****************************************************************************/
/**                                                                        **/
/**                               EOF                                      **/
/**                                                                        **/
/****************************************************************************/
