/*
                             *******************
******************************* C SOURCE FILE *******************************
**                           *******************                           **
**                                                                         **
** project   : General modules                                             **
** filename  : Sectionctrltask.c
**                                                                         **
*****************************************************************************
**                                                                         **
** Copyright (c) 2012, SBG Precision Farming/Navtronics Bvba               **
** All rights reserved.                                                    **
**                                                                         **
*****************************************************************************

VERSION HISTORY:
----------------

Version     : 1
Date        : 20121025
Revised by  : Thijs Esselink
Description : * Original version.

FILE INFORMATION:
-----------------

Task for handling the section controller, is serviced by the section control service
*/

/****************************************************************************/
/**                                                                        **/
/**                     MODULES USED                                       **/
/**                                                                        **/
/****************************************************************************/

#include "Tasks.h"
#include "section_ctrl_task.h"
#include "switch_box_service.h"
#include "section_ctrl_defs.h"
#include "io_expander_spi.h"
#include "ibs_defs_section_ctrl.h"
#include "can.h"
#include "ibs_nm.h"
#include "ibs_dl.h"

/****************************************************************************/
/**                                                                        **/
/**                     DEFINITIONS AND MACROS                             **/
/**                                                                        **/
/****************************************************************************/

#define WATCHDOG_TIMEOUT                5000
#define IO_EXPANDER_CHECK_ONLINE_TIME   5000
#define STATUS_UPDATE_TIMEOUT           1000

#define MAX_NR_OF_MESSAGES                 1
/****************************************************************************/
/**                                                                        **/
/**                     TYPEDEFS AND STRUCTURES                            **/
/**                                                                        **/
/****************************************************************************/

/****************************************************************************/
/**                                                                        **/
/**                     PROTOTYPES OF LOCAL FUNCTIONS                      **/
/**                                                                        **/
/****************************************************************************/

static void sectionCtrlTask (void);
static void watchDogAlert(void);
static void polSwitchesTimeout(void);
static void mainSwitchSendMsgTimeout(void);
static void sectionSwitchesSendMsgTimeout(void);

/****************************************************************************/
/**                                                                        **/
/**                     EXPORTED VARIABLES                                 **/
/**                                                                        **/
/****************************************************************************/

OS_TASK     TcbSectionCtrlTask;
OS_MAILBOX  MailboxSectionCtrlSetup;
OS_TIMER    MainSwitchSendMsgTimer;
OS_TIMER    SectionSwitchesSendMsgTimer;

/****************************************************************************/
/**                                                                        **/
/**                     GLOBAL VARIABLES                                   **/
/**                                                                        **/
/****************************************************************************/
// Second task
static OS_STACKPTR int StackSectionCtrlTask[256];
static OS_TIMER WatchdogTimer;
static OS_TIMER PolSwitchesTimer;
static char     MailboxSectionCtrlSetupBuffer[sizeof(IbsRxMessage_t)];
   
/****************************************************************************/
/**                                                                        **/
/**                     EXPORTED FUNCTIONS                                 **/
/**                                                                        **/
/****************************************************************************/

/*****************************************************************************
*   function :      InitsectionCtrlTask
*   parameters:     void
*   returns:        void
*   comments:       Initialization of the sectionCTRL task.
****************************************************************************/
void InitSectionCtrlTask(void)
{
    ServiceSwitchBoxInit();
    
    //Create a mailbox which can contain one message
    OS_CREATEMB(&MailboxSectionCtrlSetup, sizeof(IbsRxMessage_t), MAX_NR_OF_MESSAGES, &MailboxSectionCtrlSetupBuffer);
    OS_CREATETASK(&TcbSectionCtrlTask, "Section Ctrl Task", sectionCtrlTask, PRIORITY_SECTIONCTRL_TASK , StackSectionCtrlTask);
    OS_CREATETIMER(&WatchdogTimer,watchDogAlert,WATCHDOG_TIMEOUT);
    OS_CREATETIMER(&PolSwitchesTimer,polSwitchesTimeout, SWB_TIMER_TIMEOUT_MS); 
    OS_CREATETIMER(&MainSwitchSendMsgTimer,mainSwitchSendMsgTimeout, SWB_SEND_CAN_MSG_MS); 
    OS_CREATETIMER(&SectionSwitchesSendMsgTimer,sectionSwitchesSendMsgTimeout, SWB_SEND_CAN_MSG_MS);
}


/****************************************************************************/
/**                                                                        **/
/**                     LOCAL FUNCTIONS                                    **/
/**                                                                        **/
/****************************************************************************/

/*****************************************************************************
*   function :      watchDogAlert
*   parameters:     void
*   returns:        void
*   comments:       When no signal received for a certain period of time
*                   An error is generated to stop section control, calibration, ...
****************************************************************************/
static void watchDogAlert(void)
{
    OS_RetriggerTimer(&WatchdogTimer);
    OS_SignalEvent(SWITCH_BOX_EVENT_ERROR,&TcbSectionCtrlTask);
}

/*****************************************************************************
*   function :      polSwitchesTimeout
*   parameters:     void
*   returns:        void
*   comments:       
****************************************************************************/
static void polSwitchesTimeout(void)
{
    OS_RetriggerTimer(&PolSwitchesTimer);
    OS_SignalEvent(SWITCH_BOX_READ_INPUTS_TIME_ELAPSED,&TcbSectionCtrlTask);
}

/*****************************************************************************
*   function :      mainSwitchSendMsgTimeout
*   parameters:     void
*   returns:        void
*   comments:       
****************************************************************************/
static void mainSwitchSendMsgTimeout(void)
{
    OS_RetriggerTimer(&MainSwitchSendMsgTimer);
    OS_SignalEvent(SWITCH_BOX_SEND_SPRAYER_INFO_MSG,&TcbSectionCtrlTask);
}
/*****************************************************************************
*   function :      sectionSwitchesSendMsgTimeout
*   parameters:     void
*   returns:        void
*   comments:       
****************************************************************************/
static void sectionSwitchesSendMsgTimeout(void)
{
    OS_RetriggerTimer(&SectionSwitchesSendMsgTimer);
    OS_SignalEvent(SWITCH_BOX_SEND_SWITCH_INFO_MSG,&TcbSectionCtrlTask);
}

/*****************************************************************************
*   function :      sectionCtrlTask
*   parameters:     void
*   returns:        void
*   comments:       section controller task.
****************************************************************************/
static void sectionCtrlTask(void)
{
    //static UI16_t cntToOneHz = 0;
    char receivedEvents;
    IbsRxMessage_t rxMessage;

    while(TRUE)
    {
        receivedEvents = OS_WaitEvent( SWITCH_BOX_READ_INPUTS_TIME_ELAPSED |
                                       SWITCH_BOX_HANDLE_STATUS_MSG_ACTBOX |
                                       SWITCH_BOX_SEND_SPRAYER_INFO_MSG    |
                                       SWITCH_BOX_SEND_SWITCH_INFO_MSG     |
                                       SWITCH_BOX_EVENT_ERROR              );


        if(receivedEvents & SWITCH_BOX_READ_INPUTS_TIME_ELAPSED)
        {
            ServiceSwitchBoxHandleSwitch();
        }

        if(receivedEvents & SWITCH_BOX_HANDLE_STATUS_MSG_ACTBOX)
        {
            if(OS_GetMailCond(&MailboxSectionCtrlSetup, &rxMessage) == !0)
            {
                //Problem mailbox
            }
            else
            {
                ServiceSwitchBoxHandleStatusMsg((UI08_t*)&rxMessage);

            }  
        }

        if(receivedEvents & SWITCH_BOX_SEND_SPRAYER_INFO_MSG)
        {
            SendMainSwitchMsg();
        }

        if(receivedEvents & SWITCH_BOX_SEND_SWITCH_INFO_MSG)
        {
            SendSectionSwitchesMsg();
        }

        if(receivedEvents & SWITCH_BOX_EVENT_ERROR)
        {
        }
    }
}

/****************************************************************************/
/**                                                                        **/
/**                               EOF                                      **/
/**                                                                        **/
/****************************************************************************/
