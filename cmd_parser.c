/*
                             *******************
******************************* C SOURCE FILE *******************************
**                           *******************                           **
**                                                                         **
** project   : General modules                                             **
** filename  : cmdparser.c
**                                                                         **
*****************************************************************************
**                                                                         **
** Copyright (c) 2012, SBG Precision Farming/Navtronics Bvba               **
** All rights reserved.                                                    **
**                                                                         **
*****************************************************************************

VERSION HISTORY:
----------------

Version     : 1
Date        : 20121002
Revised by  : Koen Smeuninx 
Description : * Original version.

FILE INFORMATION:
-----------------
Parses the users commands received over the trace interface and calls the
handler functions.  

*/

/****************************************************************************/
/**                                                                        **/
/**                     MODULES USED                                       **/
/**                                                                        **/
/****************************************************************************/
#include <stdlib.h>
#include "cmd_parser.h"
#include "uart_service.h"
#include "led_service.h"
#include "lpc17xx_adc.h"
#include "lpc17xx_gpio.h"
#include "lpc17xx_dac.h"
#include "pwm.h"
#include "eeprom_i2c.h"
#include "hw_platform.h"
#include "log.h"

/****************************************************************************/
/**                                                                        **/
/**                     DEFINITIONS AND MACROS                             **/
/**                                                                        **/
/****************************************************************************/
#define COMMAND_BUFFER_SIZE 64
/****************************************************************************/
/**                                                                        **/
/**                     TYPEDEFS AND STRUCTURES                            **/
/**                                                                        **/
/****************************************************************************/
typedef struct CommandEntry_s
{
    UI08_t cmd;
    UI08_t nrOfParams;
}CommandEntry_t;
/****************************************************************************/
/**                                                                        **/
/**                     PROTOTYPES OF LOCAL FUNCTIONS                      **/
/**                                                                        **/
/****************************************************************************/
static void printMenu(void);
static void parseBuffer(void);
static void handleLedCommand(UI08_t ledId, UI08_t status);
static void handleAdcCommand(UI08_t adcInput);
static void handlePwmCommand(UI08_t pwmId, UI16_t frequency, UI08_t pwmValue);
static void handleDacCommand(UI16_t dacValue);
static void handleInputConfCommand(UI08_t InputId,UI08_t mode);
static void handlePwrCommand(UI08_t outputId, UI08_t status);
static void handle5vOutputCommand(UI08_t status);
static void handleEepromCommand(UI08_t operation, UI16_t address, UI08_t value);


/****************************************************************************/
/**                                                                        **/
/**                     EXPORTED VARIABLES                                 **/
/**                                                                        **/
/****************************************************************************/

/****************************************************************************/
/**                                                                        **/
/**                     GLOBAL VARIABLES                                   **/
/**                                                                        **/
/****************************************************************************/
const char *CmdMenu=
    "Test Commands:\r"
    "m: This menu.\r"
    "l,[id],[status (0-1-2)]: clear-set-toggle led.\r"
    "x,[channel(0-3)]: select mux channel.\r"
    "a,[input id(1-5  5 = mux)]: read ADC input\r"
    "p,[id(1,3)],[freqHz],[pwm val(0-100)]: set pwm value\r"
    //"s,[id(0=L,1=R)],[status(0=dis, 1=ena)]:  enable or disable the sectionCTRL valves"
    "d,[dacVal(0-1023)], set DAC value.\r"
    "c,[id(1-4)],[mode(0=0-10V,1=0-5V,2=4-20mA)]: configure input mode.\r"
    "o,[id(0=mani,1=Lock Valve)],[status(0=dis,1=ena)]: clear/set PWR output.\r"
    "e,[status(0=dis,1=ena)], disable/enable 5V ouput.\r"
    "E,[Operation (0=W,1=R)],[address (1-2048)],[value(0-255)]: Write/read EEPROM byte.\r";

static UI08_t CmdBuffer[COMMAND_BUFFER_SIZE];
static UI08_t CurrentBufferSize;
    
/****************************************************************************/
/**                                                                        **/
/**                     EXPORTED FUNCTIONS                                 **/
/**                                                                        **/
/****************************************************************************/

/*************************************************************************
* function: ParseCmdByte  
* comments: This function is called for every received byte.
*           it stores the bytes into a buffer until a carriage
*           return is received. Then it calls the parse function.              
**************************************************************************/
void ParseCmdByte(UI08_t cmdByte)
{

   // LogWriteBytes(&cmdByte,1);
    
    if (cmdByte == '\r')
    {
        //add \0 to the end of the string. 
        if (CurrentBufferSize < COMMAND_BUFFER_SIZE)
        {
            CmdBuffer[CurrentBufferSize] = '\0';
        }
        else
        {
            CmdBuffer[CurrentBufferSize-1] = '\0';
        }
        if (CurrentBufferSize != 0)
        {
            parseBuffer();  
        }
        CurrentBufferSize = 0;
    }
    else
    {
        if (CurrentBufferSize < COMMAND_BUFFER_SIZE)
        {
            CmdBuffer[CurrentBufferSize++] = cmdByte;
        }
        else
        {
            //do nothing until enter pressed. 
        }
    }
}


/****************************************************************************/
/**                                                                        **/
/**                     LOCAL FUNCTIONS                                    **/
/**                                                                        **/
/****************************************************************************/
/*************************************************************************
* function: parseBuffer  
* comments: parses a fully received command.
*           it expects a command with this format: 
*           <commandID>,<1st parameters>,<2nd parameter>,... up to 5 
*           parameters. 
**************************************************************************/
static void parseBuffer(void)
{
    UI08_t  cmd;
    UI08_t  valueIndex = 0;
    UI16_t  values[5];
    char    tokenToFind[] = ",";
    char    *resultPtr;

    resultPtr = strtok( (char *) &CmdBuffer, tokenToFind );
    if (resultPtr == NULL)
    {
         LogWriteString("Invalid or unknown command."); 
         return;
    }
    
    cmd = *resultPtr;
    
    while( resultPtr != NULL ) 
    {
        resultPtr = strtok( NULL, tokenToFind );
        if (resultPtr != NULL)
        {
            if (valueIndex > 4)
            {
                LogWriteString("Command contains more delimiters than allowed."); 
                return;   
            }
            values[valueIndex] = atoi(resultPtr);
            valueIndex++;
        }
    }
    switch (cmd)
    {
        case 'm':
            printMenu();
            break;
        case 'l':
            handleLedCommand(values[0],values[1]);
            break;
        case 'a':
            handleAdcCommand(values[0]);
            break;
        case 'p':
            handlePwmCommand(values[0],values[1],values[2]);
            break;
        /*case 's':
            handlesectionCTRLCommand(values[0],values[1]);
            break;*/
        case 'd':
            handleDacCommand(values[0]);
            break;
        case 'c':
            handleInputConfCommand(values[0],values[1]);
            break;
        case 'o':
            handlePwrCommand(values[0],values[1]);
            break;
        case 'e':
            handle5vOutputCommand(values[0]);
            break;
        case 'E':
            handleEepromCommand(values[0],values[1],values[2]);
            break;
        default:
            LogWriteString("Invalid or unknown command.");    
            return;
    }
    LogWriteString("READY.");    
}

/*************************************************************************
* function: printMenu  
* comments: prints a menu for the user which lists all the available
*           commands. 
**************************************************************************/
static void printMenu(void)
{
    UI16_t menuSize = strlen(CmdMenu);
    UI08_t bytesToSend;
    const char * menuPtr = CmdMenu;
    while (menuSize > 0)
    {

        if (menuSize > UART_BUFFER_SIZE - 1)
        {
            bytesToSend = UART_BUFFER_SIZE - 1;
        }else
        {
            bytesToSend = menuSize;
        }
        LogWriteBytes((UI08_t*)menuPtr,bytesToSend);
        menuSize -= bytesToSend;
        menuPtr += bytesToSend;
    }
}

/*************************************************************************
* function: handleLedCommand   
* comments: handles a led command. status is value from 0 to 2.
*           where 0 = LED Off, 1 = LED On, 2 = LED Toggle.
*           add 7 tot the led id because the leds are on GPIO port 7,8 and
*           9. 
*           this function calls the exported function HandleLedCommand
*           of the led service.
**************************************************************************/
static void handleLedCommand(UI08_t ledId, UI08_t status)
{
    //check parameters.  
    LedCommand_t ledCommand;

    if (ledId > 2)
    {
        LogWriteString("Invalid Led Id.");
    }
    if (status > LED_STATUS_TOGGLE)
    {
        LogWriteString("Invalid status");
    }

    ledCommand.gpioID = ledId+7;
    
    if (status == 0)
    {
        status = 1;
    }
    else
    {
        status = 0;
    }
    ledCommand.ledStatus = (LedStatus_t)status;
    HandleLedCommand(&ledCommand);
}

/*************************************************************************
* function: handleAdcCommand   
* comments: handles an adc command. Inputs nr are not mapped to the ADC 
*           input numbers.  Remapping is needed.  
**************************************************************************/
static void handleAdcCommand(UI08_t adcInput)
{
    UI16_t adcValue;
    switch (adcInput)
    {
        case 1:   
            adcValue = ADC_ChannelGetData(LPC_ADC,ADC_CHANNEL_2);
            break;
        case 2:   
            adcValue = ADC_ChannelGetData(LPC_ADC,ADC_CHANNEL_1);
            break;
        case 3:   
            adcValue = ADC_ChannelGetData(LPC_ADC,ADC_CHANNEL_0);
            break;
        case 4:   
            adcValue = ADC_ChannelGetData(LPC_ADC,ADC_CHANNEL_4);
            break;
        case 5:   
            adcValue = ADC_ChannelGetData(LPC_ADC,ADC_CHANNEL_5);
            break;
        default:
            LogWriteString("Invalid ADC id");
            return;
    }
    LogWriteString("ADC input %d: %d", adcInput, adcValue);
}

/*************************************************************************
* function: handlePwmCommand   
* comments: handles a pwm command.
**************************************************************************/
static void handlePwmCommand(UI08_t pwmId, UI16_t frequency, UI08_t pwmValue)
{
    if (pwmId != 1 && pwmId != 3)
    {
        LogWriteString("Only pwm ID 1 and 3 are supported");
        return;
    }
    PwmInit(frequency,100);
    PwmChInit(pwmId,pwmValue);
}

/*************************************************************************
* function: handleDacCommand   
* comments: Sets a value to the digital to analog converter
**************************************************************************/
static void handleDacCommand(UI16_t dacValue)
{
    DAC_UpdateValue(LPC_DAC,dacValue);
}

/*************************************************************************
* function: handleInputConfCommand  
* comments: three possible combinations.  
*           0-10V.  0-10V H and 4-20L
*           0-5V.   0-10V L and 4-20L
*           4-20mA  0-10V L and 4-20H
**************************************************************************/
static void handleInputConfCommand(UI08_t inputId,UI08_t mode)
{
    //InputModeConf(inputId, mode);
}

/*************************************************************************
* function: handlePwrCommand  
* comments: Sets the power outputs to a value.
*           we have two PWR outputs, 0 = PWR_MANIFOLD
*                                    1 = PWR LOCK
*
**************************************************************************/
static void handlePwrCommand(UI08_t outputId, UI08_t status)
{
    switch (outputId)
    {
        case 0:
            if (status == 0)
            {
                GPIO_ClearValue(GPIO_PORT_2, 0x01 << 4); 
            }
            else
            {
                GPIO_SetValue(GPIO_PORT_2, 0x01 << 4); 
            }
            break;
        case 1:
            if (status == 0)
            {
                GPIO_ClearValue(GPIO_PORT_2, 0x01 << 5); 
            }
            else
            {
                GPIO_SetValue(GPIO_PORT_2, 0x01 << 5); 
            }
            break;
        default:
            LogWriteString("Invalid PWR output");
    }
}

/*************************************************************************
* function: handle5vOutputCommand  
* comments: Set/clear the 5V output.  0 = clear, any other value is set.
**************************************************************************/
static void handle5vOutputCommand(UI08_t status)
{
    if (status == 0)
    {
        GPIO_ClearValue(GPIO_PORT_0, 0x01 << 6); 
    }
    else
    {
        GPIO_SetValue(GPIO_PORT_0, 0x01 << 6); 
    }
}

/*************************************************************************
* function: handleEepromCommand  
* comments: Read/write a byte from/to the eeprom.
*           operation = 0: read, any other value = write
*           
**************************************************************************/
static void handleEepromCommand(UI08_t operation, UI16_t address, UI08_t value)
{
    if (operation == 0)
    {
        EeI2cWrite(address,1,&value);
    }
    else
    {     
        EeI2cRead(address,1,&value);
        LogWriteString("Byte at Address 0x%04X: %d",address, value);    
    }
}

/****************************************************************************/
/**                                                                        **/
/**                               EOF                                      **/
/**                                                                        **/
/****************************************************************************/
