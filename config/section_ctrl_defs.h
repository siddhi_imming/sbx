/*
                             *******************
******************************* C HEADER FILE *******************************
**                           *******************                           **
**                                                                         **
** project   : section controller LPC1768                                  **
** filename  : section_ctrl_defs.h                                         **
**                                                                         **
*****************************************************************************
**                                                                         **
** Copyright (c) 2012, SBG Precision Farming/Navtronics Bvba               **
** All rights reserved.                                                    **
**                                                                         **
*****************************************************************************

VERSION HISTORY:
----------------
Version     : 1
Date        : 20130129
Revised by  : Koen Smeuninx
Description : * c style changes

Version     : 1
Date        : 20120225
Revised by  : Thijs Esselink
Description : * Original version.

FILE INFORMATION:
-----------------

Contains the defines used by the sectionCTRL task, service
*/

#ifndef _SECTION_CTRL_DEFS_INCLUDED
#define _SECTION_CTRL_DEFS_INCLUDED

/****************************************************************************/
/**                                                                        **/
/**                     MODULES USED                                       **/
/**                                                                        **/
/****************************************************************************/

#include "sysconf.h"

/****************************************************************************/
/**                                                                        **/
/**                     DEFINITIONS AND MACROS                             **/
/**                                                                        **/
/****************************************************************************/


#define MAX_NUMBER_OF_SECTIONS              0x10    


/****************************************************************************/
/**                                                                        **/
/**                     TYPEDEFS AND STRUCTURES                            **/
/**                                                                        **/
/****************************************************************************/
/*
typedef enum sectionControlType_e
{
        MODULE_MUELLER_SWITCH_BOX,
        MODULE_VALVE_CONTROL,
        MODULE_MOTOR_CONTROL
}sectionControlType_t; 
*/

typedef struct sectionOutputControl_tag
{
    UI08_t sectionSetup[8];
}sectionOutputControl_t;



typedef enum RunStatus_tag
{
    RUN_STATUS_NORMAL,
    RUN_STATUS_CALIBRATION,
    RUN_STATUS_CALIBRATION_FAILED,
    RUN_STATUS_MANUAL_CONTROL
}RunStatus_t;







/*************************************************************************
PACK 1 
**************************************************************************/
#pragma pack(1)

typedef enum SectionStatus_e
{
    SECTION_STATUS_OFF,
    SECTION_STATUS_ON
}SectionStatus_t;

typedef enum SectionOperatingStatus_e
{
    SECTION_OPERATING_STATUS_OFF,
    SECTION_OPERATING_STATUS_ON
}SectionOperatingStatus_t;

typedef enum SectionCommand_e
{
    SECTION_COMMAND_OFF,
    SECTION_COMMAND_ON
}SectionCommand_t;

typedef enum SectionIdentifier_e
{
    SECTION_ID_INVALID      =   0,
    SECTION_ID_MIN          =   1,
    SECTION_ID_MAX          =   112,
    SECTION_RESERVED_START  =   SECTION_ID_MAX + 1,
    SECTION_RESERVED_STOP   =   254,
    SECTION_ALL_SECTIONS    =   255
}SectionIdentifier_t;

typedef enum NumberOfSections_e
{
    NUMBER_OF_SECTIONS_MIN = 0,
    NUMBER_OF_SECTIONS_MAX = SECTION_ID_MAX,
}NumberOfSections_t;

typedef enum ConfigurationOptionflag_e
{
    CONFIG_OPTION_NOT_AVAILABLE,
    CONFIG_OPTION_AVAILABLE
}ConfigurationOptionflag_t;

typedef enum ImplementType_e
{
    IT_FRONT_MOUNTED,
    IT_REAR_MOUNTED,
    IT_TRAILED
}ImplementType_t;

typedef enum SteeringType_e
{
    ST_NONE,
    ST_DRAWBAR_STEERING,
    ST_AXEL_WHEEL_STEERING
}SteeringType_t;

typedef struct BoomConfigurationI_s
{
    I16_t drpXOffset;
    I16_t drpYOffset;
    NumberOfSections_t  numberOfSections;
    UI08_t configurationExchangeKey;
    UI08_t reserved;
}BoomConfigurationI_t;

typedef enum SectionCollectionId_e
{
    SECTION_ID_1_56     = 0x01,
    SECTION_ID_57_112   = 0x02,
    STATUS_INFORMATION      = 0x23,
    SEND_LAST_SECTION_STATUS= 0xFF
}SectionCollectionId_t;

typedef struct BoomConfigurationII_s
{
    ImplementType_t implementType : 4;
    SteeringType_t  steeringType  : 4;
    I16_t brpXOffset;
    I16_t brpYOffset;
    I16_t drawBarSteeringOffsetValue;
}BoomConfigurationII_t;

typedef struct EndSectionBitFlags_s
{
    UI08_t section1IsEnd : 1;
    UI08_t section2IsEnd : 1;
    UI08_t section3IsEnd : 1;
    UI08_t section4IsEnd : 1;
    UI08_t section5IsEnd : 1;
    UI08_t section6IsEnd : 1;
    UI08_t section7IsEnd : 1;
    UI08_t section8IsEnd : 1;
    UI08_t section9IsEnd : 1;
    UI08_t section10IsEnd : 1;
    UI08_t section11IsEnd : 1;
    UI08_t section12IsEnd : 1;
    UI08_t section13IsEnd : 1;
    UI08_t section14IsEnd : 1;
    UI08_t section15IsEnd : 1;
    UI08_t section16IsEnd : 1;
}EndSectionBitFlags_t;


typedef struct BoomConfigurationIIIOptions_s
{
    ConfigurationOptionflag_t   hasFreewheelValve   :1 ;
    ConfigurationOptionflag_t   hasPumpControl      :1 ;
    ConfigurationOptionflag_t   driverTypeMotor     :1 ;    
    ConfigurationOptionflag_t   driverValveInverted :1 ;
    ConfigurationOptionflag_t   reserved            :4 ;
}BoomConfigurationIIIOptions_t;  

typedef enum ActBoxLProtocol_s
{
    PROTOCOL_NONE,
    AMAZONE_AMACLICK,
    MULLER_S_BOX,
    TEEJET_MATRIX,
    DELVANO_COMMANDER_UNIT_II,
    HARDI_HC5500
}ActBoxLProtocol_t;  

typedef struct BoomConfigurationIII_s
{
    EndSectionBitFlags_t endSectionBitFlags;
    UI08_t  endSectionOffsetValue;
    BoomConfigurationIIIOptions_t   options; 
    ActBoxLProtocol_t  protocol;
    UI08_t configurationId; 
    UI08_t reserved;
}BoomConfigurationIII_t;

typedef struct SectionConfiguration_s
{
    SectionIdentifier_t sectionIdentifier;
    I16_t   derXOffset;
    I16_t   derYOffset;
    UI16_t  sectionWidth;
}SectionConfiguration_t;

typedef union BoomConfigMessageData_u
{
    BoomConfigurationI_t    boomConfigI;
    BoomConfigurationII_t   boomConfigII;
    BoomConfigurationIII_t  boomConfigIII;
    SectionConfiguration_t  sectionConfig;
}BoomConfigMessageData_t;

#pragma pack()


typedef union SectionCollectionData_u
{
    struct 
    {
        UI08_t sections1_8;
        UI08_t sections9_16;
        UI08_t sections17_24;
        UI08_t sections25_32;
        UI08_t sections33_40;
        UI08_t sections41_48;
        UI08_t sections49_56;
    };
    struct 
    {
        UI08_t sections57_64;
        UI08_t sections65_72;
        UI08_t sections73_80;
        UI08_t sections81_88;
        UI08_t sections89_96;
        UI08_t sections97_104;
        UI08_t sections105_112;
    };
    UI08_t sectionBytes[7];
}SectionCollectionData_t;
#pragma pack()



/****************************************************************************/
/**                                                                        **/
/**                     EXPORTED VARIABLES                                 **/
/**                                                                        **/
/****************************************************************************/


/****************************************************************************/
/**                                                                        **/
/**                     EXPORTED FUNCTIONS                                 **/
/**                                                                        **/
/****************************************************************************/

#endif

/****************************************************************************/
/**                                                                        **/
/**                               EOF                                      **/
/**                                                                        **/
/****************************************************************************/
