/*
                             *******************
******************************* C HEADER FILE *******************************
**                           *******************                           **
**                                                                         **
** project   : SCU LPC1768                                                 **
** filename  : config.h                                                    **
**                                                                         **
*****************************************************************************
**                                                                         **
** Copyright (c) 2012, SBG Precision Farming/Navtronics Bvba               **
** All rights reserved.                                                    **
**                                                                         **
*****************************************************************************

VERSION HISTORY:
----------------

Version     : 1
Date        : 20121011
Revised by  : Koen Smeuninx
Description : * Original version.


FILE INFORMATION:
-----------------

contains configuration parameters and functions to store/retrieve
settings.
*/

#ifndef _CONFIG_INCLUDED
#define _CONFIG_INCLUDED

/****************************************************************************/
/**                                                                        **/
/**                     MODULES USED                                       **/
/**                                                                        **/
/****************************************************************************/
#include "cfg_flash.h"
#include "sysconf.h"
#include "section_ctrl_defs.h"
#include "module_defs.h"
#include "ibs_defs_section_ctrl.h"

/****************************************************************************/
/**                                                                        **/
/**                     DEFINITIONS AND MACROS                             **/
/**                                                                        **/
/****************************************************************************/


#define CFG_PARAM_OFFSET  400


/****************************************************************************/
/**                                                                        **/
/**                     TYPEDEFS AND STRUCTURES                            **/
/**                                                                        **/
/****************************************************************************/


typedef enum CfgMemoryType_e
{
    CFG_MEMORY_STATIC_FLASH,
    CFG_MEMORY_DYNAMIC_FLASH,
    CFG_MEMORY_SECTION_CTRL_FLASH
}CfgMemoryType_t;


enum 
{
    CFG_RQ_NR_PARAM,
    CFG_RQ_PARAM,
    CFG_MOD_PARAM,
    CFG_REP_PARAM,
    CFG_REP_NR_PARAM,
    CFG_SET_DEFAULTS,
    CFG_CLEAR_EEPROM
};

enum 
{
    CFG_PARAM_BYTE,
    CFG_PARAM_WORD,
    CFG_PARAM_DWORD,
    CFG_PARAM_CHAR,
    CFG_PARAM_SHORT,
    CFG_PARAM_LONG,
    CFG_PARAM_FLOAT
};

typedef enum ParameterGroup_e
{
    PARAM_GROUP_ISOBUS
}ParameterGroup_t; 

typedef enum BoomConfig_e
{
    BOOM_CONFIG_1,
    BOOM_CONFIG_2,
    BOOM_CONFIG_3,
    SECTION_CONFIG
}BoomConfig_t;

typedef struct DynamicCfgFlash_s
{
    UI32_t  bootLoaderSoftwareVersion;  //bootloader software version
    UI32_t  ecuSoftwareVersion;         //ECU software version
    UI32_t  lastSA;                     //Last assigned IsoBus source address
    UI32_t  flashState;
    UI32_t  checkWord;
    //section controller 
}DynamicCfgFlash_t;

typedef struct SectionCtrlConfig_s
{
    BoomConfigurationI_t    boomConfigurationI;
    BoomConfigurationII_t   boomConfigurationII;
    BoomConfigurationIII_t  boomConfigurationIII;
    SectionConfiguration_t  sectionConfiguration[MAX_NUMBER_OF_SECTIONS];
    UI32_t                  checkWord;
}SectionCtrlConfig_t;

typedef struct Cfgflash_s
{
    StaticCfgFlash_t        staticCfgFlash;
    DynamicCfgFlash_t       dynamicCfgFlash;
    SectionCtrlConfig_t     sectionCtrlCfg;
}CfgFlash_t;


typedef struct 
{
    CfgFlash_t      cfgFlash;
} Cfg_t;

/****************************************************************************/
/**                                                                        **/
/**                     EXPORTED VARIABLES                                 **/
/**                                                                        **/
/****************************************************************************/

extern Cfg_t Cfg;

/****************************************************************************/
/**                                                                        **/
/**                     EXPORTED FUNCTIONS                                 **/
/**                                                                        **/
/****************************************************************************/

/*************************************************************************
* function:     InitCfg  
* parameters:   void 
* returns:      void    
* comments:     inits the configuration, checks the eeprom checksum,...  
**************************************************************************/
void InitCfg (void);

/*************************************************************************
* function:     CfgRestore  
* parameters:   void
* returns:      Bool_t: true when successful    
* comments:     Restores the configuration parameters  
**************************************************************************/
Bool_t CfgRestore (void);

/*************************************************************************
* function:     CfgSave  
* parameters:   void
* returns:      void
* comments:     Saves the configuration parameters  
**************************************************************************/
void CfgSave (CfgMemoryType_t );

/*************************************************************************
* function:     CfgReportSettings
* parameters:   void
* returns:      void    
* comments:     Reports configuration settings via uart
**************************************************************************/
void CfgReportSettings (void);

/*************************************************************************
* function:     CfgReportParameter
* parameters:   UI08_t cId: the controller that should respond.
*               UI08_t pId: the partner that sent the request
*               UI16_t paramId: id of the requested parameter
* returns:      void    
* comments:     Reports a configuration parameter with paramId via CAN bus.
**************************************************************************/                
void CfgReportParameter(UI08_t cId, UI08_t pId, UI16_t paramId);

/*************************************************************************
* function:     CfgModifyParameter
* parameters:   cId: the controller that should respond.
*               pId: the partner that sent the request
*               UI16_t paramId: id of the parameter
*               UI08_t paramType: type of the parameter
*               UI08_t *dataPtr: pointer tot the param data
* returns:      void    
* comments:     Modifies a configuration parameter with paramId via CAN bus.
**************************************************************************/  
void CfgModifyParameter(UI08_t cId, UI08_t pId, UI16_t paramId, 
                        UI08_t paramType, UI08_t *dataPtr);

/*************************************************************************
* function:     ReportAllParameters
* parameters:   UI08_t cId: the controller that should respond.
*               UI08_t pId: the partner that sent the request
* returns:      void    
* comments:     Reports all the configuration parameters.
**************************************************************************/  
void ReportAllParameters(UI08_t cId, UI08_t pId);

/*************************************************************************
* function:     CfgSetGroupToDefault
* parameters:   ParameterGroup_t parameterGroup: if of the group the 
*               parameter belongs to.
* returns:      void    
* comments:     Set all the parameters belonging to a group with 
*               groupid to their default values.
**************************************************************************/  
void CfgSetGroupToDefault(ParameterGroup_t parameterGroup);

/*************************************************************************
* function:     ReportBoomConfigurationI   
* parameters:   cId: the controller that should respond.
*               pId: the partner that sent the request    
* returns:      void
* comments:     Composes and sends the boom configuration I message
**************************************************************************/
void CfgReportBoomConfigurationI(UI08_t cId, UI08_t pId);


/*************************************************************************
* function:     ReportBoomConfigurationII   
* parameters:   cId: the controller that should respond.
*               pId: the partner that sent the request    
* returns:      void
* comments:     Composes and sends the boom configuration II message
**************************************************************************/
void CfgReportBoomConfigurationII(UI08_t cId, UI08_t pId);

/*************************************************************************
* function:     ReportBoomConfigurationIII   
* parameters:   cId: the controller that should respond.
*               pId: the partner that sent the request    
* returns:      void
* comments:     Composes and sends the boom configuration III message
**************************************************************************/
void CfgReportBoomConfigurationIII(UI08_t cId, UI08_t pId);

/*************************************************************************
* function:     ReportBoomConfigurationII   
* parameters:   cId: the controller that should respond.
*               pId: the partner that sent the request  
*               SectionIdentifier_t sectionId:  Section Nr.
* returns:      void
* comments:     Composes and sends the boom configuration II message
**************************************************************************/
void CfgReportSectionConfiguration(UI08_t cId, UI08_t pId,SectionIdentifier_t sectionId);

/*************************************************************************
* function:     CfgHandleBoomConfiguration 
* parameters:   BoomConfig_t boomConfigType: type of config, I II III or section
*               BoomConfigMessageData_t *messageDataPtr: pointer to union containing 
*               the data depending on the config type.
* returns:      void
* comments:     Handler which is called when the sprayer controller 
*               sends a new configuration
**************************************************************************/
void CfgHandleBoomConfiguration(BoomConfig_t boomConfigType,BoomConfigMessageData_t *messageDataPtr);


/*************************************************************************
* function:     CfgGetModuleCfg  
* parameters:   ModuleConfiguration_t *moduleCfgPtr: ptr to the config stuct to fill
* returns:      void
* comments:     translates config data in moduleCfg and stores it into the passed pointer
**************************************************************************/
void CfgGetModuleCfg(ModuleConfiguration_t *moduleCfgPtr);







#endif

/****************************************************************************/
/**                                                                        **/
/**                              EOF                                       **/
/**                                                                        **/
/****************************************************************************/
