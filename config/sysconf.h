/*
                             *******************
******************************* C HEADER FILE *******************************
**                           *******************                           **
**                                                                         **
** project   : LPC1768 Projects                                            **
** filename  : sysconf.h                                                   **
**                                                                         **
*****************************************************************************
**                                                                         **
** Copyright (c) 2012, SBG Precision Farming/Navtronics Bvba               **
** All rights reserved.                                                    **
**                                                                         **
*****************************************************************************


VERSION HISTORY:
----------------

Version     : 1
Date        : 20120926
Revised by  : Koen Smeuninx
Description : * Original version.

FILE INFORMATION:
-----------------
This file contains all the defines which are needed and are not already 
defined by the CMSIS includes, and also additional defines used for software
version, hw version, ...

*/


#ifndef _SYSCONF_INCLUDED
#define _SYSCONF_INCLUDED

/****************************************************************************/
/**                                                                        **/
/**                     MODULES USED                                       **/
/**                                                                        **/
/****************************************************************************/
#include "stddefs.h"

/****************************************************************************/
/**                                                                        **/
/**                     DEFINITIONS AND MACROS                             **/
/**                                                                        **/
/****************************************************************************/
#define ET_NXP                          0
#define STU                             1
#define SCU                             2


#define MODULE_MUELLER_SWITCH_BOX       0
#define MODULE_VALVE_CONTROL            1
#define MODULE_MOTOR_CONTROL            2       

#define BOARD_TYPE                     SCU

#define HW_VERSION                      1  // 0..255
#define SW_VERSION_MAJOR                1  // 0..16
#define SW_VERSION_MINOR                0  // 0..16
#define SW_VERSION_BUILD                3  // 0..255


#define PARTNER_TIMEOUT_INFINITE        0
#define PARTNER_TIMEOUT_DEFAULT         30000

//when using the cortex m3 the rtos is not using enable/disable registers for interrupts
//but it sets the base priority to 128 or 0x80
//all interrupts with prio higher (lower number is higher priority) than this will not be executed.   
#define UART_ISR_PRIORITY               (0xFE)
#define CAN_ISR_PRIORITY                (0xF0)
#define PWM_IRQ_PRIORITY                (0xE0)


#define CURRENT_ECU_SOFTWARE_VERSION    ( 0x0000 | (SW_VERSION_MAJOR << 12) | ( SW_VERSION_MINOR << 8 ) | SW_VERSION_BUILD )

#define ECU_END                     0x67FFF    //do not change this parameter, the config start and end also use blocks

#ifdef BOOTLOADER
#define ECU_START                   0x10000
#else
#define ECU_START                   0x00000
#endif

#define BOOTLOADER_END              0x0FFFF
#define BOOTLOADER_START            0x00000

#define NVIC_START_ADDRESS          ECU_START 


#define GPIO_PORT_0 	                ((0))	/**< PORT 0*/
#define GPIO_PORT_1 	                ((1))	/**< PORT 1*/
#define GPIO_PORT_2 	                ((2))	/**< PORT 2*/
#define GPIO_PORT_3 	                ((3))	/**< PORT 3*/
#define GPIO_PORT_4 	                ((4))	/**< PORT 4*/

#define GPIO_PIN_0 	                    ((0))	/**< PIN 0*/
#define GPIO_PIN_1 	                    ((1))	/**< PIN 1*/
#define GPIO_PIN_2 	                    ((2))	/**< PIN 2*/
#define GPIO_PIN_3 	                    ((3))	/**< PIN 3*/
#define GPIO_PIN_4 	                    ((4))	/**< PIN 4*/
#define GPIO_PIN_5 	                    ((5))	/**< PIN 5*/
#define GPIO_PIN_6 	                    ((6))	/**< PIN 6*/
#define GPIO_PIN_7 	                    ((7))	/**< PIN 7*/
#define GPIO_PIN_8 	                    ((8))	/**< PIN 8*/
#define GPIO_PIN_9 	                    ((9))	/**< PIN 9*/
#define GPIO_PIN_10 	                ((10))	/**< PIN 10*/
#define GPIO_PIN_11 	                ((11))	/**< PIN 11*/
#define GPIO_PIN_12	                    ((12))	/**< PIN 12*/
#define GPIO_PIN_13 	                ((13))	/**< PIN 13*/
#define GPIO_PIN_14 	                ((14))	/**< PIN 14*/
#define GPIO_PIN_15	                    ((15))	/**< PIN 15*/
#define GPIO_PIN_16 	                ((16))	/**< PIN 16*/
#define GPIO_PIN_17	                    ((17))	/**< PIN 17*/
#define GPIO_PIN_18 	                ((18))	/**< PIN 18*/
#define GPIO_PIN_19	                    ((19))	/**< PIN 19*/
#define GPIO_PIN_20 	                ((20))	/**< PIN 20*/
#define GPIO_PIN_21 	                ((21))	/**< PIN 21*/
#define GPIO_PIN_22	                    ((22))	/**< PIN 22*/
#define GPIO_PIN_23 	                ((23))	/**< PIN 23*/
#define GPIO_PIN_24 	                ((24))	/**< PIN 24*/
#define GPIO_PIN_25 	                ((25))	/**< PIN 25*/
#define GPIO_PIN_26 	                ((26))	/**< PIN 26*/
#define GPIO_PIN_27 	                ((27))	/**< PIN 27*/
#define GPIO_PIN_28 	                ((28))	/**< PIN 28*/
#define GPIO_PIN_29 	                ((29))	/**< PIN 29*/
#define GPIO_PIN_30 	                ((30))	/**< PIN 30*/
#define GPIO_PIN_31 	                ((31))	/**< PIN 31*/

#define LED_PORT                        GPIO_PORT_0
#define GPIO_PIN_LED_RED                GPIO_PIN_8
#define GPIO_PIN_LED_BLUE               GPIO_PIN_7
#define GPIO_PIN_LED_GREEN              GPIO_PIN_9

#define CAN_ISOBUS_SPEED                (250000)

    
#define ADC_CHANNEL_WHEEL_SENSOR        2
#define ADC_CHANNEL_PRESSURE_SENSOR_1   1
#define ADC_CHANNEL_PRESSURE_SENSOR_2   0
#define ADC_CHANNEL_BUTTON              4
#define ADC_CHANNEL_MUXED               5

#define INTERRUPT_TABLE_BOOT            0
#define INTERRUPT_TABLE_FLASH           1
#define INTERRUPT_TABLE_RAM             2
#define INTERRUPT_TABLE_MAX             INTERRUPT_TABLE_RAM

#define FLASH_ADDRESS_LIMIT             0x40000

// Define tick settings
#define TICK_RATE_HZ                    250                 // [Hz], maximum is 1 Mhz
#define TICK_RATE_MS                    4                   // [ms], x msec per tick
#define MS_2_TICKS(x)                   ((x)/TICK_RATE_MS)  //Convert msec to ticks
#define NR_OF_TICKS                     1

//I2C settings
#define I2C_FREQUENCY                   400000


// Define EEPROM settings
#define USE_EEPROM                      // use EEPROM
#define EE_I2C_ADDRESS                  0xA0    // device address
#define EE_I2C_ADDRESS_7BIT             0x50 
#define EE_I2C_MEMSIZE                  0x400   //1000 bytes / 8Kbit

//serial port defines
#define UART_PORT1                      0
#define UART_PORT1_BAUDRATE             115200

#define UART_PORT2                      1
#define UART_PORT2_BAUDRATE             115200

#define TRACE_PORT                      UART_PORT1      
#define TRACE_BAUDRATE                  UART_PORT1_BAUDRATE
#define UART_BUFFER_SIZE                256

#define LOG_PORT                        UART_PORT2      
#define LOG_BAUDRATE                    UART_PORT2_BAUDRATE
#define LOG_BUF_SIZE                    UART_PORT2_BUFFER_SIZE


// Define CAN settings
#define CAN_ENABLE_RX_FRAME_BUFFERING
#define CAN_RX_BUFSIZE                  16  // Rx Buffer: 16 frames
#define CAN_ENABLE_TX_FRAME_BUFFERING
#define CAN_TX_BUFSIZE                  64  // Tx Buffer: 16 frames
#define CAN_TX_DIRECT                       // Sending without buffer is possible


#define NR_OF_CAN                   1   // available CAN controllers

// Define ISO11783 settings
#define NR_OF_IBS_CAN               1
#define IBS_USE_CAN0                    // always start using CAN0 if ISO11783 required

//#define IBS_USE_CAN1                  // then start using CAN1
#ifdef IBS_USE_CAN0
  #define IBS_USE_CAN               0
#else
  #define IBS_USE_CAN               1
#endif

#define NR_OF_IBS_DEVICES               1   // this unit can represent 2 controllers, change this define to increase
#define MAIN_CONTROLLER_ID              0

#define NUM_CLAIM_ATTEMPTS              10  // max. ten claim attempts before giving up
//#define IBS_USE_TP                        // use transport protocol
#ifdef IBS_USE_TP
#define NR_OF_IBS_TPS 4                  // maximum simultaneous Transport Protocol Connections
#endif

// Application specific settings

// Conversion definitions
#define RAD2DEG_FACTOR                  57.29577951f // = 180 / PI
#define DEG2RAD_FACTOR                  0.017453293f // = PI / 180
#define DEG2RAD_FACTOR_E4               174.5329252f // = 10000 * PI / 180

// Time out setting for receiving status of terminal
#define TERMINAL_TIMEOUT_MS             1250

#define MIN_PWM_FREQUENCY               16
#define NUM_PWM                         2

#define ALLOC_ERROR 0xFF

#define ADC_CHANNEL_INPUT1              ADC_CHANNEL_2
#define ADC_CHANNEL_INPUT2              ADC_CHANNEL_1
#define ADC_CHANNEL_INPUT3              ADC_CHANNEL_0
#define ADC_CHANNEL_INPUT4              ADC_CHANNEL_4

/****************************************************************************/
/**                                                                        **/
/**                     TYPEDEFS AND STRUCTURES                            **/
/**                                                                        **/
/****************************************************************************/
typedef enum MuxChannelType_e
{
    MUX_CHANNEL_0 = 0,
    MUX_CHANNEL_1 = 1,
    MUX_CHANNEL_2 = 2,
    MUX_CHANNEL_3 = 3,
    
}MuxChannelType_t;

/****************************************************************************/
/**                                                                        **/
/**                     TYPEDEFS AND STRUCTURES                            **/
/**                                                                        **/
/****************************************************************************/

/****************************************************************************/
/**                                                                        **/
/**                     EXPORTED VARIABLES                                 **/
/**                                                                        **/
/****************************************************************************/

/****************************************************************************/
/**                                                                        **/
/**                     EXPORTED FUNCTIONS                                 **/
/**                                                                        **/
/****************************************************************************/

#endif

/****************************************************************************/
/**                                                                        **/
/**                               EOF                                      **/
/**                                                                        **/
/****************************************************************************/
