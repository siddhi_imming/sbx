/*
                             *******************
******************************* C SOURCE FILE *******************************
**                           *******************                           **
**                                                                         **
** project   : sectionCTRLing Controller LPC17xx                           **
** filename  : hwplatform.c                                                **
**                                                                         **
*****************************************************************************
**                                                                         **
** Copyright (c) 2012, SBG Precision Farming/Navtronics Bvba               **
** All rights reserved.                                                    **
**                                                                         **
*****************************************************************************

VERSION HISTORY:
----------------

Version     : 1
Date        : 20121016
Revised by  : Koen Smeuninx
Description : * c style changes

Version     : 1
Date        : 20120918
Revised by  : Koen Smeuninx
Description : * Original version.

File comments:
--------------

this unit contains all the functions specific for hardware initialization.

*/

/****************************************************************************/
/**                                                                        **/
/**                     MODULES USED                                       **/
/**                                                                        **/
/****************************************************************************/

#include <intrinsics.h>
#include <stdio.h>
#include <assert.h>
#include "sysconf.h"
#include "hw_platform.h"
#include "lpc17xx_pinsel.h"
#include "lpc17xx_gpio.h"
#include "lpc17xx_dac.h"
#include "lpc17xx_adc.h"
#include "lpc17xx_i2c.h"
#include "lpc17xx_spi.h"

/****************************************************************************/
/**                                                                        **/
/**                     DEFINITIONS AND MACROS                             **/
/**                                                                        **/
/****************************************************************************/
#define FCCLK_FREQ 100000000
/****************************************************************************/
/**                                                                        **/
/**                     TYPEDEFS AND STRUCTURES                            **/
/**                                                                        **/
/****************************************************************************/

/****************************************************************************/
/**                                                                        **/
/**                     PROTOTYPES OF LOCAL FUNCTIONS                      **/
/**                                                                        **/
/****************************************************************************/
static void initGpio(void);
static void initUart(void);
static void initDac(void);
static void initAdc(void);
static void initCan(void);
static void initPwm(void);
static void initI2c(void);
static void initSPI(void);
/****************************************************************************/
/**                                                                        **/
/**                     EXPORTED VARIABLES                                 **/
/**                                                                        **/
/****************************************************************************/


/****************************************************************************/
/**                                                                        **/
/**                     GLOBAL VARIABLES                                   **/
/**                                                                        **/
/****************************************************************************/

/****************************************************************************/
/**                                                                        **/
/**                     EXPORTED FUNCTIONS                                 **/
/**                                                                        **/
/****************************************************************************/
/*************************************************************************
* Function:    InitHW
* comments:    This function initializes the hardware (clock,GPIO, flash)
**************************************************************************/
void InitHW(void)
{
    SystemInit();
    initGpio();
    initUart();
   // initDac();
    initCan();
   // initAdc();
   // initPwm();
    initI2c();
    initSPI();
}


/*************************************************************************
* function:     Hardware cleanup function in case of an error.   
* comments:     In case of an exception or and OS error this function 
*               gets called, it makes sure all the outputs are disabled.
**************************************************************************/
void HwShutdown(void)
{
    // TODO for switch box.
    //GPIO_ClearValue( GPIO_PORT_2, 0x01 << GPIO_PWR_LOCK );       
    //GPIO_ClearValue( GPIO_PORT_2, 0x01 << GPIO_MOTOR_DRIVER_ENABLE ); 
}
/****************************************************************************/
/**                                                                        **/
/**                     LOCAL FUNCTIONS                                    **/
/**                                                                        **/
/****************************************************************************/


/*************************************************************************
* Function: initGpio
* comments: Reset all GPIO pins to default, afterward sets the hardware
*           specific init 
**************************************************************************/
static void initGpio(void)
{
    PINSEL_CFG_Type pinCfg;
    UI32_t  pinConfigMask = 0;
    
    //PORT0 BEGIN
    pinConfigMask = 0;
    pinCfg.Portnum = LED_PORT;
    pinCfg.Funcnum = PINSEL_FUNC_0;
    pinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
    pinCfg.Pinmode = PINSEL_PINMODE_PULLUP;

    //Led outputs
    pinCfg.Pinnum = GPIO_PIN_7;   //LED OUTPUT1
    pinConfigMask |= ( 0x01 << GPIO_PIN_7);
    PINSEL_ConfigPin(&pinCfg);
    pinCfg.Pinnum = GPIO_PIN_8;   //LED OUTPUT2
    pinConfigMask |= ( 0x01 << GPIO_PIN_8);
    PINSEL_ConfigPin(&pinCfg);
    pinCfg.Pinnum = GPIO_PIN_9;   //LED OUTPUT3
    pinConfigMask |= ( 0x01 << GPIO_PIN_9);
    PINSEL_ConfigPin(&pinCfg);

    GPIO_SetDir(LED_PORT, pinConfigMask, 1);           /* LEDs on PORT2 defined as Output    */
    GPIO_SetValue(LED_PORT, pinConfigMask);
    //PORT0 END

    //PORT2 BEGIN
    pinConfigMask = 0;
    pinCfg.Portnum = PINSEL_PORT_2;
    pinCfg.Pinnum = GPIO_PIN_1;    
    pinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
    pinCfg.Pinmode = PINSEL_PINMODE_PULLUP;
    pinCfg.Funcnum = PINSEL_FUNC_0;
    PINSEL_ConfigPin(&pinCfg);   //input pull up enabled
    //GPIO_SetDir(GPIO_PORT_2, pinConfigMask, 1);      
    //GPIO_ClearValue(GPIO_PORT_2, pinConfigMask);
    //PORT2 END
}

/*************************************************************************
* function: initUart    
* comments:   
**************************************************************************/
static void initUart(void)
{
    PINSEL_CFG_Type pinCfg;
    pinCfg.Funcnum = PINSEL_FUNC_1;
    pinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
    pinCfg.Pinmode = PINSEL_PINMODE_PULLUP;
    pinCfg.Pinnum = GPIO_PIN_2;
    pinCfg.Portnum = GPIO_PORT_0;
    PINSEL_ConfigPin(&pinCfg);
    pinCfg.Pinnum = GPIO_PIN_3;
    PINSEL_ConfigPin(&pinCfg);

#if (BOARD_TYPE == STU)
    pinCfg.Pinnum = GPIO_PIN_15;
    PINSEL_ConfigPin(&pinCfg);
    pinCfg.Pinnum = GPIO_PIN_16;
    PINSEL_ConfigPin(&pinCfg);
#endif
}

/*************************************************************************
* function: initDac    
* comments:   
**************************************************************************/
static void initDac(void)
{
    PINSEL_CFG_Type pinCfg;
    pinCfg.Funcnum = PINSEL_FUNC_2;
    pinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
    pinCfg.Pinmode = PINSEL_PINMODE_PULLUP;
    pinCfg.Pinnum = GPIO_PIN_26;
    pinCfg.Portnum = GPIO_PORT_0;
    PINSEL_ConfigPin(&pinCfg);
    DAC_Init(LPC_DAC);
}

/*************************************************************************
* function: initCan    
* comments:   
**************************************************************************/
static void initCan(void)
{
    PINSEL_CFG_Type pinCfg;
#if (BOARD_TYPE == STU)
	pinCfg.Portnum      = GPIO_PORT_0;
	pinCfg.Funcnum      = PINSEL_FUNC_3;
	pinCfg.OpenDrain    = PINSEL_PINMODE_NORMAL;
	pinCfg.Pinmode      = PINSEL_PINMODE_PULLUP;
	pinCfg.Pinnum       = GPIO_PIN_21;
	PINSEL_ConfigPin(&pinCfg);
	pinCfg.Pinnum       = GPIO_PIN_22;
	PINSEL_ConfigPin(&pinCfg);

	pinCfg.Portnum      = GPIO_PORT_0;
	pinCfg.Funcnum      = PINSEL_FUNC_2;
	pinCfg.OpenDrain    = PINSEL_PINMODE_NORMAL;
	pinCfg.Pinmode      = PINSEL_PINMODE_PULLUP;
	pinCfg.Pinnum       = GPIO_PIN_5;
	PINSEL_ConfigPin(&pinCfg);
	pinCfg.Pinnum       = GPIO_PIN_4;
	PINSEL_ConfigPin(&pinCfg);
#elif (BOARD_TYPE == SCU)
	//CAN1 pin init
    pinCfg.Portnum      = GPIO_PORT_0;
    pinCfg.Funcnum      = PINSEL_FUNC_1;  //first alternate function.
    pinCfg.OpenDrain    = PINSEL_PINMODE_NORMAL;
    pinCfg.Pinmode      = PINSEL_PINMODE_PULLUP;
    pinCfg.Pinnum       = GPIO_PIN_0;
    PINSEL_ConfigPin(&pinCfg);
    pinCfg.Pinnum       = GPIO_PIN_1;
    PINSEL_ConfigPin(&pinCfg);
    //CAN2 pin init
    pinCfg.Portnum      = GPIO_PORT_2;
    pinCfg.Pinnum       = GPIO_PIN_7;
    PINSEL_ConfigPin(&pinCfg);
    pinCfg.Pinnum       = GPIO_PIN_8;
    PINSEL_ConfigPin(&pinCfg);

#elif (BOARD_TYPE == ET_NXP)
    pinCfg.Portnum = GPIO_PORT_0;
    pinCfg.Funcnum = PINSEL_FUNC_1;  //first alternate function.
    pinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
    pinCfg.Pinmode = PINSEL_PINMODE_PULLUP;
    pinCfg.Pinnum = GPIO_PIN_0;
    PINSEL_ConfigPin(&pinCfg);
    pinCfg.Pinnum = GPIO_PIN_1;
    PINSEL_ConfigPin(&pinCfg);
#endif
}

/*************************************************************************
* function: initAdc    
* comments:   
**************************************************************************/
static void initAdc(void)
{
    PINSEL_CFG_Type pinCfg;
    pinCfg.Portnum = GPIO_PORT_0;
    pinCfg.Funcnum = PINSEL_FUNC_1;  //first alternate function.
    pinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
    pinCfg.Pinmode = PINSEL_PINMODE_PULLUP;

    pinCfg.Pinnum = GPIO_PIN_23;
    PINSEL_ConfigPin(&pinCfg);
    pinCfg.Pinnum = GPIO_PIN_24;
    PINSEL_ConfigPin(&pinCfg);
    pinCfg.Pinnum = GPIO_PIN_25;
    PINSEL_ConfigPin(&pinCfg);

    pinCfg.Portnum = GPIO_PORT_1;
    pinCfg.Funcnum = PINSEL_FUNC_3;
    pinCfg.Pinnum = GPIO_PIN_30;
    PINSEL_ConfigPin(&pinCfg);
    pinCfg.Pinnum = GPIO_PIN_31;
    PINSEL_ConfigPin(&pinCfg);

    ADC_Init(LPC_ADC, 200000);
    ADC_ChannelCmd(LPC_ADC,ADC_CHANNEL_2,ENABLE);
    ADC_ChannelCmd(LPC_ADC,ADC_CHANNEL_1,ENABLE);
    ADC_ChannelCmd(LPC_ADC,ADC_CHANNEL_0,ENABLE);
    ADC_ChannelCmd(LPC_ADC,ADC_CHANNEL_4,ENABLE);
    ADC_ChannelCmd(LPC_ADC,ADC_CHANNEL_5,ENABLE);
    //Start burst conversion
    ADC_BurstCmd(LPC_ADC,ENABLE);
}

/*************************************************************************
* function: initPwm    
* comments:   
**************************************************************************/
static void initPwm(void)
{
    PINSEL_CFG_Type pinCfg;
    pinCfg.Portnum = GPIO_PORT_2;
    pinCfg.Pinnum = GPIO_PIN_0;
    pinCfg.Funcnum = PINSEL_FUNC_1;  //first alternate function.
    pinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
    pinCfg.Pinmode = PINSEL_PINMODE_PULLUP;
    PINSEL_ConfigPin(&pinCfg);

    pinCfg.Portnum = GPIO_PORT_2;
    pinCfg.Pinnum = GPIO_PIN_2;
    pinCfg.Funcnum = PINSEL_FUNC_1;  //first alternate function.
    pinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
    pinCfg.Pinmode = PINSEL_PINMODE_PULLUP;
    PINSEL_ConfigPin(&pinCfg);
}

/*************************************************************************
* function: initI2c    
* comments:   
**************************************************************************/
static void initI2c(void)
{
    PINSEL_CFG_Type pinCfg;
    pinCfg.Portnum = GPIO_PORT_0;
    pinCfg.Pinnum = GPIO_PIN_19;
    pinCfg.Funcnum = PINSEL_FUNC_3;  //first alternate function.
    pinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
    pinCfg.Pinmode = PINSEL_PINMODE_PULLUP;
    PINSEL_ConfigPin(&pinCfg);
    pinCfg.Pinnum = GPIO_PIN_20;
    PINSEL_ConfigPin(&pinCfg);

    // Initialize Slave I2C peripheral
    I2C_Init(LPC_I2C1,I2C_FREQUENCY);
    /* Enable I2C operation */
    I2C_Cmd(LPC_I2C1, ENABLE);
}
/*************************************************************************
* function: initSPI  
* comments:   
**************************************************************************/
static void initSPI(void)
{
        PINSEL_CFG_Type pinCfg;
		/*
	 * Initialize SPI pin connect
	 * P0.15 - SCK;
	 * P0.17 - MISO
	 * P0.18 - MOSI
	 * P0.16 - SSEL - used as GPIO
	 */	
	pinCfg.Funcnum = PINSEL_FUNC_3;
	pinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
	pinCfg.Pinmode = PINSEL_PINMODE_PULLUP;
	pinCfg.Portnum = GPIO_PORT_0;

	//SPI MISO
	pinCfg.Pinnum = GPIO_PIN_17;
	PINSEL_ConfigPin(&pinCfg);
	//SPI MOSI
	pinCfg.Pinnum = GPIO_PIN_18;
	PINSEL_ConfigPin(&pinCfg);

#if (BOARD_TYPE == SCU)
    //SPI SCK
    pinCfg.Pinnum = GPIO_PIN_15;
    PINSEL_ConfigPin(&pinCfg);

	//SPI CS
	pinCfg.Funcnum = PINSEL_FUNC_0;
	pinCfg.Pinnum = GPIO_PIN_16;
	PINSEL_ConfigPin(&pinCfg);

	GPIO_SetDir(GPIO_PORT_0, (1<<GPIO_PIN_16), 1);
	GPIO_SetValue(GPIO_PORT_0, (1<<GPIO_PIN_16));
#endif
}
/****************************************************************************/
/**                                                                        **/
/**                               EOF                                      **/
/**                                                                        **/
/****************************************************************************/
