/*
                             *******************
******************************* C SOURCE FILE *******************************
**                           *******************                           **
**                                                                         **
** project   : sectionCTRLing controller LPC1768                                 **
** filename  : config.c                                                    **
**                                                                         **
*****************************************************************************
**                                                                         **
** Copyright (c) 2012, SBG Precision Farming/Navtronics Bvba               **
** All rights reserved.                                                    **
**                                                                         **
*****************************************************************************

VERSION HISTORY:
----------------

Version     : 1
Date        : 20121008
Revised by  : Koen Smeuninx 
Description : * Original version.

FILE INFORMATION:
-----------------
contains all the possible configuration paramters stored in flash and eeprom
*/

/****************************************************************************/
/**                                                                        **/
/**                     MODULES USED                                       **/
/**                                                                        **/
/****************************************************************************/

#include <stdio.h>
#include "config.h"
#include "eeprom_i2c.h"
//#include "uartservice.h"
#include "ibs_dl_section_ctrl.h"
#include "iap.h"
#include "crc.h"
#include "section_ctrl_task.h"
#include "log.h"

/****************************************************************************/
/**                                                                        **/
/**                     DEFINITIONS AND MACROS                             **/
/**                                                                        **/
/****************************************************************************/
#define CHECK_PARAM_TYPE(x,y) if(x != y) break

//checksum location at the end of the memory
#define CFG_CHECKSUM_ADDRESS (EE_I2C_MEMSIZE - 2)

#define SOURCE_ADDRESS_DEFAULT      0x13
/****************************************************************************/
/**                                                                        **/
/**                     TYPEDEFS AND STRUCTURES                            **/
/**                                                                        **/
/****************************************************************************/

/****************************************************************************/
/**                                                                        **/
/**                     PROTOTYPES OF LOCAL FUNCTIONS                      **/
/**                                                                        **/
/****************************************************************************/
static void cfgSaveSectionCtrlConfig(void);
static void restoreBoomCfgFromFlash(void);
static void resetToDefaultBoomConfig(void);
/****************************************************************************/
/**                                                                        **/
/**                     EXPORTED VARIABLES                                 **/
/**                                                                        **/
/****************************************************************************/

Cfg_t Cfg;


/****************************************************************************/
/**                                                                        **/
/**                     GLOBAL VARIABLES                                   **/
/**                                                                        **/
/****************************************************************************/
static BoomConfig_t         ExpectedType;
static SectionIdentifier_t  ExpectedSectionId;
/****************************************************************************/
/**                                                                        **/
/**                     EXPORTED FUNCTIONS                                 **/
/**                                                                        **/
/****************************************************************************/

/*************************************************************************
* function:     InitCfg  
* comments:     Initializes the configuraton, tries to restore settings
*               from eeprom. if restore failed, all parameters in the 
*               configuration will be reset to default.
**************************************************************************/
void InitCfg(void)
{
    // Try to restore configuration
    if (CfgRestore() == FALSE)
    {
        LogWriteString("flash Configuration changed!");
        //check if its an empty eeprom
    }
    if(Cfg.cfgFlash.dynamicCfgFlash.ecuSoftwareVersion != CURRENT_ECU_SOFTWARE_VERSION)
    {
        Cfg.cfgFlash.dynamicCfgFlash.ecuSoftwareVersion = CURRENT_ECU_SOFTWARE_VERSION;
        CfgSave(CFG_MEMORY_DYNAMIC_FLASH); // Save default configuration to EEPROM
    }
    ExpectedType = BOOM_CONFIG_1;
    ExpectedSectionId = SECTION_ID_MIN;   
}

/*************************************************************************
* function:     CfgRestore  
* comments:     
**************************************************************************/
Bool_t CfgRestore (void)
{
//Copy the data into the buffer structs.
    StaticCfgFlash_t const* const staticFlashConfig = ((StaticCfgFlash_t*) STATIC_FLASHCONFIG_START);
    DynamicCfgFlash_t const* const dynamicFlashConfig = ((DynamicCfgFlash_t*) DYNAMIC_FLASHCONFIG_START);
    
    restoreBoomCfgFromFlash();

    memcpy(&Cfg.cfgFlash.staticCfgFlash, staticFlashConfig, sizeof(StaticCfgFlash_t));
    memcpy(&Cfg.cfgFlash.dynamicCfgFlash, dynamicFlashConfig, sizeof(DynamicCfgFlash_t));
    if (Cfg.cfgFlash.dynamicCfgFlash.checkWord != VALIDPARAM_CHECKWORD)
    {
        return FALSE;
    }

    return TRUE;
}

/*************************************************************************
* function:     CfgSave  
* comments:     
**************************************************************************/
void CfgSave (CfgMemoryType_t cfgMemoryType )
{
    switch (cfgMemoryType)
    {
        case CFG_MEMORY_DYNAMIC_FLASH:
            CfgSaveFlash(DYNAMIC_FLASHCONFIG_START);
            break;
        case CFG_MEMORY_STATIC_FLASH:
            //Cannot save this.  Only bL 
        case CFG_MEMORY_SECTION_CTRL_FLASH:
            cfgSaveSectionCtrlConfig();
            break;
    }
}


/*************************************************************************
* function:     CfgReportParameter  
* comments:     
**************************************************************************/
void CfgReportParameter (UI08_t cId, UI08_t pId, UI16_t paramId)
{
    UI08_t                i;
    IbsConfigurationMsg_t msg;

    // Invalidate contents
    for (i=0; i<4; i++) 
    {
        msg.data[i] = 0;
    }
  
    msg.paramId   = paramId;
    msg.opMode    = CFG_REP_PARAM;
    msg.contrId   = cId;
    msg.partnerId = pId;
    msg.paramType = CFG_PARAM_DWORD;  //default DWORDS

    switch (paramId) 
    {
        case CFG_BL_HW_VERSION:
            memcpy(msg.data,&Cfg.cfgFlash.staticCfgFlash.hwVersion,4);
            break;
        case CFG_BL_SW_VERSION:
            memcpy(msg.data, &Cfg.cfgFlash.dynamicCfgFlash.bootLoaderSoftwareVersion,  4);
            break;
        case CFG_BL_ECU_SW_VERSION:
            memcpy(msg.data, &Cfg.cfgFlash.dynamicCfgFlash.ecuSoftwareVersion,  4);
            break;
        case CFG_BL_SERIAL_NR:
            memcpy(msg.data, &Cfg.cfgFlash.staticCfgFlash.serialNr,   4);
            break;
        case CFG_BL_CAN_MANUFACTURER:
            memcpy(msg.data, &Cfg.cfgFlash.staticCfgFlash.manufacturer,  4);
            break;
        case CFG_BL_CAN_FUNCTION:
            memcpy(msg.data, &Cfg.cfgFlash.staticCfgFlash.function,  4);
            break;
        case CFG_BL_CAN_DEVICE_CLASS:
            memcpy(msg.data, &Cfg.cfgFlash.staticCfgFlash.deviceClass,  4);
            break;
        case CFG_BL_CAN_INDUSTRY_GROUP:
            memcpy(msg.data, &Cfg.cfgFlash.staticCfgFlash.industryGroup,  4);
            break;
        case CFG_BL_ECU_TYPE:
            memcpy(msg.data, &Cfg.cfgFlash.staticCfgFlash.ecuType,  4);
            break;
        case CFG_BL_FLASH_STATE:
            memcpy(msg.data, &Cfg.cfgFlash.dynamicCfgFlash.flashState,  4);
            break;
        case CFG_BL_SELFCONFIGURABLE_ADDRESS:
            memcpy(msg.data, &Cfg.cfgFlash.staticCfgFlash.selfConfigurableAddress,  4);
            break;
        case CFG_BL_ECU_STARTADDRESS:
            memcpy(msg.data, &Cfg.cfgFlash.staticCfgFlash.startAddressEcu,  4);
            break;
        case CFG_BL_SA_CLAIM_START:
            memcpy(msg.data, &Cfg.cfgFlash.staticCfgFlash.sourceAddressClaimStart,4);
            break;
        default:
            msg.paramId = 0xFFFF; // invalid parameter id
            break;
  }

  IbsDlSendConfiguration(msg);
  //wait till buffer empty, the checktxbuf returns false if empty buffer
  while(CanCheckTxBuf(0))
  {

  };
}

/*************************************************************************
* function:     CfgModifyParameterEx 
* comments:     this function uses the earlier defined CHECK_PARAM_TYPE macro
**************************************************************************/
void CfgModifyParameterEx (UI16_t paramId, UI08_t paramType, UI08_t* data)
{
    switch (paramId) 
    {        
        
        default:
            return;

    }


}

/*************************************************************************
* function: CfgModifyParameter   
* comments: Apart from modifying this function also reports the parameter
*           when modifying is done.
**************************************************************************/
void CfgModifyParameter (UI08_t cId, UI08_t pId, UI16_t paramId, 
                         UI08_t paramType, UI08_t* data)
{
    CfgModifyParameterEx(paramId,paramType,data);

    CfgReportParameter(cId, pId, paramId);
}

/*************************************************************************
* function: CfgEraseAndWriteStaticParams         
* comments: No impelementation in this project to protect the static
*           parameter from beeing erased.  Only the bootloader should be
*           able to do this.
**************************************************************************/
void CfgEraseAndWriteStaticParams()
{
    //CfgSaveFlash(LPC1768_STATIC_FLASHCONFIG_START);
}

/*************************************************************************
* function: CfgReportNumParameters    
* comments: Obsolete, the number of parameters is no longer needed
*           by the partner. 
**************************************************************************/
void CfgReportNumParameters (UI08_t cId, UI08_t pId, UI08_t numParams)
{
    //not needed anymore return 0
  IbsConfigurationMsg_t msg;

  msg.opMode    = CFG_REP_NR_PARAM;
  msg.paramId   = 0;
  msg.partnerId = pId;
  msg.contrId   = cId;

  IbsDlSendConfiguration(msg);
}


/*****************************************************************************
* function: ReportAllParameters
* comments: This function sends all the parameters of this FW release
*           When the request all is received.  
*           When parameter changes or parameters are added/removed
*           change this function.
****************************************************************************/
void ReportAllParameters(UI08_t cId, UI08_t pId)
{
    CfgReportParameter(cId, pId,    CFG_BL_HW_VERSION                   );
    CfgReportParameter(cId, pId,    CFG_BL_SW_VERSION                   );
    CfgReportParameter(cId, pId,    CFG_BL_ECU_SW_VERSION               );
}

/*****************************************************************************
* function: SetGroupToDefault
* comments:                          
****************************************************************************/
void CfgSetGroupToDefault(ParameterGroup_t ParameterGroup)
{

         Cfg.cfgFlash.dynamicCfgFlash.lastSA = SOURCE_ADDRESS_DEFAULT;

}

/*************************************************************************
* function:  ReportBoomConfigurationI  
* comments:  Reports the boom configuration I data.  
**************************************************************************/
void CfgReportBoomConfigurationI(UI08_t cId, UI08_t pId)
{
    CanFrame_t frm;
    frm.id = PGN_PROPRIETARY_A_DEST;
    frm.dlc = 8;
    frm.bytes[0] = CBTX_BOOM_CONFIGURATION_I;
    memcpy(&frm.bytes[1], &Cfg.cfgFlash.sectionCtrlCfg.boomConfigurationI , sizeof(BoomConfigurationI_t));
    // Send message to partner
    IbsNmSendPartnerFrame(cId, pId, &frm);
}
/*************************************************************************
* function:  ReportBoomConfigurationII  
* comments:  Reports the boom configuration II data.  
**************************************************************************/
void CfgReportBoomConfigurationII(UI08_t cId, UI08_t pId)
{
    CanFrame_t frm;
    frm.id = PGN_PROPRIETARY_A_DEST;
    frm.dlc = 8;
    frm.bytes[0] = CBTX_BOOM_CONFIGURATION_II;
    memcpy(&frm.bytes[1], &Cfg.cfgFlash.sectionCtrlCfg.boomConfigurationII , sizeof(BoomConfigurationII_t));
    // Send message to partner
    IbsNmSendPartnerFrame(cId, pId, &frm);
}
/*************************************************************************
* function:  ReportBoomConfigurationIII  
* comments:  Reports the boom configuration I data.  
**************************************************************************/
void CfgReportBoomConfigurationIII(UI08_t cId, UI08_t pId)
{
    CanFrame_t frm;
    frm.id = PGN_PROPRIETARY_A_DEST;
    frm.dlc = 8;
    frm.bytes[0] = CBTX_BOOM_CONFIGURATION_III;
    memcpy(&frm.bytes[1], &Cfg.cfgFlash.sectionCtrlCfg.boomConfigurationIII , sizeof(BoomConfigurationIII_t));
    // Send message to partner
    IbsNmSendPartnerFrame(cId, pId, &frm);

    
}
/*************************************************************************
* function:  ReportSectionConfiguration  
* comments:  Reports the Section configuration.  
**************************************************************************/
void CfgReportSectionConfiguration(UI08_t cId, UI08_t pId,SectionIdentifier_t sectionId )
{
    CanFrame_t frm;
    if (  (sectionId < SECTION_ID_MIN) || 
          (sectionId > SECTION_ID_MAX) ||
          (sectionId > MAX_NUMBER_OF_SECTIONS) )
    {
        return;
    }
    frm.id = PGN_PROPRIETARY_A_DEST;
    frm.dlc = 8;
    frm.bytes[0] = CBTX_SECTION_CONFIGURATION;
    memcpy(&frm.bytes[1], &Cfg.cfgFlash.sectionCtrlCfg.sectionConfiguration[sectionId-1] , sizeof(SectionConfiguration_t));
    // Send message to partner
    IbsNmSendPartnerFrame(cId, pId, &frm);
}

/*************************************************************************
* function:     CfgHandleBoomConfiguration 
* comments:     Handler which is called when the sprayer controller 
*               sends a new configuration
**************************************************************************/
void CfgHandleBoomConfiguration(BoomConfig_t boomConfigType,BoomConfigMessageData_t *messageDataPtr)
{
    SectionIdentifier_t receivedSectionId;
    if (boomConfigType != ExpectedType)
    {
        ExpectedType = BOOM_CONFIG_1;
        ExpectedSectionId = SECTION_ID_MIN;
        restoreBoomCfgFromFlash();
        return;
    }

    switch (boomConfigType)
    {
        case BOOM_CONFIG_1:
            Cfg.cfgFlash.sectionCtrlCfg.boomConfigurationI = messageDataPtr->boomConfigI;
            ExpectedType = BOOM_CONFIG_2;
            break;
        case BOOM_CONFIG_2:
            Cfg.cfgFlash.sectionCtrlCfg.boomConfigurationII = messageDataPtr->boomConfigII;
            ExpectedType = BOOM_CONFIG_3;
            break;
        case BOOM_CONFIG_3:
            Cfg.cfgFlash.sectionCtrlCfg.boomConfigurationIII = messageDataPtr->boomConfigIII;
            ExpectedType = SECTION_CONFIG;
            ExpectedSectionId = SECTION_ID_MIN;
            break;
        case SECTION_CONFIG:
            receivedSectionId =  messageDataPtr->sectionConfig.sectionIdentifier;
            if (ExpectedSectionId != receivedSectionId)
            {
                ExpectedType = BOOM_CONFIG_1;
                ExpectedSectionId = SECTION_ID_MIN;
                restoreBoomCfgFromFlash();
                return;
            }
            else
            {
                Cfg.cfgFlash.sectionCtrlCfg.sectionConfiguration[receivedSectionId-1] = messageDataPtr->sectionConfig;
                ExpectedSectionId++;
                if (Cfg.cfgFlash.sectionCtrlCfg.boomConfigurationI.numberOfSections == (NumberOfSections_t)receivedSectionId)
                {                    
                    //all config data received, store and send back.
                    CfgSave(CFG_MEMORY_SECTION_CTRL_FLASH);
                    restoreBoomCfgFromFlash();  //read it back to make sure all the data is stored. 
                    IbsDlSendBoomConfiguration(TRUE);
                    ExpectedType = BOOM_CONFIG_1;
                    ExpectedSectionId = SECTION_ID_MIN;
                }
            }
    }
}



/****************************************************************************/
/**                                                                        **/
/**                     LOCAL FUNCTIONS                                    **/
/**                                                                        **/
/****************************************************************************/

static void restoreBoomCfgFromFlash(void)
{
    SectionCtrlConfig_t const* const sectionCtrlConfig = ((SectionCtrlConfig_t*) EXTENDED_CONFIG_START);
    memcpy(&Cfg.cfgFlash.sectionCtrlCfg, sectionCtrlConfig, sizeof(SectionCtrlConfig_t));
    if (Cfg.cfgFlash.sectionCtrlCfg.checkWord != VALIDPARAM_CHECKWORD)
    {
        resetToDefaultBoomConfig();
    }

}


/*************************************************************************
* function:     resetToDefaultBoomConfig 
* comments:     When no configuration is available yet, the default
*               boom configuration should be loaded.  
**************************************************************************/
static void resetToDefaultBoomConfig(void)
{
    UI08_t sectionIndex;

    Cfg.cfgFlash.sectionCtrlCfg.boomConfigurationI.configurationExchangeKey = 0x01;
    Cfg.cfgFlash.sectionCtrlCfg.boomConfigurationI.drpXOffset = 300;  //=1.5/0.005
    Cfg.cfgFlash.sectionCtrlCfg.boomConfigurationI.drpYOffset = 0;
    Cfg.cfgFlash.sectionCtrlCfg.boomConfigurationI.numberOfSections = 7;
    Cfg.cfgFlash.sectionCtrlCfg.boomConfigurationI.reserved = 0;

    Cfg.cfgFlash.sectionCtrlCfg.boomConfigurationII.brpXOffset = 0;
    Cfg.cfgFlash.sectionCtrlCfg.boomConfigurationII.brpYOffset = 0;
    Cfg.cfgFlash.sectionCtrlCfg.boomConfigurationII.implementType = IT_REAR_MOUNTED;
    Cfg.cfgFlash.sectionCtrlCfg.boomConfigurationII.steeringType = ST_NONE;
    Cfg.cfgFlash.sectionCtrlCfg.boomConfigurationII.drawBarSteeringOffsetValue = 0;
       

      
    Cfg.cfgFlash.sectionCtrlCfg.boomConfigurationIII.endSectionBitFlags.section1IsEnd = 0;
    Cfg.cfgFlash.sectionCtrlCfg.boomConfigurationIII.endSectionBitFlags.section2IsEnd = 0;
    Cfg.cfgFlash.sectionCtrlCfg.boomConfigurationIII.endSectionBitFlags.section3IsEnd = 0;
    Cfg.cfgFlash.sectionCtrlCfg.boomConfigurationIII.endSectionBitFlags.section4IsEnd = 0;
    Cfg.cfgFlash.sectionCtrlCfg.boomConfigurationIII.endSectionBitFlags.section5IsEnd = 0;
    Cfg.cfgFlash.sectionCtrlCfg.boomConfigurationIII.endSectionBitFlags.section6IsEnd = 0;
    Cfg.cfgFlash.sectionCtrlCfg.boomConfigurationIII.endSectionBitFlags.section7IsEnd = 0;
    Cfg.cfgFlash.sectionCtrlCfg.boomConfigurationIII.endSectionBitFlags.section8IsEnd = 0;
    Cfg.cfgFlash.sectionCtrlCfg.boomConfigurationIII.endSectionBitFlags.section9IsEnd = 0;
    Cfg.cfgFlash.sectionCtrlCfg.boomConfigurationIII.endSectionBitFlags.section10IsEnd = 0;
    Cfg.cfgFlash.sectionCtrlCfg.boomConfigurationIII.endSectionBitFlags.section11IsEnd = 0;
    Cfg.cfgFlash.sectionCtrlCfg.boomConfigurationIII.endSectionBitFlags.section12IsEnd = 0;
    Cfg.cfgFlash.sectionCtrlCfg.boomConfigurationIII.endSectionBitFlags.section13IsEnd = 0;
    Cfg.cfgFlash.sectionCtrlCfg.boomConfigurationIII.endSectionBitFlags.section14IsEnd = 0;
    Cfg.cfgFlash.sectionCtrlCfg.boomConfigurationIII.endSectionBitFlags.section15IsEnd = 0;
    Cfg.cfgFlash.sectionCtrlCfg.boomConfigurationIII.endSectionBitFlags.section16IsEnd = 0;
    Cfg.cfgFlash.sectionCtrlCfg.boomConfigurationIII.endSectionOffsetValue = 0;
    
    Cfg.cfgFlash.sectionCtrlCfg.boomConfigurationIII.options.hasFreewheelValve      = CONFIG_OPTION_NOT_AVAILABLE;
    Cfg.cfgFlash.sectionCtrlCfg.boomConfigurationIII.options.hasPumpControl         = CONFIG_OPTION_NOT_AVAILABLE;
    Cfg.cfgFlash.sectionCtrlCfg.boomConfigurationIII.options.driverTypeMotor        = CONFIG_OPTION_NOT_AVAILABLE;
    Cfg.cfgFlash.sectionCtrlCfg.boomConfigurationIII.options.driverValveInverted    = CONFIG_OPTION_NOT_AVAILABLE;
    Cfg.cfgFlash.sectionCtrlCfg.boomConfigurationIII.options.reserved = 0       ;  
    Cfg.cfgFlash.sectionCtrlCfg.boomConfigurationIII.protocol = 0;
    Cfg.cfgFlash.sectionCtrlCfg.boomConfigurationIII.configurationId = 0;
    
    Cfg.cfgFlash.sectionCtrlCfg.boomConfigurationIII.reserved  = FALSE;
    
    for (   sectionIndex = 0; 
            sectionIndex < Cfg.cfgFlash.sectionCtrlCfg.boomConfigurationI.numberOfSections; 
            sectionIndex++)
    {
        Cfg.cfgFlash.sectionCtrlCfg.sectionConfiguration[sectionIndex].sectionIdentifier = sectionIndex + 1;
        Cfg.cfgFlash.sectionCtrlCfg.sectionConfiguration[sectionIndex].sectionWidth =  600;//3 / 0.005;
        Cfg.cfgFlash.sectionCtrlCfg.sectionConfiguration[sectionIndex].derXOffset = 0;
        Cfg.cfgFlash.sectionCtrlCfg.sectionConfiguration[sectionIndex].derYOffset = -1800 + 600* sectionIndex; //-9 / 0.005 + (3/0.005)*index
    }
}

/*************************************************************************
* function: cfgSaveSectionCtrlConfig      
* comments: Saves the section control data to the flash memory at a 
*           separate sector.  
**************************************************************************/
static void cfgSaveSectionCtrlConfig(void)
{
    static UI08_t tmpBytes[(UI16_t)IAP_SIZE_512]; //size 256 doesn't work!!!
    UI32_t endAddress;

    Cfg.cfgFlash.sectionCtrlCfg.checkWord = VALIDPARAM_CHECKWORD;

    memcpy(&tmpBytes, &Cfg.cfgFlash.sectionCtrlCfg, sizeof(Cfg.cfgFlash.sectionCtrlCfg));
    memset(&tmpBytes[sizeof(Cfg.cfgFlash.sectionCtrlCfg)], 0, IAP_SIZE_512-sizeof(Cfg.cfgFlash.sectionCtrlCfg));
    endAddress = EXTENDED_CONFIG_END;    
    // Disable all interrupts when writing

    OS_DisableInt();
    if (IapErase(EXTENDED_CONFIG_START, endAddress) != IAP_CMD_SUCCESS)
    {
        return;
    }
    if (IapWrite(EXTENDED_CONFIG_START, ((UI32_t)&tmpBytes), IAP_SIZE_512) != IAP_CMD_SUCCESS)
    {
        return; 
    }
    OS_EnableInt();   
    return;
}

/*************************************************************************
* function:     CfgGetModuleCfg  
* parameters:   ModuleConfiguration_t *moduleCfgPtr: ptr to the config stuct to fill
* returns:      void
* comments:     translates config data in moduleCfg and stores it into the passed pointer
**************************************************************************/
void CfgGetModuleCfg(ModuleConfiguration_t *moduleCfgPtr)
{
    moduleCfgPtr->auxActType = AUX_ACT_NOT_AVAILABLE;
    moduleCfgPtr->numSections = Cfg.cfgFlash.sectionCtrlCfg.boomConfigurationI.numberOfSections;
    if (Cfg.cfgFlash.sectionCtrlCfg.boomConfigurationIII.protocol == PROTOCOL_NONE)
    {
        moduleCfgPtr->moduleType = MODULE_OUTPUTS;

        if (Cfg.cfgFlash.sectionCtrlCfg.boomConfigurationIII.options.hasFreewheelValve == 
                CONFIG_OPTION_AVAILABLE)
        {
            moduleCfgPtr->auxActType = FREE_WHEEL_VALVE;
        }

	    if (Cfg.cfgFlash.sectionCtrlCfg.boomConfigurationIII.options.hasPumpControl == 
	            CONFIG_OPTION_AVAILABLE)
	    {
	        if (moduleCfgPtr->auxActType == AUX_ACT_NOT_AVAILABLE)
	        {
	            moduleCfgPtr->auxActType = PUMP_CONTROL;
	        }
	        else
	        {
	            //conflicting settings, disable feature...
	            moduleCfgPtr->auxActType = AUX_ACT_NOT_AVAILABLE;
	        }
	    }
   
    if (Cfg.cfgFlash.sectionCtrlCfg.boomConfigurationIII.options.driverTypeMotor == 
            CONFIG_OPTION_AVAILABLE)
    {
        moduleCfgPtr->driverType = DRIVER_MOTOR;
    }
    else
    {
        moduleCfgPtr->driverType = DRIVER_SOLENOID;
    }

        if (Cfg.cfgFlash.sectionCtrlCfg.boomConfigurationIII.options.driverValveInverted == 
                CONFIG_OPTION_AVAILABLE)
        {
            moduleCfgPtr->sectionLogicLevel = ACT_LOGIC_LOW;
            moduleCfgPtr->auxActLogicLevel  = ACT_LOGIC_LOW;
        }
        else
        {
            moduleCfgPtr->sectionLogicLevel = ACT_LOGIC_HIGH;
            moduleCfgPtr->auxActLogicLevel  = ACT_LOGIC_HIGH;
        }
        moduleCfgPtr->numSections = Cfg.cfgFlash.sectionCtrlCfg.boomConfigurationI.numberOfSections;
    }
    else
    {
        moduleCfgPtr->moduleType = MODULE_BRIDGE;
        moduleCfgPtr->protocolType = Cfg.cfgFlash.sectionCtrlCfg.boomConfigurationIII.protocol;
    }
}





/****************************************************************************/
/**                                                                        **/
/**                              EOF                                       **/
/**                                                                        **/
/****************************************************************************/
