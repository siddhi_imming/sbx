/*
                             *******************
******************************* C HEADER FILE *******************************
**                           *******************                           **
**                                                                         **
** project   : sectionCTRLing Controller LPC17xx                                 **
** filename  : hwplatform.h                                                **  
**                                                                         **
*****************************************************************************
**                                                                         **
** Copyright (c) 2012, SBG Precision Farming/Navtronics Bvba               **
** All rights reserved.                                                    **
**                                                                         **
*****************************************************************************

VERSION HISTORY:
----------------

Version     : 1
Date        : 20120919
Revised by  : Koen Smeuninx
Description : * Original version.

FILE INFORMATION:
-----------------

this unit contains all the functions specific for hardware initialization.

*/

#ifndef _HWPLATFORM_INCLUDED
#define _HWPLATFORM_INCLUDED

/****************************************************************************/
/**                                                                        **/
/**                     MODULES USED                                       **/
/**                                                                        **/
/****************************************************************************/

#include "stddefs.h"

/****************************************************************************/
/**                                                                        **/
/**                     DEFINITIONS AND MACROS                             **/
/**                                                                        **/
/****************************************************************************/
typedef enum InputPinMode_e
{
    INPUT_MODE_0_10V,
    INPUT_MODE_0_5V,
    INPUT_MODE_4_20mA
    
}InputPinMode_t;

typedef enum InputPinId_e
{
    INPUT1  =   1,
    INPUT2  =   2,
    INPUT3  =   3,
    INPUT4  =   4
}InputPinId_t;

/****************************************************************************/
/**                                                                        **/
/**                     TYPEDEFS AND STRUCTURES                            **/
/**                                                                        **/
/****************************************************************************/

/****************************************************************************/
/**                                                                        **/
/**                     EXPORTED VARIABLES                                 **/
/**                                                                        **/
/****************************************************************************/


/****************************************************************************/
/**                                                                        **/
/**                     EXPORTED FUNCTIONS                                 **/
/**                                                                        **/
/****************************************************************************/
void InitHW(void);


/*************************************************************************
* function:     Hardware cleanup function in case of an error.   
* comments:     In case of an exception or and OS error this function 
*               gets called, it makes sure all the outputs are disabled.
**************************************************************************/
void HwShutdown(void); 

#endif

/****************************************************************************/
/**                                                                        **/
/**                               EOF                                      **/
/**                                                                        **/
/****************************************************************************/
