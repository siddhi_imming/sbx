/*
                             *******************
******************************* C SOURCE FILE *******************************
**                           *******************                           **
**                                                                         **
** project   : sectionCTRLing controller 1768                              **
** filename  : main.c                                                      **
**                                                                         **
*****************************************************************************
**                                                                         **
** Copyright (c) 2012, SBG Precision Farming/Navtronics Bvba               **
** All rights reserved.                                                    **
**                                                                         **
*****************************************************************************
VERSION HISTORY:
----------------

Version     : 1
Date        : 20120925
Author      : Koen Smeuninx
Description : * Original version

FILE INFORMATION:
-----------------
main unit where all the task are initialized.  
*/

#include <intrinsics.h>
#include <stdio.h>
#include <assert.h>

#include "RTOS.h"
#include "hw_platform.h"
#include "tasks.h"
#include "led_task.h"
#include "ibs_task.h"
#include "uart_task.h"
#include "uart_service.h"
//#include "switch_box_service.h"
#include "section_ctrl_task.h"
#include "config.h"
#include "log.h"

#include "lpc17xx_adc.h"
#include "lpc17xx_pinsel.h"
#include "lpc17xx_gpio.h"

/****************************************************************************/
/**                                                                        **/
/**                     DEFINITIONS AND MACROS                             **/
/**                                                                        **/
/****************************************************************************/

/****************************************************************************/
/**                                                                        **/
/**                     TYPEDEFS AND STRUCTURES                            **/
/**                                                                        **/
/****************************************************************************/

/****************************************************************************/
/**                                                                        **/
/**                     PROTOTYPES OF LOCAL FUNCTIONS                      **/
/**                                                                        **/
/****************************************************************************/

/****************************************************************************/
/**                                                                        **/
/**                     EXPORTED VARIABLES                                 **/
/**                                                                        **/
/****************************************************************************/


/****************************************************************************/
/**                                                                        **/
/**                     GLOBAL VARIABLES                                   **/
/**                                                                        **/
/****************************************************************************/

/****************************************************************************/
/**                                                                        **/
/**                     EXPORTED FUNCTIONS                                 **/
/**                                                                        **/
/****************************************************************************/

/*************************************************************************
* Function: main
* Description: main entry point of the software.
*************************************************************************/
int main(void)
{
    // Initialize hardware
    OS_IncDI();                      /* Initially disable interrupts  */
    OS_InitKern();                   /* Initialize OS                 */
    OS_InitHW();                     /* Initialize Hardware for OS    */
    //The init hardware is also calling the init function of the cmsis!    
    InitCfg();
    InitHW();
    InitUartTask();
    InitLog(LOG_OUTPUT_FIRST_UART);

    InitLedTask();
    InitIsoBusTask();
    
    InitSectionCtrlTask();

    OS_Start();
}

