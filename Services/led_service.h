/*
                             *******************
******************************* C HEADER FILE *******************************
**                           *******************                           **
**                                                                         **
** project   : Steering Controller LPC17xx                                 **
** filename  : ledservice.h                                                **
**                                                                         **
*****************************************************************************
**                                                                         **
** Copyright (c) 2012, SBG Precision Farming/Navtronics Bvba               **
** All rights reserved.                                                    **
**                                                                         **
*****************************************************************************

VERSION HISTORY:
----------------

Version     : 1
Date        : 20120919
Revised by  : Koen Smeuninx
Description : * Original version.

FILE INFORMATION:
-----------------

This file services the led task.  

*/

#ifndef _LEDSERVICE_INCLUDED
#define _LEDSERVICE_INCLUDED

/****************************************************************************/
/**                                                                        **/
/**                     MODULES USED                                       **/
/**                                                                        **/
/****************************************************************************/

//#include "sysconf.h"

/****************************************************************************/
/**                                                                        **/
/**                     DEFINITIONS AND MACROS                             **/
/**                                                                        **/
/****************************************************************************/
#define NUMBER_OF_LEDS 8

/****************************************************************************/
/**                                                                        **/
/**                     TYPEDEFS AND STRUCTURES                            **/
/**                                                                        **/
/****************************************************************************/
typedef enum LedStatus_e
{
    LED_STATUS_OFF,
    LED_STATUS_ON,
    LED_STATUS_TOGGLE
}LedStatus_t;

typedef struct LedCommand_s
{
    UI08_t      gpioID;
    LedStatus_t ledStatus;
}LedCommand_t;
/****************************************************************************/
/**                                                                        **/
/**                     EXPORTED VARIABLES                                 **/
/**                                                                        **/
/****************************************************************************/
extern OS_Q LedServiceQueue;


/****************************************************************************/
/**                                                                        **/
/**                     EXPORTED FUNCTIONS                                 **/
/**                                                                        **/
/****************************************************************************/
/*************************************************************************
* function:     HandleLedCommand 
* parameters:   LedCommand_t *ledCmdPtr: structure containing id and 
*                   requested state change of the led. 
* returns:      void    
* comments:     Handler of a led command.
**************************************************************************/
void HandleLedCommand(LedCommand_t *ledCmdPtr);

#endif

/****************************************************************************/
/**                                                                        **/
/**                               EOF                                      **/
/**                                                                        **/
/****************************************************************************/
