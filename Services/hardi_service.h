/*
                             *******************
******************************* C HEADER FILE *******************************
**                           *******************                           **
**                                                                         **
** project   : Steering Controller LPC17xx                                 **
** filename  : uartservice.h                                                **
**                                                                         **
*****************************************************************************
**                                                                         **
** Copyright (c) 2012, SBG Precision Farming/Navtronics Bvba               **
** All rights reserved.                                                    **
**                                                                         **
*****************************************************************************

VERSION HISTORY:
----------------

Version     : 1
Date        : 20120919
Revised by  : Koen Smeuninx
Description : * Original version.

file information:
----------------

This file services the uart task.  

*/

#ifndef _UARTSERVICE_INCLUDED
#define _UARTSERVICE_INCLUDED

/****************************************************************************/
/**                                                                        **/
/**                     MODULES USED                                       **/
/**                                                                        **/
/****************************************************************************/

#include "sysconf.h"
#include "lpc17xx_uart.h"

/****************************************************************************/
/**                                                                        **/
/**                     DEFINITIONS AND MACROS                             **/
/**                                                                        **/
/****************************************************************************/
#define NUMBER_OF_UARTS 2

/****************************************************************************/
/**                                                                        **/
/**                     TYPEDEFS AND STRUCTURES                            **/
/**                                                                        **/
/****************************************************************************/
typedef struct UartCommand_s
{
    LPC_UART_TypeDef *uartIdPtr;
    UI08_t  dataSize;
    UI08_t  data[UART_BUFFER_SIZE];
}UartCommand_t;

/****************************************************************************/
/**                                                                        **/
/**                     EXPORTED VARIABLES                                 **/
/**                                                                        **/
/****************************************************************************/
extern OS_Q UartServiceQueue;

/****************************************************************************/
/**                                                                        **/
/**                     EXPORTED FUNCTIONS                                 **/
/**                                                                        **/
/****************************************************************************/

/*************************************************************************
* function:     UartWriteBytes 
* parameters:   UI08_t uartId: external ID of the uart, 0,1,2, 
*               UI08_t *dataPtr: pointer to the data to send.  
*               UI08_t dataSize: number of bytes in the dataPtr
* returns:      void 
* comments:     Call this function if you want to send data to one of the uarts
**************************************************************************/
void UartWriteBytes(UI08_t uartId, const UI08_t *dataPtr, UI08_t dataSize);

/*************************************************************************
* function:     UartWriteString 
* parameters:   UI08_t uartId: external ID of the uart, 0,1,2, 
*               char *messagePtr: pointer to the data to send.
* returns:      void 
* comments:     Call this function if you want to send a string to one of the uarts
**************************************************************************/
void UartWriteString(UI08_t uartId, char *messagePtr);

#endif

/****************************************************************************/
/**                                                                        **/
/**                               EOF                                      **/
/**                                                                        **/
/****************************************************************************/
