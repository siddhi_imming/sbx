/*
                             *******************
******************************* C HEADER FILE *******************************
**                           *******************                           **
**                                                                         **
** project   : Switch Box LPC17xx                                          **
** filename  : switch_box_service.h                                        **
**                                                                         **
*****************************************************************************
**                                                                         **
** Copyright (c) 2012, SBG Precision Farming/Navtronics Bvba               **
** All rights reserved.                                                    **
**                                                                         **
*****************************************************************************

VERSION HISTORY:
----------------
Version     : 1
Date        : 20130610
Revised by  : Siddhi Imming
Description : * Original version.


FILE INFORMATION:
-----------------

Service unit for the switch box task. 
*/
#ifndef _SWITCH_BOX_SERVICE_INCLUDED
#define _SWITCH_BOX_SERVICE_INCLUDED

/****************************************************************************/
/**                                                                        **/
/**                     MODULES USED                                       **/
/**                                                                        **/
/****************************************************************************/

#include "sysconf.h"
#include "section_ctrl_defs.h"


/****************************************************************************/
/**                                                                        **/
/**                     DEFINITIONS AND MACROS                             **/
/**                                                                        **/
/****************************************************************************/

#define SWB_TIMER_TIMEOUT_MS    50
#define SWB_SEND_CAN_MSG_MS   1000

/****************************************************************************/
/**                                                                        **/
/**                     TYPEDEFS AND STRUCTURES                            **/
/**                                                                        **/
/****************************************************************************/

typedef struct GpioPinAddress_s
{
    UI08_t PORT_NUM;
    UI08_t PIN_NUM;
} GpioPinAddress_t;

/****************************************************************************/
/**                                                                        **/
/**                     EXPORTED VARIABLES                                 **/
/**                                                                        **/
/****************************************************************************/

/****************************************************************************/
/**                                                                        **/
/**                     EXPORTED FUNCTIONS                                 **/
/**                                                                        **/
/****************************************************************************/

/*****************************************************************************
*   function :      ServiceSwitchBoxInit
*   parameters:     void
*   returns:        void
*   comments:       Sets all GPIO pins into the right mode
****************************************************************************/
void ServiceSwitchBoxInit(void);

/*****************************************************************************
*   function :      ServiceSwitchBoxHandleStatusMsg
*   parameters:     void
*   returns:        void
*   comments:       Handle the section status messages and give feedback to 
*                   the sprayer module S.
****************************************************************************/
void ServiceSwitchBoxHandleStatusMsg(UI08_t * sectionBytesPtr);

/*****************************************************************************
*   function :      ServiceSwitchBoxHandleSwitch
*   parameters:     void
*   returns:        void
*   comments:       Read switch of the sprayer module S and send the state 
*                   of the switch on the CAN-Bus.
****************************************************************************/
void ServiceSwitchBoxHandleSwitch(void);

/*****************************************************************************
*   function :      SendSectionSwitchesMsg
*   parameters:     void
*   returns:        void
*   comments:       
****************************************************************************/
void SendSectionSwitchesMsg(void);

/*****************************************************************************
*   function :      SendMainSwitchMsg
*   parameters:     void
*   returns:        void
*   comments:       
****************************************************************************/
void SendMainSwitchMsg(void);

#endif
/****************************************************************************/
/**                                                                        **/
/**                               EOF                                      **/
/**                                                                        **/
/****************************************************************************/
