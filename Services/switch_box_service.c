/*
                             *******************
******************************* C SOURCE FILE *******************************
**                           *******************                           **
**                                                                         **
** project   : Switch Box LPC1768                                          **
** filename  : switch_box_service.c                                        **
**                                                                         **
*****************************************************************************
**                                                                         **
** Copyright (c) 2012, SBG Precision Farming/Navtronics Bvba               **
** All rights reserved.                                                    **
**                                                                         **
*****************************************************************************

VERSION HISTORY:
----------------

Version     : 1
Date        : 20130610
Revised by  : Siddhi Imming
Description : * Original version.

FILE INFORMATION:
-----------------
Service unit for the Section control task
*/
/****************************************************************************/
/**                                                                        **/
/**                     MODULES USED                                       **/
/**                                                                        **/
/****************************************************************************/
#include "hw_platform.h"
#include "lpc17xx_pinsel.h"
#include "lpc17xx_adc.h"
#include "lpc17xx_gpio.h"

#include "switch_box_service.h"
#include "section_ctrl_task.h"
#include "section_ctrl_defs.h"

#include "config.h"
#include "ibs_dl_section_ctrl.h"
#include "section_ctrl_utils.h"

/****************************************************************************/
/**                                                                        **/
/**                     DEFINITIONS AND MACROS                             **/
/**                                                                        **/
/****************************************************************************/

/****************************************************************************/
/**                                                                        **/
/**                     TYPEDEFS AND STRUCTURES                            **/
/**                                                                        **/
/****************************************************************************/

typedef union Switches_u
{
    UI08_t switchesByte[2];
    struct
    {
        UI08_t switchBit0  :1; //1
        UI08_t switchBit1  :1; //2
        UI08_t switchBit2  :1; //3
        UI08_t switchBit3  :1; //4
        UI08_t switchBit4  :1; //5
        UI08_t switchBit5  :1; //6
        UI08_t switchBit6  :1; //7
        UI08_t switchBit7  :1; //8
        UI08_t switchBit8  :1; //9
        UI08_t switchBit9  :1; //10
        UI08_t switchBit10  :1; //11
        UI08_t switchBit11  :1; //12
        UI08_t switchBit12  :1; //13
        UI08_t switchBit13  :1; //14
        UI08_t switchBit14  :1; //15
        UI08_t switchBit15  :1; //16
    }bits;
}Switches_t;

typedef struct SwitchStatus_s
{
    Bool_t      mainSwitch;
    Switches_t  switches;
}SwitchStatus_t;
/****************************************************************************/
/**                                                                        **/
/**                     PROTOTYPES OF LOCAL FUNCTIONS                      **/
/**                                                                        **/
/****************************************************************************/

/****************************************************************************/
/**                                                                        **/
/**                     EXPORTED VARIABLES                                 **/
/**                                                                        **/
/****************************************************************************/

/****************************************************************************/
/**                                                                        **/
/**                     GLOBAL VARIABLES                                   **/
/**                                                                        **/
/****************************************************************************/


static GpioPinAddress_t GpioInputPortPins[17] = 
{
    { 1, 0 },// Input 1
    { 1, 1 },// Input 2
    { 1, 4 },// Input 3
    { 1, 8 },// Input 4
    { 1, 9 },// Input 5
    { 1, 10},// Input 6
    { 1, 14},// Input 7
    { 4, 28},// Input 8
    { 0, 4 },// Input 9
    { 0, 5 },// Input 10
    { 0, 6 },// Input 11
    { 2, 0 },// Input 12
    { 1, 15},// Input 13
    { 1, 16},// Input 14
    { 1, 17},// Input 15
    { 4, 29},// Input 16
    { 2, 1 }// Input remote
};

static GpioPinAddress_t GpioOutputPortsPins[17] = 
{
    { 1, 18 }, // Output 1 
    { 1, 19 },// Output 2 
    { 1, 20 },// Output 3 
    { 1, 21 },// Output 4 
    { 3, 26 },// Output 5 
    { 3, 25 },// Output 6 
    { 0, 29 },// Output 7 
    { 0, 30 },// Output 8 
    { 1, 26 },// Output 9 
    { 1, 27 },// Output 10 
    { 1, 28 },// Output 11
    { 1, 29 },// Output 12 
    { 1, 25 },// Output 13 
    { 1, 24 },// Output 14 
    { 1, 23 },// Output 15 
    { 1, 22 },// Output 16 
    { 0, 10 }// Output remote 
};

static SwitchStatus_t SwitchStatus;
static SwitchStatus_t PrevSwitchStatus;
static SpayerSystemInformation_t SpayerSystemInformation;
/****************************************************************************/
/**                                                                        **/
/**                     LOCAL FUNCTIONS                                    **/
/**                                                                        **/
/****************************************************************************/

/****************************************************************************/
/**                                                                        **/
/**                     EXPORTED FUNCTIONS                                 **/
/**                                                                        **/
/****************************************************************************/

/*****************************************************************************
*   function :      ServiceSwitchBoxInit
*   parameters:     void
*   returns:        void
*   comments:       Sets all GPIO pins into the right mode
****************************************************************************/
void ServiceSwitchBoxInit(void)
{
    UI08_t i = 0;
    PINSEL_CFG_Type pinCfg;

    // Inputs
    // SetDir to 0 for inputs
    pinCfg.Funcnum = PINSEL_FUNC_0;
    pinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
    pinCfg.Pinmode = PINSEL_PINMODE_NORMAL; // pull-ups on the PCB

    // loop to all inputs pins
    for (i = 0; i < 17; i++)
    {
        pinCfg.Portnum = GpioInputPortPins[i].PORT_NUM;
        pinCfg.Pinnum = GpioInputPortPins[i].PIN_NUM;
        PINSEL_ConfigPin(&pinCfg);
        GPIO_SetDir(GpioInputPortPins[i].PORT_NUM, 1<<GpioInputPortPins[i].PIN_NUM, 0);
    }

    // Outputs
    // SetDir to 1 for outputs
    pinCfg.Funcnum = PINSEL_FUNC_0;
    pinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
    pinCfg.Pinmode = PINSEL_PINMODE_PULLUP;

    // loop to all outputs pins
    for (i = 0; i < 17; i++)
    {
        pinCfg.Pinnum = GpioOutputPortsPins[i].PIN_NUM;
        pinCfg.Portnum = GpioOutputPortsPins[i].PORT_NUM;
        PINSEL_ConfigPin(&pinCfg);
        GPIO_SetDir(GpioOutputPortsPins[i].PORT_NUM, 1<<GpioOutputPortsPins[i].PIN_NUM, 1);
    }
    SwitchStatus.mainSwitch = FALSE;
    SwitchStatus.switches.switchesByte[0] = 0;
    SwitchStatus.switches.switchesByte[1] = 0;
    PrevSwitchStatus.mainSwitch = FALSE;
    PrevSwitchStatus.switches.switchesByte[0] = 0;
    PrevSwitchStatus.switches.switchesByte[1] = 0;
    SpayerSystemInformation.machineDirection = MACHINE_DIRECTION_NOT_AVAILABLE;
    SpayerSystemInformation.autosteerSwitchCommand = AUTOSTEER_SWITCH_COMMAND_NOT_PRESSED;
    SpayerSystemInformation.wheelbasedSpeed = 0;
    SpayerSystemInformation.DisplayDayNightMode = DISPLAY_STATUS_NOT_AVAILABLE;
    SpayerSystemInformation.wheelsteeringMode = WHEELSTEERING_MODE_NOT_AVAILABLE;
    SpayerSystemInformation.manualSteeringDetection = MANUAL_STEERING_NOT_DETECTED;
    SpayerSystemInformation.externalMainSwitchToggle = SWITCH_NOT_AVAILABLE;
    SpayerSystemInformation.mainSwitchCommand = SWITCH_NOT_AVAILABLE;
}

/*****************************************************************************
*   function :      ServiceSwitchBoxHandleStatusMsg
*   parameters:     void
*   returns:        void
*   comments:       Handle the section status messages and give feedback to 
*                   the sprayer module S.
****************************************************************************/
void ServiceSwitchBoxHandleStatusMsg(UI08_t * sectionBytesPtr)
{
    UI08_t pin_value;

    // The pump in indication is inverted to the 16 section.
    
    if (SwitchStatus.mainSwitch && ((sectionBytesPtr[1] != 0) || ((sectionBytesPtr[2] & 0x7F) != 0)))
    {
        // This is the 17 output indication of the main switch
        GPIO_SetValue(GpioOutputPortsPins[16].PORT_NUM, (0x01 << GpioOutputPortsPins[16].PIN_NUM));
    }
    else
    {
        GPIO_ClearValue(GpioOutputPortsPins[16].PORT_NUM,(0x01 << GpioOutputPortsPins[16].PIN_NUM));
    }

    for (UI08_t index1 = 0; index1 < 8; index1++)
    {
        pin_value  = (sectionBytesPtr[1] >> index1) & 0x01;
        if (pin_value != 1)
        {
            GPIO_SetValue(GpioOutputPortsPins[index1].PORT_NUM, (0x01 << GpioOutputPortsPins[index1].PIN_NUM));
        }
        else
        {
            GPIO_ClearValue(GpioOutputPortsPins[index1].PORT_NUM,(0x01 << GpioOutputPortsPins[index1].PIN_NUM));
        }
    }
    for (UI08_t index1 = 8; index1 < 16; index1++)
    {
        pin_value  = (sectionBytesPtr[2] >> index1-8) & 0x01;
        if (pin_value != 1)
        {
            GPIO_SetValue(GpioOutputPortsPins[index1].PORT_NUM, (0x01 << GpioOutputPortsPins[index1].PIN_NUM));
        }
        else
        {
            GPIO_ClearValue(GpioOutputPortsPins[index1].PORT_NUM,(0x01 << GpioOutputPortsPins[index1].PIN_NUM));
        }
    }

}

/*****************************************************************************
*   function :      ServiceSwitchBoxHandleSwitch
*   parameters:     void
*   returns:        void
*   comments:       Read switch of the sprayer module S and send the state 
*                   of the switch on the CAN-Bus.
****************************************************************************/
void ServiceSwitchBoxHandleSwitch(void)
{
    UI32_t port_value;
    UI08_t pin_value;
    SwitchStatus.switches.switchesByte[0] = 0x00;
    SwitchStatus.switches.switchesByte[1] = 0x00;
    
    for (UI08_t index1 = 0; index1 < 8; index1++)
    {
        // Read input of the ports:
        port_value = GPIO_ReadValue(GpioInputPortPins[index1].PORT_NUM);
        // sift 
        pin_value = (port_value >> GpioInputPortPins[index1].PIN_NUM) & 0x1;
        SwitchStatus.switches.switchesByte[0] |= !pin_value << index1;
    }
    for(I08_t index2 = 8; index2 < 16; index2++)
    {
        port_value = GPIO_ReadValue(GpioInputPortPins[index2].PORT_NUM);
        pin_value = (port_value >> GpioInputPortPins[index2].PIN_NUM) & 0x1;
        SwitchStatus.switches.switchesByte[1] |= !pin_value << (index2-8);
    }

    // Read Main switch
    port_value = GPIO_ReadValue(GpioInputPortPins[16].PORT_NUM);
    SwitchStatus.mainSwitch = ((port_value >> GpioInputPortPins[16].PIN_NUM) & 0x1) == FALSE;
    if (SwitchStatus.mainSwitch == FALSE)
    {
        SwitchStatus.switches.switchesByte[0] = 0;
        SwitchStatus.switches.switchesByte[1] = 0;
    }

    // If switches are change send over CAN
    if ((SwitchStatus.switches.switchesByte[0] != PrevSwitchStatus.switches.switchesByte[0]) ||
        (SwitchStatus.switches.switchesByte[1] != PrevSwitchStatus.switches.switchesByte[1]))
    {
        SendSectionSwitchesMsg();
        OS_RetriggerTimer(&SectionSwitchesSendMsgTimer);
    }

    if (SwitchStatus.mainSwitch != PrevSwitchStatus.mainSwitch)
    {
        SendMainSwitchMsg();
        OS_RetriggerTimer(&MainSwitchSendMsgTimer);
    }
}

/*****************************************************************************
*   function :      SendSectionSwitchesMsg
*   parameters:     void
*   returns:        void
*   comments:       
****************************************************************************/
void SendSectionSwitchesMsg(void)
{
    PrevSwitchStatus.switches.switchesByte[0] = SwitchStatus.switches.switchesByte[0];
    PrevSwitchStatus.switches.switchesByte[1] = SwitchStatus.switches.switchesByte[1];
    IbsSendScuCommand(&SwitchStatus.switches.switchesByte[0]);
}

/*****************************************************************************
*   function :      SendMainSwitchMsg
*   parameters:     void
*   returns:        void
*   comments:       
****************************************************************************/
void SendMainSwitchMsg(void)
{
    PrevSwitchStatus.mainSwitch = SwitchStatus.mainSwitch;
    SpayerSystemInformation.mainSwitchCommand = SwitchStatus.mainSwitch;
    IbsDlSendSprayerSystemInformation(SpayerSystemInformation);
}

/****************************************************************************/
/**                                                                        **/
/**                               EOF                                      **/
/**                                                                        **/
/****************************************************************************/
