/*
                             *******************
******************************* C SOURCE FILE *******************************
**                           *******************                           **
**                                                                         **
** project   : LPC1768 Projects                                            **
** filename  : ledservice.c                                                **
**                                                                         **
*****************************************************************************
**                                                                         **
** Copyright (c) 2012, SBG Precision Farming/Navtronics Bvba               **
** All rights reserved.                                                    **
**                                                                         **
*****************************************************************************

VERSION HISTORY:
----------------

Version     : 1
Date        : 20120919
Revised by  : Koen Smeuninx
Description : * Original version.

FILE INFORMATION:
-----------------
Service for the led task.

*/

/****************************************************************************/
/**                                                                        **/
/**                     MODULES USED                                       **/
/**                                                                        **/
/****************************************************************************/
#include "stddefs.h"
#include "sysconf.h"
#include "led_service.h"
#include "led_task.h"
#include "uart_service.h"

#include "lpc17xx_pinsel.h"
#include "lpc17xx_gpio.h"

/****************************************************************************/
/**                                                                        **/
/**                     DEFINITIONS AND MACROS                             **/
/**                                                                        **/
/****************************************************************************/
#define LED_CMD_QUEUE_SIZE 100
/****************************************************************************/
/**                                                                        **/
/**                     TYPEDEFS AND STRUCTURES                            **/
/**                                                                        **/
/****************************************************************************/

/****************************************************************************/
/**                                                                        **/
/**                     PROTOTYPES OF LOCAL FUNCTIONS                      **/
/**                                                                        **/
/****************************************************************************/
static void timerCallBack(void);
/****************************************************************************/
/**                                                                        **/
/**                     EXPORTED VARIABLES                                 **/
/**                                                                        **/
/****************************************************************************/
OS_Q LedServiceQueue;
/****************************************************************************/
/**                                                                        **/
/**                     GLOBAL VARIABLES                                   **/
/**                                                                        **/
/****************************************************************************/
static UI08_t LedQueue[LED_CMD_QUEUE_SIZE];
static OS_TIMER Timers[NUMBER_OF_LEDS];
//static LedAddress_t LedAdddress[NUMBER_OF_LEDS];
/****************************************************************************/
/**                                                                        **/
/**                     EXPORTED FUNCTIONS                                 **/
/**                                                                        **/
/****************************************************************************/
/*****************************************************************************
* function: InitLedService
* comments: Initializes the led service, the array's with the 
*           addresses of the led set and clear registers are initialized.  
******************************************************************************/
void InitLedService(void)
{
    //OS_CREATETIMER(&Timers[0], timerCallBack, 40);
    //OS_CREATETIMER(&Timers[1], timerCallBack, 500);
    OS_CREATETIMER(&Timers[2], timerCallBack, 1000);
    OS_Q_Create (&LedServiceQueue, LedQueue, LED_CMD_QUEUE_SIZE);
}

/*****************************************************************************
* function: HandleLedCommand
* comments: The led task calls this function to handle a received led command
*           The set and clear registers are used to avoid the need of a 
*           resource lock between reading and writing.  
******************************************************************************/
void HandleLedCommand(LedCommand_t *ledCmdPtr)
{
    Bool_t  statusToSet;
    
    switch (ledCmdPtr->ledStatus)
    {
        case LED_STATUS_ON:
            statusToSet = 0;
            break;
        case LED_STATUS_OFF:
            statusToSet = 1;
            break;
        case LED_STATUS_TOGGLE:
            if ( ( (GPIO_ReadValue(LED_PORT) >> (ledCmdPtr->gpioID)) & 0x01) !=0 )
            {
                statusToSet = 0;
            }
            else
            {
                statusToSet = 1;
            }
            break;
    }

    if (statusToSet == 0)
    {
        GPIO_ClearValue(LED_PORT, (0x01 << (ledCmdPtr->gpioID) ));         
    }
    else
    {
        GPIO_SetValue(LED_PORT, (0x01 << (ledCmdPtr->gpioID) ));
    }
        
    
}
/****************************************************************************/
/**                                                                        **/
/**                     LOCAL FUNCTIONS                                    **/
/**                                                                        **/
/****************************************************************************/

/*****************************************************************************
*   function :      timerCallBack
*   comments:       this callback is called when the software timer expires
*                   It add a command on a queue
****************************************************************************/
static void timerCallBack(void)
{
    LedCommand_t ledCommand;
    OS_TIMER *timerPtr;
    ledCommand.gpioID = GPIO_PIN_LED_BLUE;
    timerPtr = OS_GetpCurrentTimer();

    if (OS_GetTimerPeriod(timerPtr) == 1000)
    {
        ledCommand.ledStatus = LED_STATUS_ON;
        OS_SetTimerPeriod(timerPtr,20);
    }
    else
    {
        ledCommand.ledStatus = LED_STATUS_OFF;
        OS_SetTimerPeriod(timerPtr,1000);
    }
    //store a command on the queue with LED id and status ID
    OS_Q_Put(&LedServiceQueue,&ledCommand, sizeof(LedCommand_t));
    OS_RetriggerTimer(timerPtr);
}

/****************************************************************************/
/**                                                                        **/
/**                               EOF                                      **/
/**                                                                        **/
/****************************************************************************/
