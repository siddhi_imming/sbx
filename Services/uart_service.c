/*
                             *******************
******************************* C SOURCE FILE *******************************
**                           *******************                           **
**                                                                         **
** project   : LPC1768 Projects                                            **
** filename  : uartservice.c                                               **
**                                                                         **
*****************************************************************************
**                                                                         **
** Copyright (c) 2012, SBG Precision Farming/Navtronics Bvba               **
** All rights reserved.                                                    **
**                                                                         **
*****************************************************************************

VERSION HISTORY:
----------------
Version     : 2
Date        : 20121016
Revised by  : Koen Smeuninx
Description : * c style changes

Version     : 1
Date        : 20120919
Revised by  : Koen Smeuninx
Description : * Original version.

FILE INFORMATION:
-----------------

Service for the uart task.

*/

/****************************************************************************/
/**                                                                        **/
/**                     MODULES USED                                       **/
/**                                                                        **/
/****************************************************************************/
#include <stdarg.h>
#include <stdio.h>

#include "stddefs.h"
#include "sysconf.h"
#include "uart_service.h"
#include "uart_task.h"
#include "cmd_parser.h"
#include "reset.h"
#include "abx_bridge_hardi_driver.h"

#include "lpc17xx_gpio.h"
#include "lpc17xx_pinsel.h"

/****************************************************************************/
/**                                                                        **/
/**                     DEFINITIONS AND MACROS                             **/
/**                                                                        **/
/****************************************************************************/
/****************************************************************************/
/**                                                                        **/
/**                     TYPEDEFS AND STRUCTURES                            **/
/**                                                                        **/
/****************************************************************************/

/****************************************************************************/
/**                                                                        **/
/**                     PROTOTYPES OF LOCAL FUNCTIONS                      **/
/**                                                                        **/
/****************************************************************************/
static void uart0IrqHandler(void);
static void uart1IrqHandler(void);
static void uartParse(LPC_UART_TypeDef * uartId);
/****************************************************************************/
/**                                                                        **/
/**                     EXPORTED VARIABLES                                 **/
/**                                                                        **/
/****************************************************************************/
OS_Q UartServiceQueue;
/****************************************************************************/
/**                                                                        **/
/**                     GLOBAL VARIABLES                                   **/
/**                                                                        **/
/****************************************************************************/
//location where the queue is stored in memory
static UI08_t UartQueue[sizeof(UartCommand_t)*NUMBER_OF_UARTS];
//static UartAddress_t UartAdddress[NUMBER_OF_UARTS];
/****************************************************************************/
/**                                                                        **/
/**                     EXPORTED FUNCTIONS                                 **/
/**                                                                        **/
/****************************************************************************/
/*****************************************************************************
* function: InitUartService
* comments: Initializes the uart service, the array's with the 
*           addresses of the uart set and clear registers are initialized.  
******************************************************************************/
void InitUartService(void)
{
    // UART Configuration structure variable
    UART_CFG_Type uartConfigStruct;
    // UART FIFO configuration Struct variable
    UART_FIFO_CFG_Type uartFIFOConfigStruct;

    UART_ConfigStructInit(&uartConfigStruct);
      uartConfigStruct.Baud_rate =  UART_PORT1_BAUDRATE; 
      
    OS_Q_Create (&UartServiceQueue, UartQueue, sizeof(UartQueue));

    // Initialize UART0 peripheral with given to corresponding parameter
    UART_Init((LPC_UART_TypeDef *)LPC_UART0, &uartConfigStruct);
   
    
    UART_FIFOConfigStructInit(&uartFIFOConfigStruct);

    // Initialize FIFO for UART0 peripheral
    UART_FIFOConfig((LPC_UART_TypeDef *)LPC_UART0, &uartFIFOConfigStruct);

    // Enable UART Transmit
    UART_TxCmd((LPC_UART_TypeDef *)LPC_UART0, ENABLE);

    /* Enable UART Rx interrupt */
    UART_IntConfig((LPC_UART_TypeDef *)LPC_UART0, UART_INTCFG_RBR, ENABLE);
    /* Enable UART line status interrupt */
    UART_IntConfig((LPC_UART_TypeDef *)LPC_UART0, UART_INTCFG_RLS, ENABLE);
    /*
     * Do not enable transmit interrupt here, since it is handled by
     * UART_Send() function, just to reset Tx Interrupt state for the
     * first time
     */

    OS_ARM_InstallISRHandler(UART0_IRQn + 16,uart0IrqHandler);
    OS_ARM_ISRSetPrio(UART0_IRQn + 16,UART_ISR_PRIORITY);  // /* preemption = 1, sub-priority = 1 */
    OS_ARM_EnableISR(UART0_IRQn+ 16);

    uartConfigStruct.Baud_rate = UART_PORT2_BAUDRATE; 

    // Initialize UART0 peripheral with given to corresponding parameter
    UART_Init((LPC_UART_TypeDef *)LPC_UART1, &uartConfigStruct);
        
    UART_FIFOConfigStructInit(&uartFIFOConfigStruct);

    // Initialize FIFO for UART0 peripheral
    UART_FIFOConfig((LPC_UART_TypeDef *)LPC_UART1, &uartFIFOConfigStruct);

    // Enable UART Transmit
    UART_TxCmd((LPC_UART_TypeDef *)LPC_UART1, ENABLE);

    /* Enable UART Rx interrupt */
    UART_IntConfig((LPC_UART_TypeDef *)LPC_UART1, UART_INTCFG_RBR, ENABLE);
    /* Enable UART line status interrupt */
    UART_IntConfig((LPC_UART_TypeDef *)LPC_UART1, UART_INTCFG_RLS, ENABLE);
    /*
     * Do not enable transmit interrupt here, since it is handled by
     * UART_Send() function, just to reset Tx Interrupt state for the
     * first time
     */

    OS_ARM_InstallISRHandler(UART1_IRQn + 16,uart1IrqHandler);
    OS_ARM_ISRSetPrio(UART1_IRQn + 16,UART_ISR_PRIORITY);  // /* preemption = 1, sub-priority = 1 */
    OS_ARM_EnableISR(UART1_IRQn + 16);
    
}

/*************************************************************************
* function: uart0IrqHandler 
* comments: Interrupt handler for uart 0 
**************************************************************************/
static void uart0IrqHandler(void)
{
    OS_EnterInterrupt();
    uartParse((LPC_UART_TypeDef *)LPC_UART0);
    OS_LeaveInterrupt();
}

/*************************************************************************
* function: uart1IrqHandler 
* comments: Interrupt handler for uart 1
**************************************************************************/
static void uart1IrqHandler(void)
{
    OS_EnterInterrupt();
    uartParse((LPC_UART_TypeDef *)LPC_UART1);
    OS_LeaveInterrupt();
}


/*************************************************************************
* function: uartParse 
* comments: general parser for uart commands
**************************************************************************/
static void uartParse(LPC_UART_TypeDef * uartId)
{
    
    UI08_t buffer[32];
    UI32_t nrBytesRead;
    UI08_t nIndex;
    static const UI08_t resetCommand[] = {0x24,0x40,0x79,0x7E,0x00,0xC0,0x0C,0x00,0x51,0x00,0x00,0x00};
    static UI08_t resetByteCounter=0;
    //send all data to the uart, (low priority task)
    nrBytesRead = UART_Receive(uartId, &buffer[0], 32, NONE_BLOCKING);

    if (uartId == (LPC_UART_TypeDef *)LPC_UART0)
    {
      for (nIndex = 0; nIndex < nrBytesRead ; nIndex++)
      {
          if (buffer[nIndex] == resetCommand[resetByteCounter++])
          {
              if (resetByteCounter >= sizeof(resetCommand))
              {
                  Reset();
              }
          }
          else
          {
              resetByteCounter = 0;
          }
      }

      for (nIndex = 0; nIndex < nrBytesRead ; nIndex++)
      {
          ParseCmdByte(buffer[nIndex]);
          if ((UART_GetLineStatus(uartId) & UART_LSR_RXFE) == UART_LSR_RXFE )
          {

          }
      }
    }
}

/*************************************************************************
* function:     UartWriteString 
* comments:     this function translates the external uart ID to an internal
*               uart pointer and puts a message on the uart queue
**************************************************************************/
void UartWriteString(UI08_t uartId, char *messagePtr)
{
    UI08_t messageSize = strlen(messagePtr);
    if (messageSize > 0)
    {
        UartWriteBytes(uartId,(const UI08_t *) messagePtr, messageSize);
        UartWriteBytes(uartId, "\r", 1);
    }
}
   

/*************************************************************************
* function:     UartWriteBytes 
* comments:     this function translates the external uart ID to an internal
*               uart pointer and puts a message on the uart queue
**************************************************************************/
void UartWriteBytes(UI08_t uartId, const UI08_t *dataPtr, UI08_t dataSize)
{
    UartCommand_t uartCmd;
    switch (uartId)
    {
        case UART_PORT1:
            uartCmd.uartIdPtr = LPC_UART0;
            break;
        case UART_PORT2:
            uartCmd.uartIdPtr = (LPC_UART_TypeDef *)LPC_UART1;
            break;
        default:
            return;
    };
    memcpy(&uartCmd.data, dataPtr, dataSize);
    uartCmd.dataSize = dataSize;
    OS_Q_Put(&UartServiceQueue, &uartCmd, sizeof(uartCmd) - UART_BUFFER_SIZE + dataSize);
}

/*****************************************************************************
* function: UartSendBytes
* comments: Internal function which is made accessible for the uart task.
*           this is the actual sending of the data to the hardware uart
******************************************************************************/
void UartSendBytes(UartCommand_t *cmdPtr)
{
    //send all data to the uart, (low priority task)
    UART_Send(cmdPtr->uartIdPtr, cmdPtr->data, cmdPtr->dataSize, BLOCKING);
}
/****************************************************************************/
/**                                                                        **/
/**                     LOCAL FUNCTIONS                                    **/
/**                                                                        **/
/****************************************************************************/



/****************************************************************************/
/**                                                                        **/
/**                               EOF                                      **/
/**                                                                        **/
/****************************************************************************/
